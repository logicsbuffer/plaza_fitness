<?php 

//======Front end template=========

require_once(ABSPATH.'wp-admin/includes/user.php' );
$user = wp_get_current_user ();
$obj_dashboard= new Gmgtdashboard;
//$user_id = get_current_user_id();
$user_id = get_current_user_id();
$user_meta = get_userdata($user_id);
$user_roles = $user_meta->roles;
if($user_roles[0] == 'staff_member'){
	$trainer_user_id = get_current_user_id();
	if(isset($_REQUEST['member_id'])){
		$user_id = $_REQUEST['member_id'];
	}elseif($_REQUEST['submit_userid']){
		$selected_member_id = $_POST['user_id'];
		update_user_meta($trainer_user_id,'selected_member',$selected_member_id);
		$user_id = get_user_meta ($trainer_user_id,'selected_member',true);

	}else{
			$selected_member_user_id = get_user_meta ($trainer_user_id,'selected_member',true);
			if($selected_member_user_id){
			$user_id = $selected_member_user_id;
			}else{
			$get_members = array(
				 'role' => 'member',
				 'orderby' => 'user_nicename',
				 'order' => 'ASC'
				);
			$membersdata = get_users($get_members);
			$first_member_id = $membersdata[0]->data->ID;
			$user_id = $first_member_id;
			}
	}
}elseif($user_roles[0] == 'member'){
	$user_id = get_current_user_id();
}
$strength_band_arr = '';
$mile_stone_arr ='';
$strength_band ='';
$strength_band_name ='';
$mile_stone ='';
$mile_stone_name ='';

$strength_band = get_user_meta ($user_id,'strength_band',true);
if($strength_band){
	$strength_band_arr = explode("|",$strength_band);
	$strength_band = $strength_band_arr[1];
	$strength_band_name = $strength_band_arr[0];
}

$mile_stone = get_user_meta ($user_id,'mile_stone',true);
if($mile_stone){
	$mile_stone_arr = explode("|",$mile_stone);
	$mile_stone = $mile_stone_arr[1];
	$mile_stone_name = $mile_stone_arr[0];
}


$obj_gym = new Gym_management(get_current_user_id());
if (! is_user_logged_in ()) {
	$page_id = get_option ( 'gmgt_login_page' );
	
	wp_redirect ( home_url () . "?page_id=" . $page_id );
}
if (is_super_admin ()) {
	wp_redirect ( admin_url () . 'admin.php?page=gym_system' );
}
$obj_reservation = new Gmgtreservation;
$reservationdata = $obj_reservation->get_all_reservation();
$cal_array = array();
if(!empty($reservationdata))
{
	foreach ($reservationdata as $retrieved_data){
		$cal_array [] = array (
				'title' => $retrieved_data->event_name,
				'start' => $retrieved_data->event_date,
				'end' => $retrieved_data->event_date,


		);
	}
}
$birthday_boys=get_users(array('role'=>'member'));
$boys_list="";
if (! empty ( $birthday_boys )) {
		foreach ( $birthday_boys as $boys ) {
			 //$boys_list.=$boys->display_name." ";
			$cal_array [] = array (
					'title' => __($boys->display_name.' Birthday','gym_mgt'),
					'start' =>mysql2date('Y-m-d', $boys->birth_date) ,
					'end' => mysql2date('Y-m-d', $boys->birth_date),
					'backgroundColor' => '#F25656'
			);	
			
			
		}
		
	}
	
if (! empty ( $obj_gym->notice )) {
	foreach ( $obj_gym->notice as $notice ) {
			 $notice_start_date=get_post_meta($notice->ID,'gmgt_start_date',true);
			 $notice_end_date=get_post_meta($notice->ID,'gmgt_end_date',true);
			$i=1;
			
			$cal_array[] = array (
					'title' => $notice->post_title,
					'start' => mysql2date('Y-m-d', $notice_start_date ),
					'end' => date('Y-m-d',strtotime($notice_end_date.' +'.$i.' days')),
					'color' => '#12AFCB'
			);	
			
		}
	}	
function get_all_measurement_frontend()
	{
		global $wpdb;
		$table_gmgt_measurment = $wpdb->prefix. 'gmgt_measurment';
	
		$result = $wpdb->get_results("SELECT * FROM $table_gmgt_measurment");
		return $result;
	
	}
	
?>

<script>
jQuery(document).ready(function() {
	
	jQuery('#calendar').fullCalendar({
		header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				defaultView: 'month',
				editable: false,
			eventLimit: true, // allow "more" link when too many events
			events: <?php echo json_encode($cal_array);?>
			
		});

		 
	});
</script>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/dataTables.css'; ?>">
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/dataTables.editor.min.css'; ?>">
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/dataTables.tableTools.css'; ?>">
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/dataTables.responsive.css'; ?>">
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/jquery-ui.css'; ?>">
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/font-awesome.min.css'; ?>">
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/popup.css'; ?>">
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/style1.css'; ?>">
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/custom.css'; ?>">
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/fullcalendar.css'; ?>">


<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/bootstrap.min.css'; ?>">	
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/bootstrap-timepicker.min.css'; ?>">
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/bootstrap-multiselect.css'; ?>">	
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/white.css'; ?>">
    
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/gymmgt.min.css'; ?>">
<?php  if (is_rtl())
		 {?>
			<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/bootstrap-rtl.min.css'; ?>">
		<?php } ?>


<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/lib/validationEngine/css/validationEngine.jquery.css'; ?>">
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/lib/select2-3.5.3/select2.css'; ?>">
<link rel="stylesheet"	href="<?php echo GMS_PLUGIN_URL.'/assets/css/gym-responsive.css'; ?>">


<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/assets/js/jquery-1.11.1.min.js'; ?>"></script>
<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/assets/js/jquery-ui.js'; ?>"></script>
<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/assets/js/moment.min.js'; ?>"></script>
<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/assets/js/fullcalendar.min.js'; ?>"></script>
<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/lib/select2-3.5.3/select2.min.js'; ?>"></script>


<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/assets/js/jquery.dataTables.min.js'; ?>"></script>
<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/assets/js/dataTables.tableTools.min.js'; ?>"></script>
<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/assets/js/dataTables.editor.min.js'; ?>"></script>
<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/assets/js/dataTables.responsive.js'; ?>"></script>
<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/assets/js/bootstrap.min.js'; ?>"></script>
<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/assets/js/bootstrap-timepicker.min.js'; ?>"></script>
<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/assets/js/bootstrap-multiselect.js'; ?>"></script>
<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/assets/js/responsive-tabs.js'; ?>"></script>
<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/lib/validationEngine/js/languages/jquery.validationEngine-en.js'; ?>"></script>
<script type="text/javascript"	src="<?php echo GMS_PLUGIN_URL.'/lib/validationEngine/js/jquery.validationEngine.js'; ?>"></script>

</head>
<body class="gym-management-content">
  
  <div class="container-fluid mainpage">
  <div class="navbar">
	
	<div class="col-md-8 col-sm-8 col-xs-6">
		<h3 class="logo-image"><img src="<?php echo get_option( 'gmgt_system_logo' ) ?>" class="img-circle head_logo" width="40" height="40" />
		<span><?php echo get_option( 'gmgt_system_name' );?> </span>
		</h3></div>
		
		<ul class="nav navbar-right col-md-4 col-sm-4 col-xs-6">
				
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<li class="dropdown"><a data-toggle="dropdown"
					class="dropdown-toggle" href="javascript:;">
						<?php
						$userimage = get_user_meta( $user->ID,'gmgt_user_avatar',true );
						if (empty ( $userimage )){
							echo '<img src='.get_option( 'gmgt_system_logo' ).' height="40px" width="40px" class="img-circle" />';
						}
						else	
							echo '<img src=' . $userimage . ' height="40px" width="40px" class="img-circle"/>';
						?>
							<span>	<?php echo $user->display_name;?> </span> <b class="caret"></b>
				</a>
					<ul class="dropdown-menu extended logout">
						<li><a href="?dashboard=user&page=account"><i class="fa fa-user"></i>
								<?php _e('My Profile','gym_mgt');?></a></li>
						<li><a href="<?php echo wp_logout_url(home_url()); ?>"><i
								class="fa fa-sign-out m-r-xs"></i><?php _e('Log Out','gym_mgt');?> </a></li>
					</ul></li>
				<!-- END USER LOGIN DROPDOWN -->
			</ul>
	
	</div>
	</div>
	<div class="container-fluid">
	<div class="row">
		<div class="col-sm-2 nopadding gym_left nav-side-menu">	<!--  Left Side -->
		<div class="brand"><?php _e('Menu',''); ?>    
		<i data-target="#menu-content" data-toggle="collapse" 
		class="fa fa-bars fa-2x toggle-btn collapsed" aria-expanded="false"></i></div>
 <?php
	
	$menu = gmgt_menu();
	
	$class = "";
	if (! isset ( $_REQUEST ['page'] ))	
		$class = 'class = "active"';
		 //print_r($menu); 	?>
	<ul class="nav nav-pills nav-stacked collapse in" id="menu-content">
				<li><a href="<?php echo site_url();?>"><span class="icone"><img src="<?php echo plugins_url( 'gym-management/assets/images/icon/home.png' )?>"/></span><span class="title"><?php _e('Home','gym_mgt');?></span></a></li>
				<li <?php echo $class;?>><a href="?dashboard=user"><span class="icone"><img src="<?php echo plugins_url('gym-management/assets/images/icon/dashboard.png' )?>"/></span><span
						class="title"><?php _e('Dashboard','gym_mgt');?></span></a></li>
				<?php
								
								 $role = $obj_gym->role;
								foreach ( $menu as $value ) {
									if ( isset($value[$role]) &&  $value[$role]) {
										if (isset ( $_REQUEST ['page'] ) && $_REQUEST ['page'] == $value ['page_link'])
											$class = 'class = "active"';
										else
											$class = "";
										echo '<li ' . $class . '><a href="?dashboard=user&page=' . $value ['page_link'] . '" class="left-tooltip" data-tooltip="'. $value ['menu_title'] . '" title="'. $value ['menu_title'] . '"><span class="icone"> <img src="' .$value ['menu_icone'].'" /></span><span class="title">'. $value ['menu_title'] . '</span></a></li>';
									}
									
									?>
									
        
        <?php
								}
							
									
									if (isset ( $_REQUEST['page'] ) == 'member' && isset ( $_REQUEST['tab'] )== 'addmember'){
									?>
									<script>
									jQuery( "#menu-content li:eq(6)" ).removeClass('active');
									jQuery( "#menu-content li:eq(5)" ).removeClass('active');
									jQuery( "#menu-content li:eq(2)" ).addClass('active');
									jQuery( document ).ready(function() {
										jQuery( "#account_dt a").attr('href','#');
										jQuery( "#account_dt a").html('<i class="fa fa-user"></i> Account Details');
									});
									</script>
									<?php
									}else if(isset ( $_REQUEST['page'] ) == 'member' && isset ( $_REQUEST['tab'] )== 'viewmember'){ ?>
										<script>
											jQuery( document ).ready(function() {
												jQuery( "#menu-content li:eq(2)" ).removeClass('active');
											});
										</script>
								<?php	} 
								
								if (isset ( $_REQUEST['action'] )){
									if (isset ( $_REQUEST['page'] ) == 'member'){
											if($_REQUEST['action'] == 'view'){
												?>
												<script>
												jQuery( document ).ready(function() {
													jQuery( "#account_dt a").hide();
													jQuery( "#menu-content li:eq(2)" ).removeClass('active');
													jQuery( "#menu-content li:eq(4)" ).addClass('active');
												});
												</script>
												<?php
											}elseif($_REQUEST['action'] == 'edit'){
												?>
												<script>
												jQuery( document ).ready(function() {
													jQuery( "#menu-content li:eq(4)" ).removeClass('active');
													jQuery( "#menu-content li:eq(2)" ).addClass('active');
												});
												</script>
												<?php
											}else{
												?>
												<script>
												jQuery( document ).ready(function() {
													//jQuery( "#menu-content li:eq(4)" ).removeClass('active');
													jQuery( "#menu-content li:eq(2)" ).removeClass('active');
												});
												</script>
												<?php
											}
									
									}
								}
								?>
								
	</ul>
		</div>
		<div class="page-inner" style="min-height:1050px;">
		
		<div class="right_side <?php if(isset($_REQUEST['page']))echo $_REQUEST['page'];?>">
		   <?php 
		if (isset ( $_REQUEST ['page'] )) {
			require_once GMS_PLUGIN_DIR . '/template/' . $_REQUEST['page'] . '.php';
			return false;
		}?>
		<!---start new dashboard------>
			<div class="row">
	<div class="col-sm-12">
	

	</div>
	</div>
		<div class="row "><!-- Start Row2 -->
		<div class="row left_section col-md-8 col-sm-8"><!-- Start Row2 -->
<?php	
/* if(isset($_REQUEST['member_id']))
$member_id=$_REQUEST['member_id'];
$edit=0;					
$edit=1; */
//$member_id = get_current_user_id();
$user_info = get_userdata($user_id);
$membership_status = get_user_meta($user_id,'membership_status',true);
$edit ='allowed';
?>
<div class="member_view_row1">

	<div class="col-md-12 col-sm-12 membr_left">
	<div class="user_sel_main row">
			
		<form name="user_sel_form" action="" method="POST" class="form-horizontal" id="user_sel_form">
			
			<?php if($obj_gym->role=='staff_member' || $obj_gym->role=='accountant'){ ?>
			<label class="col-sm-3 control-label" for="day"><?php _e('Select Member','gym_mgt');?><span class="require-field">*</span></label>	
			<div class="col-sm-6">
				<select id="member_list" class="form-control display-members" name="user_id" required>
				<option value=""><?php _e('Select Member','gym_mgt');?></option>
					<?php 
					//$get_members = array('role' => 'member');
					$get_members = array(
						 'role' => 'member',
						 'orderby' => 'user_nicename',
						 'order' => 'ASC'
						);
					
					$membersdata = get_users($get_members);
					 if(!empty($membersdata))
					 {
						foreach ($membersdata as $member){?>
							<?php 
							$member_id = $member->ID;
							$trainer_id = get_current_user_id();	
							$member_trainer = get_user_meta($member_id, 'member_trainer',true);

							if($trainer_id == $member_trainer){ ?> 
							<option value="<?php echo $member->ID;?>" <?php selected($member_id,$user_id);?>><?php echo $member->display_name." - ".$member->ID; ?> </option>
						<?php }
						}
					 }?>
			</select>
			</div>
			<div class="col-sm-2">
				<input type="submit" class="btn btn-info" value="Load member Data" name="submit_userid">
			</div>
			<?php }
			else
			{?>
				<input type="hidden" id="member_list" name="user_id" value="<?php echo $user_id;?>">
			<?php } ?>
			

		</form>
	</div>
	<h2 class="section_heading">Overview</h2>
		<div class="col-md-6 col-sm-12 left_side">
		<?php 
		if($user_info->gmgt_user_avatar == "")
			{?>
			<img alt="" src="<?php echo get_option( 'gmgt_system_logo' ); ?>">
			<?php }
			else {
				?>
			<img style="max-width:100%;" src="<?php if($edit)echo esc_url( $user_info->gmgt_user_avatar ); ?>" />
			<?php 
			}
		?>
		</div>
		<div class="col-md-6 col-sm-12 right_side">
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-user"></i> 
				<?php _e('Name','gym_mgt'); ?>	
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color">
					<?php echo $user_info->first_name." ".$user_info->middle_name." ".$user_info->last_name;?> 
				</span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-envelope"></i> 
				<?php _e('Email','gym_mgt');?> 	
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color"><?php echo $user_info->user_email;?></span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td"><i class="fa fa-phone"></i> <?php _e('Mobile No','gym_mgt');?> </div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color">
					<span class="txt_color"><?php echo $user_info->mobile;?> </span>
				</span>
			</div>
		</div>
		<div class="table_row hide">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-users"></i> <?php _e('Membership','gym_mgt');?>
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color"><?php echo $membership_status;?></span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-power-off"></i> <?php _e('Status','gym_mgt');?>
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color"><?php echo $membership_status;?></span>
			</div>
		</div>
	</div>
	</div>
</div>
<?php
$current_live_stream_video = get_option( "current_live_stream_video");
?>
		<div class="row recent_video">
			<div class="col-md-12 col-sm-12">
			<h2 class="section_heading">Current Livestream</h2>
				<iframe src="<?php echo $current_live_stream_video; ?>" width="640" height="564" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" style="width: 720px; height: 542.5px;"></iframe>
			</div>
		</div>
<h2 class="section_heading">Your Community</h2>
<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="<?php echo home_url().'?dashboard=user&page=member';?>">
				<div class="panel info-box panel-white">
					<div class="panel-body member">
						<div class="info-box-stats">
							<p class="counter"><?php echo count(get_users(array('role'=>'member')));?></p>
							
							<span class="info-box-title"><?php echo esc_html( __( 'Member', 'gym_mgt' ) );?></span>
						</div>
						<img src="<?php echo GMS_PLUGIN_URL."/assets/images/dashboard/member.png"?>" class="dashboard_background">
					</div>
				</div>
			</a>
			</div>
			<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="<?php echo home_url().'?dashboard=user&page=staff_member';?>">
				<div class="panel info-box panel-white">
					<div class="panel-body staff-member">
						<div class="info-box-stats">
							<p class="counter"><?php echo count(get_users(array('role'=>'staff_member')));?></p>
							<span class="info-box-title"><?php echo esc_html( __( 'Staff Member', 'gym_mgt' ) );?></span>
						</div>
						<img src="<?php echo GMS_PLUGIN_URL."/assets/images/dashboard/staff-member.png"?>" class="dashboard_background">
                        
					</div>
				</div>
				</a>
			</div>
			
			<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="<?php echo home_url().'?dashboard=user&page=group';?>">
				<div class="panel info-box panel-white">
					<div class="panel-body group">
						<div class="info-box-stats groups-label">
							<p class="counter"><?php echo $obj_dashboard->count_group();?></p>
							
							<span class="info-box-title"><?php echo esc_html( __( 'Group', 'gym_mgt' ) );?></span>
						</div>
						<img src="<?php echo GMS_PLUGIN_URL."/assets/images/dashboard/group.png"?>" class="dashboard_background">
						
					</div>
				</div>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="<?php echo home_url().'?dashboard=user&page=message&tab=inbox';?>">
				<div class="panel info-box panel-white">
					<div class="panel-body message">
						<div class="info-box-stats">
							<p class="counter"><?php echo count(gmgt_count_inbox_item(get_current_user_id()));?></p>
							<span class="info-box-title"><?php echo esc_html( __( 'Message', 'gym_mgt' ) );?></span>
						</div>
						<img src="<?php echo GMS_PLUGIN_URL."/assets/images/dashboard/message.png"?>" class="dashboard_background_message">
						
					</div>
				</div>
				</a>
			</div>
			</div>
			<div class="col-md-4 membership-list col-sm-4 col-xs-12">
			<h2 class="section_heading">Biometrics</h2>
				<div class="panel panel-white">
					<div class="panel-body biometric_data">
						<h3><?php _e('Strength Band','gym_mgt'); ?></h3><p><?php echo $strength_band_name; ?></p>	
						<?php if($strength_band){ ?>
							<img src="<?php echo $strength_band; ?>">
						<?php } ?>
					</div>
				</div>
				<div class="panel panel-white">
				
					<div class="panel-body biometric_data">
						<h3><?php _e('Mile Stone','gym_mgt');?></h3><p><?php echo $mile_stone_name; ?></p>	
						<?php if($mile_stone){ ?>
							<img src="<?php echo $mile_stone; ?>">
						<?php } ?>
					</div>
				</div>
				<div class="panel panel-white">			
					<div class="panel-body">
						<div class="biometric_section">
							<canvas id="body_weight"></canvas>
						</div>
						<div class="biometric_section">
							<canvas id="body_fat"></canvas>
						</div>
						<div class="biometric_section">
							<canvas id="body_mass_index"></canvas>
						</div>
						<div class="biometric_section">
							<canvas id="push_up_max"></canvas>
						</div>
						<div class="biometric_section">
							<canvas id="crawl_max"></canvas>
						</div>
						<div class="biometric_section">
							<canvas id="plank_max"></canvas>
						</div>
						<div class="biometric_section">
							<canvas id="pull_up_max"></canvas>
						</div>
					</div>
				</div>
	
		   </div>
			<!--<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="panel panel-white">
					<div class="panel-body">
						<div id="calendar"></div>
					</div>
				</div>
			</div> -->
		</div>	<!-- End Row2 -->
	</div>
</div>
		
	</div>

</div>


<?php //$biometric_data = 0, 10, 5, 2, 20, 30, 45; 
	$measurement_data = get_all_measurement_frontend();

	$pullup_max_date_set = get_user_meta( $user_id, 'pullup_max_date',true); 
	$plank_max_date_set = get_user_meta( $user_id, 'plank_max_date',true); 
	$crawl_max_date_set = get_user_meta( $user_id, 'crawl_max_date',true); 
	$body_weight_date_set = get_user_meta( $user_id, 'body_weight_date',true);
	$body_fat_date_set = get_user_meta( $user_id, 'body_fat_date',true);
	$push_upmax_date_set = get_user_meta( $user_id, 'pushup_max_date',true); 
	$body_massi_date_set = get_user_meta( $user_id, 'body_massi_date',true);



	// Set date on graph
	$php_array_bw = explode(",",$pullup_max_date_set);
	$pull_up_max_date_array = json_encode($php_array_bw);

	$php_array_bw = explode(",",$plank_max_date_set);
	$plank_max_date_array = json_encode($php_array_bw);

	$php_array_bw = explode(",",$crawl_max_date_set);
	$crawl_max_date_array = json_encode($php_array_bw);

	$php_array_bw = explode(",",$body_weight_date_set);
	$body_weight_date_array = json_encode($php_array_bw);

	$php_array_bw = explode(",",$body_fat_date_set);
	$body_fat_date_array = json_encode($php_array_bw);

	$php_array_bw = explode(",",$push_upmax_date_set);
	$push_up_max_date_array = json_encode($php_array_bw);

	$php_array_bw = explode(",",$body_massi_date_set);
	$body_mass_date_array = json_encode($php_array_bw);
	?><pre><?php print_r($user_id); ?></pre><?php

	$body_weight = get_user_meta( $user_id, 'body_weight',true);
	$body_fat = get_user_meta( $user_id, 'body_fat',true);
	$body_mass_index = get_user_meta( $user_id, 'body_mass_index',true);
	$push_up_max = get_user_meta( $user_id, 'push_up_max',true);
	$crawl_max = get_user_meta( $user_id, 'crawl_max',true);
	$plank_max = get_user_meta( $user_id, 'plank_max',true);
	$pull_up_max = get_user_meta( $user_id, 'pull_up_max',true);

	//Data get
	
	$php_array_bw = explode(",",$body_weight);
	$body_weight_array = json_encode($php_array_bw);	

	//echo 'Body Weight:'.$body_weight;
	$php_array_bf = explode(",",$body_fat);
	$body_fat_array = json_encode($php_array_bf);
	
	$php_array_bm = explode(",",$body_mass_index);
	$body_mass_index_array = json_encode($php_array_bm);
	
	$php_array_pu = explode(",",$push_up_max);
	$push_up_max_array = json_encode($php_array_pu);
	
	$php_array_cm = explode(",",$crawl_max);
	$crawl_max_array = json_encode($php_array_cm);
	
	$php_array_pm = explode(",",$plank_max);
	$plank_max_array = json_encode($php_array_pm);
	
	$php_array_pum = explode(",",$pull_up_max);
	$pull_up_max_array = json_encode($php_array_pum);
	

?>
<!-- Chart Js -->
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<script>
$(document).ready(function() {

	var body_weight = document.getElementById('body_weight').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $body_weight_date_array; ?>,
			datasets: [{
				label: 'Body Weight',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $body_weight_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'lbs'
						},
						ticks: {
							max: 500,
							min: 0,
							suggestedMax: 500,
							suggestedMin: 0,
						}
					}]
				}
			}

		// Configuration options go here
		});
	var body_weight = document.getElementById('body_fat').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $body_fat_date_array; ?>,
			datasets: [{
				label: 'Body Fat',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $body_fat_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'percentage'
						},
						ticks: {
							max: 60,
							min: 0,
							suggestedMax: 60,
							suggestedMin: 0,
						}
					}]
				}
			}

		// Configuration options go here
		});
	var body_weight = document.getElementById('body_mass_index').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $body_mass_date_array; ?>,
			datasets: [{
				label: 'Body Mass Index',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $body_mass_index_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'numbers'
						},
						ticks: {
							max: 50,
							min: 15,
							suggestedMax: 50,
							suggestedMin: 15,
						}
					}]
				}
			}

		// Configuration options go here
		});
		
	var body_weight = document.getElementById('push_up_max').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $push_up_max_date_array; ?>,
			datasets: [{
				label: 'Push up',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $push_up_max_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'numbers'
						},
						ticks: {
							max: 200,
							min: 0,
							suggestedMax: 200,
							suggestedMin: 0,
						}
					}]
				}
			}

		// Configuration options go here
		});
		
	var body_weight = document.getElementById('pull_up_max').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $pull_up_max_date_array; ?>,
			datasets: [{
				label: 'Pull Up',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $pull_up_max_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'numbers'
						},
						ticks: {
							max: 40,
							min: 0,
							suggestedMax: 40,
							suggestedMin: 0,
						}
					}]
				}
			}

		// Configuration options go here
		});
		
	var body_weight = document.getElementById('crawl_max').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $crawl_max_date_array; ?>,
			datasets: [{
				label: 'Crawl',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $crawl_max_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'time'
						},
						ticks: {
							max: 40,
							min: 0,
							suggestedMax: 40,
							suggestedMin: 0,
						}
					}]
				}
			}

		// Configuration options go here
		});
		
	var body_weight = document.getElementById('plank_max').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $plank_max_date_array; ?>,
			datasets: [{
				label: 'Plank',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $plank_max_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'time'
						},
						ticks: {
							max: 40,
							min: 0,
							suggestedMax: 40,
							suggestedMin: 0,
						}
					}]
				}
			}

		// Configuration options go here
		});
		});
		

</script>

</body>
</html>

<?php  ?>