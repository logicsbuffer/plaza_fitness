<?php 

function gmgt_get_remote_file($url, $timeout = 30){
	$ch = curl_init();
	curl_setopt ($ch, CURLOPT_URL, $url);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$file_contents = curl_exec($ch);
	curl_close($ch);
	return ($file_contents) ? $file_contents : FALSE;
}
function load_documets($file,$type,$nm) {


$imagepath =$file;
     
$parts = pathinfo($_FILES[$type]['name']);


 $inventoryimagename = mktime()."-".$nm."-"."in".".".$parts['extension'];
 $document_dir = WP_CONTENT_DIR ;
           $document_dir .= '/uploads/gym_assets/';
	
		$document_path = $document_dir;

 
if($imagepath != "")
{	
	if(file_exists(WP_CONTENT_DIR.$imagepath))
	unlink(WP_CONTENT_DIR.$imagepath);
}
if (!file_exists($document_path)) {
	mkdir($document_path, 0777, true);
}	
       if (move_uploaded_file($_FILES[$type]['tmp_name'], $document_path.$inventoryimagename)) {
          $imagepath= $inventoryimagename;	
       }


return $imagepath;
}
add_action( 'wp_login_failed', 'gmgt_login_failed' ); // hook failed login 

function get_lastmember_id($role)
{
	global $wpdb;
	$this_role = "'[[:<:]]".$role."[[:>:]]'";
	$table_name = $wpdb->prefix .'usermeta';
	$metakey=$wpdb->prefix .'capabilities';
	$userid=$wpdb->get_row("SELECT MAX(user_id)as uid FROM $table_name where meta_key = '$metakey' AND meta_value RLIKE $this_role");
	return get_user_meta($userid->uid,'member_id',true);
	
}
function gmgt_login_failed( $user ) {
	// check what page the login attempt is coming from
	$referrer = $_SERVER['HTTP_REFERER'];
	
	 $curr_args = array(
				'page_id' => get_option('gmgt_login_page'),
				'login' => 'failed'
				);
				print_r($curr_args);
				$referrer_faild = add_query_arg( $curr_args, get_permalink( get_option('gmgt_login_page') ) );


	// check that were not on the default login page
	if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') && $user!=null ) {
		// make sure we don't already have a failed login attempt
		if ( !strstr($referrer, 'login=failed' )) {
			// Redirect to the login page and append a querystring of login failed
			wp_redirect( $referrer_faild);
		} else {
			wp_redirect( $referrer );
		}

		exit;
	}
}

function gmgt_menu()
{
	$user = wp_get_current_user();

	$user_menu = array();
	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/account.png' ),'menu_title'=>__( 'Account', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>1,'page_link'=>'account');
	
	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/message.png' ),'menu_title'=>__( 'Message', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>1,'page_link'=>'message');
	
	//$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/attandance.png' ),'menu_title'=>__( 'Calendar', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>1,'page_link'=>'calender');
	
	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon//reservation.png' ),'menu_title'=>__( 'Biometrics', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>1,'page_link'=>'biometrics');
	
	if ( !in_array( 'member', (array) $user->roles ) ) {

	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/staff-member.png' ),'menu_title'=>__( 'Trainers', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>1,'page_link'=>'staff_member');
	    //The user has the "author" role
	}
	
	//$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/membership-type.png' ),'menu_title'=>__( 'Membership Type', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'membership');
	
	//$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/group.png' ),'menu_title'=>__( 'Group', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'group');
	
	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/member.png' ),'menu_title'=>__( 'Member', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>1,'page_link'=>'member');
	
	//$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/activity.png' ),'menu_title'=>__( 'Activity', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'activity');
	
	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/products.png' ),'menu_title'=>__( 'The Playbook', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'the-playbook');
		
	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/products.png' ),'menu_title'=>__( 'Milestones ', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'your-performance-journey');
	
	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/products.png' ),'menu_title'=>__( 'Strength Band Achievement', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'strength-band-forum');
	
	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/workout.png' ),'menu_title'=>__( 'Session Library', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'livestream-studio');
	
	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/workout.png' ),'menu_title'=>__( 'Workouts', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'workouts');
	
	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/workout.png' ),'menu_title'=>__( 'Skills School', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'skills-school');
	
	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/products.png' ),'menu_title'=>__( 'Gamification', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'gamification');
	
	//$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/group.png' ),'menu_title'=>__( 'The Community', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'community');

	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/notice.png' ),'menu_title'=>__( 'Feedback', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'feedback');
	
	//$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/class-shedule.png' ),'menu_title'=>__( 'Class Shedule', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'class-schedule');
	 
	// $user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/attandance.png' ),'menu_title'=>__( 'Attendence', 'gym_mgt' ),'member'=>0,'staff_member' =>1,'accountant'=>0,'page_link'=>'attendence');
	
	// $user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/assigne-workout.png' ),'menu_title'=>__( 'Assigne Workouts', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'assign-workout');
	
	// $user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/workout.png' ),'menu_title'=>__( 'Workouts', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'workouts');
	
	// $user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/accountant.png' ),'menu_title'=>__( 'Accountant', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>1,'page_link'=>'accountant');
	
	 //$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/fee.png' ),'menu_title'=>__( 'Fee Payment', 'gym_mgt' ),'member'=>1,'staff_member' => 0,'accountant'=>1,'page_link'=>'membership_payment');
	
	//$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/payment.png' ),'menu_title'=>__( 'Payment', 'gym_mgt' ),'member'=>1,'staff_member' => 0,'accountant'=>1,'page_link'=>'payment');
	
	//$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/products.png' ),'menu_title'=>__( 'product', 'gym_mgt' ),'member'=>0,'staff_member' => 1,'accountant'=>1,'page_link'=>'product');
	
//	$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/store.png' ),'menu_title'=>__( 'Store', 'gym_mgt' ),'member'=>0,'staff_member' => 1,'accountant'=>1,'page_link'=>'store');
	
	//$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/newsletter.png' ),'menu_title'=>__( 'News letter', 'gym_mgt' ),'member'=>0,'staff_member' => 1,'accountant'=>0,'page_link'=>'news_letter');
	
	///$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/message.png' ),'menu_title'=>__( 'Message', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>1,'page_link'=>'message');
	
	//$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/notice.png' ),'menu_title'=>__( 'Notice', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>1,'page_link'=>'notice');
	
	//$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/nutrition-schedule.png' ),'menu_title'=>__( 'Nutrition Schedule', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'nutrition');
	
	//$user_menu[] = array('menu_icone'=>plugins_url( 'gym-management/assets/images/icon/reservation.png' ),'menu_title'=>__( 'Reservation', 'gym_mgt' ),'member'=>1,'staff_member' => 1,'accountant'=>0,'page_link'=>'reservation');
	
	
	return $user_menu;

}
function gym_get_single_membership_payment_record($mp_id)
{
	global $wpdb;
		$table_gmgt_membership_payment = $wpdb->prefix. 'gmgt_membership_payment';
		$result = $wpdb->get_row("SELECT * FROM $table_gmgt_membership_payment where mp_id=".$mp_id);
		return $result;
}
function gym_get_payment_history_by_mpid($mp_id)
{
	global $wpdb;
	$table_gmgt_membership_payment_history = $wpdb->prefix .'gmgt_membership_payment_history';
	
	$result =$wpdb->get_results("SELECT * FROM $table_gmgt_membership_payment_history WHERE mp_id=".$mp_id);
	return $result;
}
function gmgt_login_link()
{

	$args = array( 'redirect' => site_url() );
	
	if(isset($_GET['login']) && $_GET['login'] == 'failed')
	{
		?>

<div id="login-error" style="background-color: #FFEBE8;border:1px solid #C00;padding:5px;">
  <p>Login failed: You have entered an incorrect Username or password, please try again.</p>
</div>
<?php
	}
		
	 $args = array(
			'echo' => true,
			'redirect' => site_url( $_SERVER['REQUEST_URI'] ),
			'form_id' => 'loginform',
			'label_username' => __( 'Username' , 'gym_mgt'),
			'label_password' => __( 'Password', 'gym_mgt' ),
			'label_remember' => __( 'Remember Me' , 'gym_mgt'),
			'label_log_in' => __( 'Log In' , 'gym_mgt'),
			'id_username' => 'user_login',
			'id_password' => 'user_pass',
			'id_remember' => 'rememberme',
			'id_submit' => 'wp-submit',
			'remember' => true,
			'value_username' => NULL,
	        'value_remember' => false ); 
	 $args = array('redirect' => site_url('/?dashboard=user') );
	 
	 if ( is_user_logged_in() )
	 {
	 	?>
<a href="<?php echo home_url('/')."?dashboard=user"; ?>"><i
								class="fa fa-sign-out m-r-xs"></i>
<?php _e('Dashboard','gym_mgt');?>
</a>
<br /><a href="<?php echo wp_logout_url(); ?>"><i class="fa fa-sign-out m-r-xs" /><?php _e('Logout','gyn_mgt');?></a> 
<?php 
	 }
	 else 
	 {
		
	 wp_login_form( $args );
	 ?>
	 <div style="text-align: left;width: 35%;margin: 0 auto 40px;">
	 <a style="color:#9c9c9c;" href="<?php echo wp_lostpassword_url(); ?>" title="Lost Password">Lost Password ?</a></div><?php
	
	}
	 
}

add_action( 'wp_ajax_gmgt_add_or_remove_category', 'gmgt_add_or_remove_category');
add_action( 'wp_ajax_gmgt_add_category', 'gmgt_add_category');
add_action( 'wp_ajax_gmgt_remove_category', 'gmgt_remove_category');
add_action( 'wp_ajax_gmgt_load_user', 'gmgt_load_user');
add_action( 'wp_ajax_gmgt_invoice_view', 'gmgt_invoice_view');
add_action( 'wp_ajax_gmgt_load_activity', 'gmgt_load_activity');
add_action( 'wp_ajax_gmgt_nutrition_schedule_view', 'gmgt_nutrition_schedule_view');
add_action( 'wp_ajax_gmgt_load_workout_measurement', 'gmgt_load_workout_measurement');
add_action( 'wp_ajax_gmgt_group_member_view', 'gmgt_group_member_view');
add_action( 'wp_ajax_gmgt_add_workout', 'gmgt_add_workout');
add_action( 'wp_ajax_gmgt_delete_workout', 'gmgt_delete_workout');
add_action( 'wp_ajax_gmgt_today_workouts', 'gmgt_today_workouts');
add_action( 'wp_ajax_gmgt_measurement_view', 'gmgt_measurement_view');
add_action( 'wp_ajax_gmgt_measurement_delete', 'gmgt_measurement_delete');
add_action( 'wp_ajax_gmgt_load_enddate', 'gmgt_load_enddate');
add_action( 'wp_ajax_gmgt_view_notice',  'gmgt_view_notice');
add_action( 'wp_ajax_gmgt_add_nutrition', 'gmgt_add_nutrition');
add_action( 'wp_ajax_gmgt_delete_nutrition', 'gmgt_delete_nutrition');
add_action( 'wp_ajax_gmgt_paymentdetail_bymembership', 'gmgt_paymentdetail_bymembership');
add_action( 'wp_ajax_gmgt_member_add_payment',  'gmgt_member_add_payment');
add_action( 'wp_ajax_gmgt_member_view_paymenthistory',  'gmgt_member_view_paymenthistory');

function gmgt_view_notice()
{
	 $notice = get_post($_REQUEST['notice_id']);
	 ?>
<div class="form-group"> <a class="close-btn badge badge-success pull-right" href="#">X</a>
  <h4 class="modal-title" id="myLargeModalLabel">
    <?php _e('Notice Detail','gym_mgt'); ?>
  </h4>
</div>
<hr>
<div class="panel panel-white form-horizontal">
  <div class="form-group">
    <label class="col-sm-3" for="notice_title">
    <?php _e('Notice Title','gym_mgt');?>
    : </label>
    <div class="col-sm-9"> <?php echo $notice->post_title;?> </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3" for="notice_title">
    <?php _e('Notice Comment','gym_mgt');?>
    : </label>
    <div class="col-sm-9"> <?php echo $notice->post_content;?> </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3" for="notice_title">
    <?php _e('Notice For','gym_mgt');?>
    : </label>
    <div class="col-sm-9"> <?php echo get_post_meta( $notice->ID, 'notice_for',true);?> </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3" for="notice_title">
    <?php _e('Start Date','gym_mgt');?>
    : </label>
    <div class="col-sm-9"> <?php echo get_post_meta( $notice->ID, 'gmgt_start_date',true);?> </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3" for="notice_title">
    <?php _e('End Date','gym_mgt');?>
    : </label>
    <div class="col-sm-9"> <?php echo get_post_meta( $notice->ID, 'gmgt_end_date',true);?> </div>
  </div>
</div>
<?php 
	die();
}
function gmgt_today_workouts()
{
	$user_id=$_POST['uid'];
	
	
		global $wpdb;
		$table_name = $wpdb->prefix."gmgt_assign_workout";
		$table_gmgt_workout_data = $wpdb->prefix."gmgt_workout_data";
		$date = date('Y-m-d');
		
		$day_name = date('l', strtotime($date));
		//$sql = "Select * From $table_name where  user_id = $user_id AND CURDATE() between Start_date and End_date ";
		$sql = "Select *From $table_name as workout,$table_gmgt_workout_data as workoutdata where  workout.user_id = $user_id 
		AND  workout.workout_id = workoutdata.workout_id 
		AND workoutdata.day_name = '$day_name'
		AND '".$_POST['record_date']."' between workout.Start_date and workout.End_date ";

		$result = $wpdb->get_results($sql);
		if(!empty($result)){
			echo $option="<div class='work_out_datalist_header'><div class='col-md-10'>
					<span class='col-md-3'>".__('Activity','gym_mgt')."</span>
					<span class='col-md-2'>".__('Sets','gym_mgt')."</span>
					<span class='col-md-2'>".__('Reps','gym_mgt')."</span>
					<span class='col-md-2'>".__('KG','gym_mgt')."</span>
					<span class='col-md-3'>".__('Rest Time','gym_mgt')."</span>
					</div></div>";
			
			foreach ($result as $retrieved_data){
			
			
			echo $option="<div class='work_out_datalist'><div class='col-sm-10'>
				<input type='hidden' name='workouts_array[]' value='".$retrieved_data->id."'>
				<input type='hidden' name='workout_name_".$retrieved_data->id."' value='".$retrieved_data->workout_name."'>
				<span class='col-md-3'>".$retrieved_data->workout_name."</span>
				<span class='col-md-2'>".$retrieved_data->sets." ".__('Sets','gym_mgt')."</span>
				<span class='col-md-2'>".$retrieved_data->reps."  ".__('Reps','gym_mgt')."</span>
				<span class='col-md-2'>".$retrieved_data->kg."  ".__('Kg','gym_mgt')."</span>
				<span class='col-md-2'>".$retrieved_data->time."  ".__('Min','gym_mgt')."</span>
			</div>";
			echo $option="<div class='col-sm-10'>
				<span class='col-md-3'>".__('Your Workout','gym_mgt')."</span>
				<span class='col-md-2'><input type='text' class='my-workouts validate[required]' id='sets' name='sets_".$retrieved_data->id."' width='50px'></span>
				<span class='col-md-2'><input type='text' class='my-workouts validate[required]' id='reps' name='reps_".$retrieved_data->id."' width='50px'></span>
				<span class='col-md-2'><input type='text' class='my-workouts validate[required]' id='kg' name='kg_".$retrieved_data->id."' width='50px'></span>
				<span class='col-md-2'><input type='text' class='my-workouts validate[required]' id='rest' name='rest_".$retrieved_data->id."' width='50px'></span>
			</div></div>";
			
			}
		}
		else
		{
			echo $option = "<div class='work_out_datalist'><div class='col-sm-10'><span class='col-md-10'>".__('Not Assigned Today Workout','gym_mgt')."</span></div></div>";
		}
		
		die();
}


function gmgt_load_workout_measurement()
{
	
	global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_workouts';
	$result = $wpdb->get_row("SELECT measurment_id FROM $table_workout where id=". $_REQUEST['workout_id']);
	echo get_the_title($result->measurment_id);	
	die();
}

function gmgt_add_categorytype($data)
{
	global $wpdb;
	$result = wp_insert_post( array(

			'post_status' => 'publish',

			'post_type' => $data['category_type'],

			'post_title' => $data['category_name']) );

	$id = $wpdb->insert_id;

	return $id;

}


function gmgt_add_category($data)
{
	global $wpdb;

	$model = $_REQUEST['model'];

	$array_var = array();

	$data['category_name'] = $_REQUEST['category_name'];

	$data['category_type'] = $_REQUEST['model'];

	$id = gmgt_add_categorytype($data);



	$row1 = '<tr id="cat-'.$id.'"><td>'.$_REQUEST['category_name'].'</td><td><a class="btn-delete-cat badge badge-delete" href="#" id='.$id.' model="'.$model.'">X</a></td></tr>';

	$option = "<option value='$id'>".$_REQUEST['category_name']."</option>";

	$array_var[] = $row1;

	$array_var[] = $option;

	echo json_encode($array_var);

	die();

}
//Get byid // get by id
function gmgt_get_class_name($cid)
{
	
	global $wpdb;
	$table_name = $wpdb->prefix .'gmgt_class_schedule';
	
	$classname =$wpdb->get_row("SELECT class_name FROM $table_name WHERE class_id=".$cid);
	if(!empty($classname))
		return $classname->class_name;
	else
		return " ";
}
function get_membership_name($mid)
{

	global $wpdb;
	$table_name = $wpdb->prefix .'gmgt_membershiptype';

	$result =$wpdb->get_row("SELECT membership_label FROM $table_name WHERE membership_id=".$mid);
	if(!empty($result))
		return $result->membership_label;
	else
		return " ";
}
function get_membership_price($mid)
{

	global $wpdb;
	$table_name = $wpdb->prefix .'gmgt_membershiptype';

	$result =$wpdb->get_row("SELECT membership_amount FROM $table_name WHERE membership_id=".$mid);
	if(!empty($result))
		return $result->membership_amount;
	else
		return " ";
}
function get_membership_days($mid)
{

	global $wpdb;
	$table_name = $wpdb->prefix .'gmgt_membershiptype';

	$result =$wpdb->get_row("SELECT membership_length_id FROM $table_name WHERE membership_id=".$mid);
	if(!empty($result))
		return $result->membership_length_id;
	else
		return " ";
}
function get_membership_paymentstatus($mp_id)
{
	global $wpdb;
		$table_gmgt_membership_payment = $wpdb->prefix. 'gmgt_membership_payment';
		//echo "SELECT paid_amount FROM $table_gmgt_membership_payment where mp_id = $mp_id";
		$result = $wpdb->get_row("SELECT * FROM $table_gmgt_membership_payment where mp_id = $mp_id");
		//return $result->paid_amount;
	if($result->paid_amount >= $result->membership_amount)
		return 'Fully Paid';
	elseif($result->paid_amount > 0)
		return 'Partially Paid';
	else
		return 'Not Paid';
}
function get_all_membership_payment_byuserid($member_id)
{
	global $wpdb;
	$table_gmgt_membership_payment = $wpdb->prefix. 'gmgt_membership_payment';
	
	$result = $wpdb->get_results("SELECT * FROM $table_gmgt_membership_payment where member_id = $member_id");
	return $result;
}
function gym_get_groupmember($group_id)
{
	global $wpdb;
	$table_gmgt_groupmember = $wpdb->prefix. 'gmgt_groupmember';
	$result = $wpdb->get_results("SELECT member_id FROM $table_gmgt_groupmember where group_id=".$group_id);
	return $result;
}
function gmgt_get_activity_by_category($cat_id)
{

	global $wpdb;
	$table_activity = $wpdb->prefix. 'gmgt_activity';

	$activitydata = $wpdb->get_results("SELECT * FROM $table_activity where activity_cat_id=".$cat_id);
	
	
	return $activitydata;
}
function gym_get_activity_by_staffmember($staff_memberid)
{
	global $wpdb;
	$table_gmgt_activity = $wpdb->prefix. 'gmgt_activity';
	$result = $wpdb->get_results("SELECT * FROM $table_gmgt_activity where activity_assigned_to=".$staff_memberid);
	return $result;
}
function gmgt_remove_category()
{

	wp_delete_post($_REQUEST['cat_id']);

	die();

}
function  gmgt_get_all_category($model){

	$args= array('post_type'=> $model,'posts_per_page'=>-1,'orderby'=>'post_title','order'=>'Asc');

	$cat_result = get_posts( $args );

	return $cat_result;

}
function gmgt_add_or_remove_category()
{


	$model = $_REQUEST['model'];
	 


		$title = __("title",'gym_mgt');

		$table_header_title =  __("header",'gym_mgt');

		$button_text=  __("Add category",'gym_mgt');

		$label_text =  __("category Name",'gym_mgt');

	

	if($model == 'membership_category')
	{

		$title = __("Add Membership Category",'gym_mgt');

		$table_header_title =  __("Category Name",'gym_mgt');

		$button_text=  __("Add Category",'gym_mgt');

		$label_text =  __("Category Name",'gym_mgt');	

	}
	if($model == 'installment_plan')
	{

		$title = __("Add Installment Plan",'gym_mgt');

		$table_header_title =  __("Plan Name",'gym_mgt');

		$button_text=  __("Add Plan",'gym_mgt');

		$label_text =  __("Inastallment Plan Name",'gym_mgt');	

	}
	if($model == 'membership_period')
	{

		$title = __("Add Membership Period",'gym_mgt');

		$table_header_title =  __("Membership Period Name",'gym_mgt');

		$button_text=  __("Add Membership Period",'gym_mgt');

		$label_text =  __("Membership Period Name",'gym_mgt');	
		
		$placeholder_text=__("Only Number of Days",'gym_mgt');

	}
	if($model == 'role_type')
	{

		$title = __("Add Role Type",'gym_mgt');

		$table_header_title =  __("Role Name",'gym_mgt');

		$button_text=  __("Add Role",'gym_mgt');

		$label_text =  __("Role Name",'gym_mgt');	

	}
	if($model == 'specialization')
	{

		$title = __("Add Specialization",'gym_mgt');

		$table_header_title =  __("Specialization Name",'gym_mgt');

		$button_text=  __("Add Specialization",'gym_mgt');

		$label_text =  __("Specialization Name",'gym_mgt');	

	}
	if($model == 'intrest_area')
	{

		$title = __("Add Intrest Area",'gym_mgt');

		$table_header_title =  __("Intrest Area Name",'gym_mgt');

		$button_text=  __("Add Intrest Area",'gym_mgt');

		$label_text =  __("Intrest Area Name",'gym_mgt');	

	}
	if($model == 'source')
	{

		$title = __("Add Source",'gym_mgt');

		$table_header_title =  __("Source Name",'gym_mgt');

		$button_text=  __("Add Source",'gym_mgt');

		$label_text =  __("Source Name",'gym_mgt');	

	}
	if($model == 'event_place')
	{

		$title = __("Add Event Place",'gym_mgt');

		$table_header_title =  __("Place Name",'gym_mgt');

		$button_text=  __("Add Place",'gym_mgt');

		$label_text =  __("Place Name",'gym_mgt');	

	}
	if($model == 'activity_category')
	{

		$title = __("Add Activity Category",'gym_mgt');

		$table_header_title =  __("Activity Category Name",'gym_mgt');

		$button_text=  __("Add Activity Category",'gym_mgt');

		$label_text =  __("Activity Category Name",'gym_mgt');	

	}
	if($model == 'measurment')
	{

		$title = __("Add Measurment",'gym_mgt');

		$table_header_title =  __("Measurment Name",'gym_mgt');

		$button_text=  __("Add Measurment",'gym_mgt');

		$label_text =  __("Measurment Name",'gym_mgt');	

	}
	if($model == 'level_type')
	{

		$title = __("Add Level Type",'gym_mgt');

		$table_header_title =  __("Level Name",'gym_mgt');

		$button_text=  __("Add Level",'gym_mgt');

		$label_text =  __("Level Name",'gym_mgt');	

	}
	if($model == 'workout_limit')
	{

		$title = __("Add Workout Limit",'gym_mgt');

		$table_header_title =  __("Workout Limit",'gym_mgt');

		$button_text=  __("Add Workout Limit",'gym_mgt');

		$label_text =  __("Workout Limit",'gym_mgt');	

	}
	if($model == 'calories_category')
	{

		$title = __("Add Calories Category",'gym_mgt');

		$table_header_title =  __("Calories",'gym_mgt');

		$button_text=  __("Add Calories Category",'gym_mgt');

		$label_text =  __("Calories",'gym_mgt');	

	}
	
	$cat_result = gmgt_get_all_category( $model );

	?>

	<div class="modal-header"> <a href="#" class="close-btn badge badge-success pull-right">X</a>

  		<h4 id="myLargeModalLabel" class="modal-title"><?php echo $title;?></h4>

	</div>

	<div class="panel panel-white">

  		<div class="category_listbox">

  			<div class="table-responsive">

		  		<table class="table">

			  		<thead>

			  			<tr>

			                <!--  <th>#</th> -->

			                <th><?php echo $table_header_title;?></th>

			                <th><?php _e('Action','gym_mgt');?></th>

			            </tr>

			        </thead>

			         <?php 

			

        	$i = 1;

        	if(!empty($cat_result))

        	{

        		

        		foreach ($cat_result as $retrieved_data)

        		{

        		echo '<tr id="cat-'.$retrieved_data->ID.'">';

        		//echo '<td>'.$i.'</td>';

        		echo '<td>'.$retrieved_data->post_title.'</td>';

  				echo '<td id='.$retrieved_data->ID.'><a class="btn-delete-cat badge badge-delete" model='.$model.' href="#" id='.$retrieved_data->ID.'>X</a></td>';

        		echo '</tr>';

        		$i++;		

        		}

        	}

        ?>

		       </table>

		     </div>

  		</div>

  		<form name="category_form" action="" method="post" class="form-horizontal" id="category_form">

	  	 	<div class="form-group">

				<label class="col-sm-4 control-label" for="category_name"><?php echo $label_text;?><span class="require-field">*</span></label>

				<div class="col-sm-4">

					<input id="category_name" class="form-control text-input"  value="" name="category_name" <?php if(isset($placeholder_text)){?> type="number" placeholder="<?php  echo $placeholder_text;}else{?>" type="text" <?php }?>>

				</div>

				<div class="col-sm-4">

					<input type="button" value="<?php echo $button_text;?>" name="save_category" class="btn btn-success" model="<?php echo $model;?>" id="btn-add-cat"/>
						
				</div>

			</div>

  		</form>

  	</div>

	<?php 

	die();	

}
function gmgt_get_countery_phonecode($country_name)
{
	$url = plugins_url( 'countrylist.xml', __FILE__ );
	$xml=simplexml_load_file(plugins_url( 'countrylist.xml', __FILE__ )) or die("Error: Cannot create object");
	//$xml =simplexml_load_string(gmgt_get_remote_file($url));
	foreach($xml as $country)
	{
		if($country_name == $country->name)
			return $country->phoneCode;

	}
}
function days_array()
{
	return $week=array('Sunday'=>'Sunday','Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday');
}
function minute_array()
{
	return $minute=array('00'=>'00','15'=>'15','30'=>'30','45'=>'45');
}
// 1. Body weight, 2. % body fat, 3. Body Mass Index (BMI), Push up max, Crawl max (time), Plank max (time), and Pull up max.

function measurement_array()
{
	/* return $measurment = array('Body weight'=>'Body weight',
	'body fat'=>'% body fat','Body Mass Index (BMI)'=>'Body Mass Index (BMI)','Push up max'=>'Push up max','Crawl max (time)'=>'Crawl max (time)','Plank max (time)'=>'Plank max (time)','Pull up max'=>'Pull up max'); */
	return $measurment=array('Height'=>'Height',
	'Weight'=>'Weight','Chest'=>'Chest','Waist'=>'Waist','Thigh'=>'Thigh','Arms'=>'Arms','Fat'=>'Fat');
}
function get_single_class_name($class_id)
{
	global $wpdb;
		$table_class = $wpdb->prefix. 'gmgt_class_schedule';
	return $retrieve_subject = $wpdb->get_var( "SELECT class_name FROM $table_class WHERE class_id=".$class_id);	
}
function gmgt_load_user()
{
		
		$class_id =$_POST['class_list'];
		
		global $wpdb;
		$retrieve_data=get_users(array('meta_key' => 'class_id', 'meta_value' => $class_id,'role'=>'member'));
		$defaultmsg=__( 'Select Member' , 'gym_mgt');
		echo "<option value=''>".$defaultmsg."</option>";	
		foreach($retrieve_data as $users)
		{
			echo "<option value=".$users->id.">".$users->display_name."</option>";
		}
		die();
		
		
}
function gmgt_load_activity()
{
	
	global $wpdb;
		$table_activity = $wpdb->prefix. 'gmgt_activity';
	
		$activitydata = $wpdb->get_results("SELECT * FROM $table_activity where activity_cat_id=".$_REQUEST['activity_list']);
		$defaultmsg=__( 'Select Activity', 'gym_mgt');
		echo "<option value=''>".$defaultmsg."</option>";	
		foreach($activitydata as $activity)
		{
			echo "<option value=".$activity->activity_id.">".$activity->activity_title."</option>";
		}
		die();
}
function get_invoice_data($invoice_id)
{
	global $wpdb;
		$table_invoice= $wpdb->prefix. 'gmgt_payment';
		$result = $wpdb->get_row("SELECT *FROM $table_invoice where payment_id = ".$invoice_id);
		return $result;
}
function gmgt_invoice_view()
{
	
	$obj_payment= new Gmgtpayment();
	if($_POST['invoice_type']=='invoice')
	{
	$invoice_data=get_invoice_data($_POST['idtest']);
	}
	if($_POST['invoice_type']=='income'){
	$income_data=$obj_payment->gmgt_get_income_data($_POST['idtest']);
	}
	if($_POST['invoice_type']=='expense'){
	$expense_data=$obj_payment->gmgt_get_income_data($_POST['idtest']);

	}
	//var_dump($income_data);
	//exit;
	?>
		
			<div class="modal-header">
			<a href="#" class="close-btn badge badge-success pull-right">X</a>
			<h4 class="modal-title"><?php echo get_option('hmgt_hospital_name');?></h4>
			</div>
			<div class="modal-body" style="height:500px; overflow:auto;">
				<div id="invoice_print"> 
					<table width="100%" border="0">
						<tbody>
							<tr>
								<td width="70%">
									<img style="max-height:80px;" src="<?php echo get_option( 'gmgt_system_logo' ); ?>">
								</td>
								<td align="right" width="24%">
									
									<h5><?php $issue_date='DD-MM-YYYY';
												if(!empty($income_data)){
													$issue_date=$income_data->invoice_date;
													$payment_status=$income_data->payment_status;}
												if(!empty($invoice_data)){
													$issue_date=$invoice_data->payment_date;
													$payment_status=$invoice_data->payment_status;	}
												if(!empty($expense_data)){
													$issue_date=$expense_data->invoice_date;
													$payment_status=$expense_data->payment_status;}
									
									echo __('Issue Date','gym_mgt')." : ".$issue_date;?></h5>
									<h5><?php echo __('Status','gym_mgt')." : ".$payment_status;?></h5>
								</td>
							</tr>
						</tbody>
					</table>
					<hr>
					<table width="100%" border="0">
						<tbody>
							<tr>
								<td align="left">
									<h4><?php _e('Payment To','gym_mgt');?> </h4>
								</td>
								<td align="right">
									<h4><?php _e('Bill To','gym_mgt');?> </h4>
								</td>
							</tr>
							<tr>
								<td valign="top" align="left">
									<?php echo get_option( 'gmgt_system_name' )."<br>"; 
									 echo get_option( 'gmgt_gym_address' ).","; 
									 echo get_option( 'gmgt_contry' )."<br>"; 
									 echo get_option( 'gmgt_contact_number' )."<br>"; 
									?>
									
								</td>
								<td valign="top" align="right">
									<?php 
									if(!empty($expense_data)){
									echo $party_name=$expense_data->supplier_name; 
									}
									else
									{
										if(!empty($income_data))
											$member_id=$income_data->supplier_name;
										 if(!empty($invoice_data))
											$member_id=$invoice_data->member_id;
										
										
										
										$patient=get_userdata($member_id);
												
										echo $patient->display_name."<br>"; 
										 echo get_user_meta( $member_id,'address',true ).","; 
										 echo get_user_meta( $member_id,'city_name',true ).","; 
										echo get_user_meta( $member_id,'mobile',true )."<br>"; 
									}
									?>
								</td>
							</tr>
						</tbody>
					</table>
					<hr>
					<h4><?php _e('Invoice Entries','gym_mgt');?></h4>
					<table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
						<thead>
							<tr>
								<th class="text-center">#</th>
								<th class="text-center"> <?php _e('Date','gym_mgt');?></th>
								<th width="60%"><?php _e('Entry','gym_mgt');?> </th>
								<th><?php _e('Price','gym_mgt');?></th>
								<th class="text-center"> <?php _e('Username','gym_mgt');?> </th>
							</tr>
						</thead>
						<tbody>
						<?php 
							$id=1;
							$total_amount=0;
						if(!empty($income_data) || !empty($expense_data)){
							if(!empty($expense_data))
								$income_data=$expense_data;
							
							$patient_all_income=$obj_payment->get_oneparty_income_data($income_data->supplier_name);
							foreach($patient_all_income as $result_income){
								$income_entries=json_decode($result_income->entry);
								foreach($income_entries as $each_entry){
								$total_amount+=$each_entry->amount;?>
							<tr>
								<td class="text-center"><?php echo $id;?></td>
								<td class="text-center"><?php echo $result_income->invoice_date;?></td>
								<td><?php echo $each_entry->entry; ?> </td>
								<td class="text-right"> <?php echo $each_entry->amount; ?></td>
								<td class="text-center"><?php echo gym_get_display_name($result_income->receiver_id);?></td>
							</tr>
								<?php $id+=1;}
								}
						 
						}
						 if(!empty($invoice_data)){
							 $total_amount=$invoice_data->total_amount
							 ?>
							<tr>
								<td class="text-center"><?php echo $id;?></td>
								<td class="text-center"><?php echo $invoice_data->payment_date;?></td>
								<td><?php echo $invoice_data->title; ?> </td>
								<td class="text-right"> <?php echo $invoice_data->total_amount; ?></td>
								<td class="text-center"><?php echo gym_get_display_name($invoice_data->receiver_id);?></td>
							</tr>
							<?php }?>
						</tbody>
					</table>
					<table width="100%" border="0">
						<tbody>
							
							<?php if(!empty($invoice_data)){
								$total_amount=$invoice_data->total_amount;
								$grand_total=$invoice_data->total_amount-$invoice_data->discount;
								?>
							<tr>
								<td width="80%" align="right"><?php _e('Sub Total :','gym_mgt');?></td>
								<td align="right"><?php echo $total_amount;?></td>
							</tr>
							
							<tr>
								<td width="80%" align="right"><?php _e('Discount :','gym_mgt');?></td>
								<td align="right"><?php echo $invoice_data->discount;?></td>
							</tr>
							<tr>
								<td colspan="2">
									<hr style="margin:0px;">
								</td>
							</tr>
							<?php
							}
							if(!empty($income_data)){
								$grand_total=$total_amount;
							}
							?>								
							<tr>
								<td width="80%" align="right"><?php _e('Grand Total :','gym_mgt');?></td>
								<td align="right"><h4><?php echo $grand_total; ?></h4></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="print-button pull-left">
						<a  href="?page=invoice&print=print&invoice_id=<?php echo $_POST['idtest'];?>&invoice_type=<?php echo $_POST['invoice_type'];?>" target="_blank"class="btn btn-success"><?php _e('Print','gym_mgt');?></a>
				</div>
			</div>
			
				
			
		
	
	
	
	<?php die();
}

function gmgt_invoice_print($invoice_id)
{
	$obj_payment= new Gmgtpayment();
	if($_REQUEST['invoice_type']=='invoice')
	{
		$invoice_data=get_invoice_data($invoice_id);
	}
	if($_REQUEST['invoice_type']=='income'){
		$income_data=$obj_payment->gmgt_get_income_data($invoice_id);
	}
	if($_REQUEST['invoice_type']=='expense'){
		$expense_data=$obj_payment->gmgt_get_income_data($invoice_id);

	}?>
		
			
			<div class="modal-body">
				<div id="invoice_print" style="width: 90%;margin:0 auto;"> 
				<div class="modal-header">
			
			<h4 class="modal-title"><?php echo get_option('gmgt_system_name');?></h4>
			</div>
			
					<table width="100%" border="0">
						<tbody>
							<tr>
								<td width="70%">
									<img style="max-height:80px;" src="<?php echo get_option( 'gmgt_system_logo' ); ?>">
								</td>
								<td align="right" width="24%">
									
									<h5><?php $issue_date='DD-MM-YYYY';
												if(!empty($income_data)){
													$issue_date=$income_data->invoice_date;
													$payment_status=$income_data->payment_status;}
												if(!empty($invoice_data)){
													$issue_date=$invoice_data->payment_date;
													$payment_status=$invoice_data->payment_status;	}
												if(!empty($expense_data)){
													$issue_date=$expense_data->invoice_date;
													$payment_status=$expense_data->payment_status;}
									
									echo __('Issue Date','gym_mgt')." : ".$issue_date;?></h5>
									<h5><?php echo __('Status','gym_mgt')." : ".$payment_status;?></h5>
								</td>
							</tr>
						</tbody>
					</table>
					<hr>
					<table width="100%" border="0">
						<tbody>
							<tr>
								<td align="left">
									<h4><?php _e('Payment To','gym_mgt');?> </h4>
								</td>
								<td align="right">
									<h4><?php _e('Bill To','gym_mgt');?> </h4>
								</td>
							</tr>
							<tr>
								<td valign="top" align="left">
									<?php echo get_option( 'gmgt_system_name' )."<br>"; 
									 echo get_option( 'gmgt_gym_address' ).","; 
									 echo get_option( 'gmgt_contry' )."<br>"; 
									 echo get_option( 'gmgt_contact_number' )."<br>"; 
									?>
									
								</td>
								<td valign="top" align="right">
									<?php 
									if(!empty($expense_data)){
									echo $party_name=$expense_data->supplier_name; 
									}
									else
									{
										if(!empty($income_data))
											$member_id=$income_data->supplier_name;
										 if(!empty($invoice_data))
											$member_id=$invoice_data->member_id;
										
										
										
										$patient=get_userdata($member_id);
												
										echo $patient->display_name."<br>"; 
										 echo get_user_meta( $member_id,'address',true ).","; 
										 echo get_user_meta( $member_id,'city_name',true ).","; 
										echo get_user_meta( $member_id,'mobile',true )."<br>"; 
									}
									?>
								</td>
							</tr>
						</tbody>
					</table>
					<hr>
					<h4><?php _e('Invoice Entries','gym_mgt');?></h4>
					<table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
						<thead>
							<tr>
								<th class="text-center">#</th>
								<th class="text-center"> <?php _e('Date','gym_mgt');?></th>
								<th width="60%"><?php _e('Entry','gym_mgt');?> </th>
								<th><?php _e('Price','gym_mgt');?></th>
								<th class="text-center"> <?php _e('Username','gym_mgt');?> </th>
							</tr>
						</thead>
						<tbody>
						<?php 
							$id=1;
							$total_amount=0;
						if(!empty($income_data) || !empty($expense_data)){
							if(!empty($expense_data))
								$income_data=$expense_data;
							
							$patient_all_income=$obj_payment->get_oneparty_income_data($income_data->supplier_name);
							foreach($patient_all_income as $result_income){
								$income_entries=json_decode($result_income->entry);
								foreach($income_entries as $each_entry){
								$total_amount+=$each_entry->amount;?>
							<tr>
								<td class="text-center"><?php echo $id;?></td>
								<td class="text-center"><?php echo $result_income->invoice_date;?></td>
								<td><?php echo $each_entry->entry; ?> </td>
								<td class="text-right"> <?php echo $each_entry->amount; ?></td>
								<td class="text-center"><?php echo gym_get_display_name($result_income->receiver_id);?></td>
							</tr>
								<?php $id+=1;}
								}
						 
						}
						 if(!empty($invoice_data)){
							 $total_amount=$invoice_data->total_amount;?>
							<tr>
								<td class="text-center"><?php echo $id;?></td>
								<td class="text-center"><?php echo $invoice_data->payment_date;?></td>
								<td><?php echo $invoice_data->title; ?> </td>
								<td class="text-right"> <?php echo $invoice_data->unit_price; ?></td>
								<td class="text-center"><?php echo gym_get_display_name($invoice_data->receiver_id);?></td>
							</tr>
							<?php }?>
						</tbody>
					</table>
					<table width="100%" border="0">
						<tbody>
							
							<?php if(!empty($invoice_data)){
								$total_amount=$invoice_data->unit_price;
								$grand_total=$invoice_data->unit_price-$invoice_data->discount;
								?>
							<tr>
								<td width="80%" align="right"><?php _e('Sub Total :','gym_mgt');?></td>
								<td align="right"><?php echo $total_amount;?></td>
							</tr>
							
							<tr>
								<td width="80%" align="right"><?php _e('Discount :','gym_mgt');?></td>
								<td align="right"><?php echo $invoice_data->discount;?></td>
							</tr>
							<tr>
								<td colspan="2">
									<hr style="margin:0px;">
								</td>
							</tr>
							<?php
							}
							if(!empty($income_data)){
								$grand_total=$total_amount;
							}
							?>								
							<tr>
								<td width="80%" align="right"><?php _e('Grand Total :','gym_mgt');?></td>
								<td align="right"><h4><?php echo $grand_total; ?></h4></td>
							</tr>
						</tbody>
					</table>
				</div>
	</div>
	<?php die();
}

function gmgt_print_init()
{
	if(isset($_REQUEST['print']) && $_REQUEST['print'] == 'print' && $_REQUEST['page'] == 'invoice')
	{
		?>
<script>window.onload = function(){ window.print(); };</script>
<?php 
				
				gmgt_invoice_print($_REQUEST['invoice_id']);
				exit;
			}
	
			
}

add_action('init','gmgt_print_init');


function gmgt_nutrition_schedule_view()
{
	
	 //var_dump($notice);
	$obj_nutrition=new Gmgtnutrition;
	$result = $obj_nutrition->get_single_nutrition($_REQUEST['nutrition_id']);
	 ?>
			<div class="form-group"> 	<a href="#" class="close-btn badge badge-success pull-right">X</a>
			  <h4 class="modal-title" id="myLargeModalLabel">
				<?php echo $result->day.' '. __('Nutrition Schedule','gym_mgt'); ?>
			  </h4>
			</div>
			<hr>
			<div class="panel panel-white form-horizontal">
			  <div class="form-group">
				<label class="col-sm-3" for="Breakfast"><strong>
				<?php _e(' Breakfast','gym_mgt');?></strong>
				: </label>
				<div class="col-sm-9"> <?php echo $result->breakfast;?> </div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-3" for="notice_title"><strong>
				<?php _e('Midmorning Snack','gym_mgt');?></strong>
				: </label>
				<div class="col-sm-9"> <?php echo $result->midmorning_snack;?> </div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-3" for="lunch"><strong>
				<?php _e('Lunch','gym_mgt');?></strong>
				: </label>
				<div class="col-sm-9"> <?php echo $result->lunch;?> </div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-3" for="afternoon_snack"><strong>
				<?php _e('Afternoon Snack','gym_mgt');?></strong>
				: </label>
				<div class="col-sm-9"> <?php echo $result->afternoon_snack;?> </div>
			  </div>
			   <div class="form-group">
				<label class="col-sm-3" for="dinner"><strong>
				<?php _e('Dinner','gym_mgt');?></strong>
				: </label>
				<div class="col-sm-9"> <?php echo $result->dinner;?> </div>
			  </div>
			   <div class="form-group">
				<label class="col-sm-3" for="afterdinner_snack"><strong>
				<?php _e('Afterdinner Snack','gym_mgt');?></strong>
				: </label>
				<div class="col-sm-9"> <?php echo $result->afterdinner_snack;?> </div>
			  </div>
			  			   <div class="form-group">
				<label class="col-sm-3" for="afterdinner_snack"><strong>
				<?php _e('Afterdinner Snack','gym_mgt');?></strong>
				: </label>
				<div class="col-sm-9"> <?php echo $result->start_date;?> </div>
			  </div>
			  			   <div class="form-group">
				<label class="col-sm-3" for="afterdinner_snack"><strong>
				<?php _e('Afterdinner Snack','gym_mgt');?></strong>
				: </label>
				<div class="col-sm-9"> <?php echo $result->expire_date;?> </div>
			  </div>
			<?php 
				die();
}
function gmgt_group_member_view()
{
	$group_id = $_REQUEST['group_id'];
	//$allmembers=get_users(array('meta_key'=>'group_id','meta_value'=>$group_id));
	//$joingroup_list = $obj_member->get_all_joingroup($member_id);
	//$groups_array = $obj_member->convert_grouparray($joingroup_list);
	$allmembers =gym_get_groupmember($group_id);
	?>
	<div class="form-group"> 	<a href="#" class="close-btn badge badge-success pull-right">X</a>
		<h4 class="modal-title" id="myLargeModalLabel">
			<?php echo  __('Group Member','gym_mgt'); ?>
		</h4>
	</div>
	<hr>
	<div class="panel-body">
		<div class="slimScrollDiv">
			<div class="inbox-widget slimscroll">
			<?php 
			if(!empty($allmembers))
			foreach ($allmembers as $retrieved_data){
			?>
				<div class="inbox-item">
					<div class="inbox-item-img">
			<?php 
				$uid=$retrieved_data->member_id;
				$userimage=get_user_meta($uid, 'gmgt_user_avatar', true);
				if(empty($userimage))
								{
												echo '<img src='.get_option( 'gmgt_system_logo' ).' height="50px" width="50px" class="img-circle" />';
								}
								else
									echo '<img src='.$userimage.' height="50px" width="50px" class="img-circle"/>';	
			?>
				</div>
				<p class="inbox-item-author"><?php echo gym_get_display_name($retrieved_data->member_id);?></p>
				</div>
				
			<?php 
			}
			else 
			{
				?>
			<p><?php _e('No any member yet.','gym_mgt');?></p>
			<?php
			} 
			?>
			</div>
		</div>
	</div>
	<?php 
?>
	
<?php 
	die();
}
function gmgt_measurement_delete()
{
	$obj_workout = new Gmgtworkout();
	$measurement_id = $_REQUEST['measurement_id'];
	$measurement_data = $obj_workout->get_measurement_deleteby_id($measurement_id);
	die();
}
function gmgt_load_enddate()
{
	
	$membership_id = $_POST['membership_id'];
	$start_date = $_POST['start_date'];
	$obj_membership=new Gmgtmembership;
		
		$joiningdate=$start_date;
		$membership=$obj_membership->get_single_membership($membership_id);
	 $validity=$membership->membership_length_id;
	 
	 $expiredate= date('Y-m-d', strtotime($joiningdate. ' + '.$validity.' days'));
	echo $expiredate;
	die();
}
function gmgt_measurement_view()
{
	$obj_workout = new Gmgtworkout();
	$curr_user_id=get_current_user_id();
	$obj_gym=new Gym_management($curr_user_id);
	$user_id = $_REQUEST['user_id'];	
	$measurement_data = $obj_workout->get_all_measurement_by_userid($user_id);
	?>
		<div class="form-group"> 	<a href="#" class="close-btn badge badge-success pull-right">X</a>
			<h4 class="modal-title" id="myLargeModalLabel">
				<?php echo  gym_get_display_name($user_id).__('\'s Measurement','gym_mgt'); ?>
			</h4>
		</div>
		<hr>
		<div class="panel-body">
			<div class="table-responsive box-scroll">
        		<table id="measurement_list" class="display table" cellspacing="0" width="100%">
		        	 <thead>
		            	<tr>						
						<th><?php  _e( 'Measurement', 'gym_mgt' ) ;?></th>
						<th><?php  _e( 'Result', 'gym_mgt' ) ;?></th>			
					    <th><?php  _e( 'Record Date', 'gym_mgt' ) ;?></th>		
					     <th><?php  _e( 'Action Date', 'gym_mgt' ) ;?></th>				            
		            	</tr>		            	 
		        	</thead>
		        	<tbody>
		        	<?php 
		        	
		        	if(!empty($measurement_data))
		        	{
		        		foreach ($measurement_data as $retrieved_data)
		        		{?>
			        		<tr id="row_<?php echo $retrieved_data->measurment_id?>">
			        			<td class="recorddate"><?php echo $retrieved_data->result_measurment;?></td>
								<td class="duration"><?php echo $retrieved_data->result;?></td>
								<td class="result"><?php echo $retrieved_data->result_date;?></td>
								<td class="result">
								<?php if($obj_gym->role=='Administrator'){?>
								<a href="?page=gmgt_workout&tab=addmeasurement&action=edit&measurment_id=<?php echo $retrieved_data->measurment_id?>" class="btn btn-info">
								<?php }
								else
								{?>
								<a href="?dashboard=user&page=workouts&tab=addmeasurement&action=edit&measurment_id=<?php echo $retrieved_data->measurment_id?>" class="btn btn-info">
								<?php }
								_e('Edit', 'gym_mgt' ) ;?></a>
								 <a href="#" class="btn btn-danger measurement_delete" data-val="<?php echo $retrieved_data->measurment_id?>"><?php _e('Delete','gym_mgt');?></a>
								</td>
			        		</tr>
		        		<?php 
		        		}
		        	}
		        	else 
		        	{
		        	?>
		        		<tr>
		        		<td colspan=3> <?php _e('No Record Found','gym_mgt');?></td>
		        		</tr>
		        	<?php 
		        	}
		        	?>
		        	</tbody>
		        	
		        </table>
		</div>
		<?php
		die(); 
}
function gmgt_add_workout()
{
	if(isset($_REQUEST['data_array']))
	{
	$data_array = $_REQUEST['data_array'];
	$data_value = json_encode($data_array);
	echo "<input type='hidden' value='".htmlspecialchars($data_value,ENT_QUOTES)."' name='activity_list[]'>";
	//var_dump($data_array);
	}
	die();
}
function gmgt_add_nutrition()
{
	
	if(isset($_REQUEST['data_array']))
	{
	$data_array = $_REQUEST['data_array'];
	$data_value = json_encode($data_array);
	echo "<input type='hidden' value='".htmlspecialchars($data_value,ENT_QUOTES)."' name='nutrition_list[]'>";
	//var_dump($data_array);
	}
	die();
}
function gmgt_delete_workout()
{

	
	$work_out_id = $_REQUEST['workout_id'];
	global $wpdb;
	$table_workout = $wpdb->prefix. 'gmgt_assign_workout';
	$table_workout_data = $wpdb->prefix. 'gmgt_workout_data';
	$result = $wpdb->query("DELETE FROM $table_workout_data where workout_id= ".$work_out_id);
	$result = $wpdb->query("DELETE FROM $table_workout where workout_id= ".$work_out_id);
	die();
}
function gmgt_delete_nutrition()
{
	$work_out_id = $_REQUEST['workout_id'];
	global $wpdb;
	$table_gmgt_nutrition = $wpdb->prefix. 'gmgt_nutrition';
	$table_gmgt_nutrition_data = $wpdb->prefix. 'gmgt_nutrition_data';
	$result = $wpdb->query("DELETE FROM $table_gmgt_nutrition_data where nutrition_id= ".$work_out_id);
	$result = $wpdb->query("DELETE FROM $table_gmgt_nutrition where id = ".$work_out_id);
	die();
}
function gmgt_paymentdetail_bymembership()
{
	$membership_id = $_POST['membership_id'];
	global $wpdb;
	$gmgt_membershiptype = $wpdb->prefix.'gmgt_membershiptype';
	$sql = "SELECT * From $gmgt_membershiptype where membership_id = $membership_id";
	$result = $wpdb->get_row($sql);
	 
	$payment_detail = array();
	$payment_detail['title'] = $result->membership_label;
	//$payment_detail['membership_id'] = $result->membership_id;
	$payment_detail['price'] = $result->membership_amount;
	//$payment_detail['membership_length_id'] = $result->membership_length_id;
	echo json_encode($payment_detail);
	die();
}
function gmgt_member_add_payment()
{
	$mp_id = $_POST['idtest'];
?>
	<div class="modal-header">
			<a href="#" class="close-btn badge badge-success pull-right">X</a>
			<h4 class="modal-title"><?php echo get_option('gmgt_system_name');?></h4>
	</div>
	<div class="modal-body">
		 <form name="expense_form" action="" method="post" class="form-horizontal" id="expense_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="mp_id" value="<?php echo $mp_id;?>">
		<div class="form-group">
			<label class="col-sm-3 control-label" for="amount"><?php _e('Paid Amount','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="amount" class="form-control validate[required] text-input" type="text" value="" name="amount">
			</div>
		</div>
		<div class="form-group">
			<input type="hidden" name="payment_status" value="paid">
			<label class="col-sm-3 control-label" for="payment_method"><?php _e('Payment By','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			<?php global $current_user;
		$user_roles = $current_user->roles;
		$user_role = array_shift($user_roles);?>
				<select name="payment_method" id="payment_method" class="form-control">
					<?php if($user_role != 'member'){?>
					<option value="Cash"><?php _e('Cash','gym_mgt');?></option>
					<option value="Cheque"><?php _e('Cheque','gym_mgt');?></option>
					<option value="Bank Transfer"><?php _e('Bank Transfer','gym_mgt');?></option>
					<?php } else {?>
					<option value="Paypal"><?php _e('Paypal','gym_mgt');?></option>
					
					<?php }?>
						
			</select>
			</div>
		</div>
		<div class="col-sm-offset-2 col-sm-8">
        	 <input type="submit" value="<?php _e('Add Payment','gym_mgt');?>" name="add_fee_payment" class="btn btn-success"/>
        </div>
		</form>
	</div>
<?php
	die();
}
function gmgt_member_view_paymenthistory()
{
	$mp_id = $_REQUEST['idtest'];
	$fees_detail_result = gym_get_single_membership_payment_record($mp_id);
	$fees_history_detail_result = gym_get_payment_history_by_mpid($mp_id);
	?>
	<div class="modal-header">
			<a href="#" class="close-btn badge badge-success pull-right">X</a>
			<h4 class="modal-title"><?php echo get_option('gmgt_system_name');?></h4>
	</div>
	<div class="modal-body">
	
	<div id="invoice_print"> 
		<table width="100%" border="0">
						<tbody>
							<tr>
								<td width="70%">
									<img style="max-height:80px;" src="<?php echo get_option( 'gmgt_system_logo' ); ?>">
								</td>
								<td align="right" width="24%">
									
									<h5><?php $issue_date='DD-MM-YYYY';
												
													$issue_date=$fees_detail_result->created_date;
													
									
									echo __('Issue Date','gym_mgt')." : ".date("Y-m-d", strtotime($issue_date));?></h5>
									
						<h5><?php echo __('Status','gym_mgt')." : "; echo "<span class='btn btn-success btn-xs'>";
					echo get_membership_paymentstatus($fees_detail_result->mp_id);
					echo "</span>";?></h5>
								</td>
							</tr>
						</tbody>
					</table>
					<hr>
					<table width="100%" border="0">
						<tbody>
							<tr>
								<td align="left">
									<h4><?php _e('Payment To','gym_mgt');?> </h4>
								</td>
								<td align="right">
									<h4><?php _e('Bill To','gym_mgt');?> </h4>
								</td>
							</tr>
							<tr>
								<td valign="top" align="left">
									<?php echo get_option( 'gmgt_system_name' )."<br>"; 
									 echo get_option( 'gmgt_gym_address' ).","; 
									 echo get_option( 'gmgt_contry' )."<br>"; 
									 echo get_option( 'gmgt_contact_number' )."<br>"; 
									?>
									
								</td>
								<td valign="top" align="right">
									<?php
									$member_id=$fees_detail_result->member_id;								
										
										$patient=get_userdata($member_id);
												
										echo $patient->display_name."<br>"; 
										 echo get_user_meta( $member_id,'address',true ).","; 
										 echo get_user_meta( $member_id,'city_name',true ).","; 
										 echo get_user_meta( $member_id,'zip_code',true ).",<BR>"; 
										 echo get_user_meta( $member_id,'state_name',true ).","; 
										 echo get_option( 'gmgt_contry' ).","; 
										 echo get_user_meta( $member_id,'mobile',true )."<br>"; 
									
									?>
								</td>
							</tr>
						</tbody>
					</table>
					<hr>
					<table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
						<thead>
							<tr>
								<th class="text-center">#</th>
								<th class="text-center"> <?php _e('Fees Type','gym_mgt');?></th>
								<th><?php _e('Total','gym_mgt');?> </th>
								
							</tr>
						</thead>
						<tbody>
							<td>1</td>
							<td><?php echo get_membership_name($fees_detail_result->membership_id);?></td>
							<td><?php echo $fees_detail_result->membership_amount;?></td>
						</tbody>
						</table>
						<table width="100%" border="0">
						<tbody>
							
							<tr>
								<td width="80%" align="right"><?php _e('Sub Total :','gym_mgt');?></td>
								<td align="right"><?php echo $fees_detail_result->membership_amount;?></td>
							</tr>
							<tr>
								<td width="80%" align="right"><?php _e('Payment Made :','gym_mgt');?></td>
								<td align="right"><?php echo $fees_detail_result->paid_amount;?></td>
							</tr>
							<tr>
								<td width="80%" align="right"><?php _e('Due Amount  :','gym_mgt');?></td>
								<td align="right"><?php echo $fees_detail_result->membership_amount - $fees_detail_result->paid_amount;?></td>
							</tr>
							
						</tbody>
					</table>
					<hr>
					<?php if(!empty($fees_history_detail_result))
					{?>
					<h4><?php _e('Payment History','gym_mgt');?></h4>
					<table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
					<thead>
							<tr>
								<th class="text-center"><?php _e('Date','gym_mgt');?></th>
								<th class="text-center"> <?php _e('Amount','gym_mgt');?></th>
								<th><?php _e('Method','gym_mgt');?> </th>
								
							</tr>
						</thead>
						<tbody>
							<?php 
							foreach($fees_history_detail_result as  $retrive_date)
							{
							?>
							<tr>
							<td><?php echo $retrive_date->paid_by_date;?></td>
							<td><?php echo $retrive_date->amount;?></td>
							<td><?php echo $retrive_date->payment_method;?></td>
							</tr>
							<?php }?>
						</tbody>
					</table>
					<?php }?>
	</div>
	</div>
	<?php
	die();
}
function gmgt_check_membership($userid)
{
	$obj_membership=new Gmgtmembership;
	$membershipid=get_user_meta($userid,'membership_id',true);
	$membershistatus=get_user_meta($userid,'membership_status',true);
	$joiningdate=get_user_meta($userid,'begin_date',true);
	$autorenew=get_user_meta($userid,'auto_renew',true);
	$membership=$obj_membership->get_single_membership($membershipid);
	
	$validity=$membership->membership_length_id;
	$expiredate="";
	$today = date("Y-m-d");
	 $expiredate= date('Y-m-d', strtotime($joiningdate. ' + '.$validity.' days'));
	if($membershistatus!="Dropped"){
		if($today < $expiredate){
			$returnans=update_user_meta( $userid, 'membership_status','Continue');		 
			 return $expiredate;
		 }	 
		 elseif($autorenew=="Yes")
		 {
			 $returnans=update_user_meta( $userid, 'begin_date',$today );
			  $bigindate=get_user_meta($userid,'begin_date',true);
			return $expiredate= date('Y-m-d', strtotime($bigindate. ' + '.$validity.' days'));
		 }
		 else
		 {
			  $returnans=update_user_meta( $userid, 'membership_status','Active');
			  return $expiredate;
		 }
	}
	else
	{
		return $expiredate;
	}
		 
	 
}

function gmgt_view_member_attendance($start_date,$end_date,$user_id)
{
	
	global $wpdb;
	$tbl_name = $wpdb->prefix .'gmgt_attendence';
	
	$result =$wpdb->get_results("SELECT *  FROM $tbl_name where user_id=$user_id AND role_name = 'member' and attendence_date between '$start_date' and '$end_date'");
	return $result;
}
function gmgt_get_attendence($userid,$curr_date)
{
	global $wpdb;
	$table_name = $wpdb->prefix . "gmgt_attendence";
	
	$result=$wpdb->get_var("SELECT status FROM $table_name WHERE attendence_date='$curr_date'  and user_id=$userid");
	return $result;

}
function get_current_userclass($id)
{
	global $wpdb;
	$table_name = $wpdb->prefix .'gmgt_class_schedule';
	$result =$wpdb->get_results("SELECT *  FROM $table_name where staff_id=$id OR asst_staff_id =$id");
	return $result;
}
function gmgt_get_inbox_message($user_id,$p=0,$lpm1=10)
{
	
	global $wpdb;
	$tbl_name = $wpdb->prefix .'gmgt_message';
	
	$inbox =$wpdb->get_results("SELECT *  FROM $tbl_name where receiver = $user_id limit $p , $lpm1");
	return $inbox;
}
function gmgt_admininbox_pagination($totalposts,$p,$lpm1,$prev,$next)
{
	$adjacents = 1;
	$page_order = "";
	$pagination = "";
	$form_id = 1;
	if(isset($_REQUEST['form_id']))
		$form_id=$_REQUEST['form_id'];
	if(isset($_GET['orderby']))
	{
		$page_order='&orderby='.$_GET['orderby'].'&order='.$_GET['order'];
	}
	if($totalposts > 1)
	{
		$pagination .= '<div class="btn-group">';
		
		if ($p > 1)
			$pagination.= "<a href=\"?page=smgt_message&tab=inbox&pg=$prev\" class=\"btn btn-default\"><i class=\"fa fa-angle-left\"></i></a> ";
		else
			$pagination.= "<a class=\"btn btn-default disabled\"><i class=\"fa fa-angle-left\"></i></a> ";

		if ($p < $totalposts)
			$pagination.= " <a href=\"?page=smgt_message&tab=inbox&pg=$next\" class=\"btn btn-default next-page\"><i class=\"fa fa-angle-right\"></i></a>";
		else
			$pagination.= " <a class=\"btn btn-default disabled\"><i class=\"fa fa-angle-right\"></i></a>";
		$pagination.= "</div>\n";
	}
	return $pagination;
}
function gym_get_display_name($user_id) {
	if (!$user = get_userdata($user_id))
		return false;
	return $user->data->display_name;
}
function gmgt_get_all_user_in_message()
{
	$staff_member = get_users(array('role'=>'staff_member'));
	$accountant = get_users(array('role'=>'accountant'));
	$member = get_users(array('role'=>'member'));
	
	$obj_gym = new Gym_management(get_current_user_id());
	
	
	
	
	$all_user = array('member'=>$member,
			'staff_member'=>$staff_member,
			'accountant'=>$accountant,
			
	);
	$return_array = array();
	
	foreach($all_user as $key => $value)
	{
		if(!empty($value))
		{
		 echo '<optgroup label="'.$key.'" style = "text-transform: capitalize;">';
		 foreach($value as $user)
		 {
		 	/* if($key == 'member' && $obj_gym->role == 'accountant')
		 	{
		 		foreach($user as $student_class)
		 		{
		 			//echo $student_class->ID;
		 			echo '<option value="'.$student_class->ID.'">'.$student_class->display_name.'</option>';
		 		}
		 	}
		 	else  */
		 	echo '<option value="'.$user->ID.'">'.$user->display_name.'</option>';
		 }
		}
	}
}
function gmgt_get_allclass(){
	
	
	global $wpdb;
	$table_name = $wpdb->prefix .'gmgt_class_schedule';
	
	return $classdata =$wpdb->get_results("SELECT * FROM $table_name", ARRAY_A);
	//print_r($classdata);
}
function gmgt_get_user_notice($role,$class_id)
{
	if($role == 'all' )
	{
		$userdata = array();
		$roles = array('member', 'staff_member', 'accountant');
		
		foreach ($roles as $role) :
		$users_query = new WP_User_Query( array(
				'fields' => 'all_with_meta',
				'role' => $role,
				'orderby' => 'display_name'
		));
		$results = $users_query->get_results();
		if ($results) $userdata = array_merge($userdata, $results);
		endforeach;
	}
	else 
	{
		if($class_id == 'all')
			$userdata=get_users(array('role'=>$role));
		else
			$userdata=get_users(array('role'=>$role,'meta_key' => 'class_id', 'meta_value' => $class_id));
	}
		
	return $userdata;
}
function gmgt_insert_record($tablenm,$records)
{
	global $wpdb;
	$table_name = $wpdb->prefix . $tablenm;
	return $result=$wpdb->insert( $table_name, $records);
	
}
function gmgt_pagination($totalposts,$p,$lpm1,$prev,$next){
	$adjacents = 1;
	$page_order = "";
	$pagination = "";
	$form_id = 1;
	if(isset($_REQUEST['form_id']))
		$form_id=$_REQUEST['form_id'];
	if(isset($_GET['orderby']))
	{
		$page_order='&orderby='.$_GET['orderby'].'&order='.$_GET['order'];
	}
	if($totalposts > 1)
	{
		$pagination .= '<div class="btn-group">';
		
		if ($p > 1)
			$pagination.= "<a href=\"?page=smgt_message&tab=sentbox&form_id=$form_id&pg=$prev$page_order\" class=\"btn btn-default\"><i class=\"fa fa-angle-left\"></i></a> ";
		else
			$pagination.= "<a class=\"btn btn-default disabled\"><i class=\"fa fa-angle-left\"></i></a> ";
		
		if ($p < $totalposts)
			$pagination.= " <a href=\"?page=smgt_message&tab=sentbox&form_id=$form_id&pg=$next\" class=\"btn btn-default next-page\"><i class=\"fa fa-angle-right\"></i></a>";
		else
			$pagination.= " <a class=\"btn btn-default disabled\"><i class=\"fa fa-angle-right\"></i></a>";
		$pagination.= "</div>\n";
	}
	return $pagination;
}
function gmgt_count_send_item($id)
{
	global $wpdb;
	$posts = $wpdb->prefix."posts";
	$total =$wpdb->get_var("SELECT Count(*) FROM ".$posts." Where post_type = 'message' AND post_author = $id");
	return $total;
}
function gmgt_get_send_message($user_id,$max=10,$offset=0)
{
	
	global $wpdb;
	$tbl_name = $wpdb->prefix .'gmgt_message';
	
	$obj_gym = new Gym_management($user_id);
	
	if(is_admin() || $obj_gym->role=='staff_member' || $obj_gym->role=='accountant')
	{
		
		$args['post_type'] = 'message';
		$args['posts_per_page'] =$max;
		$args['offset'] = $offset;
		$args['post_status'] = 'public';
		$args['author'] = $user_id;
		
		$q = new WP_Query();
		$sent_message = $q->query( $args );
	
	}
	else 
	{
		$sent_message =$wpdb->get_results("SELECT *  FROM $tbl_name where sender = $user_id ");
	}
	return $sent_message;
}
function gmgt_get_emailid_byuser_id($id)
{
	if (!$user = get_userdata($id))
		return false;
	return $user->data->user_email;
}
function gmgt_count_inbox_item($id)
{
	global $wpdb;
	$tbl_name = $wpdb->prefix .'gmgt_message';
	$inbox =$wpdb->get_results("SELECT *  FROM $tbl_name where receiver = $id");
	return $inbox;
}
function gmgt_inbox_pagination($totalposts,$p,$lpm1,$prev,$next)
{
	$adjacents = 1;
	$page_order = "";
	$pagination = "";
	$form_id = 1;
	if(isset($_REQUEST['form_id']))
		$form_id=$_REQUEST['form_id'];
	if(isset($_GET['orderby']))
	{
		$page_order='&orderby='.$_GET['orderby'].'&order='.$_GET['order'];
	}
	if($totalposts > 1)
	{
		$pagination .= '<div class="btn-group">';
		
		if ($p > 1)
			$pagination.= "<a href=\"?dashboard=user&page=message&tab=inbox&pg=$prev\" class=\"btn btn-default\"><i class=\"fa fa-angle-left\"></i></a> ";
		else
			$pagination.= "<a class=\"btn btn-default disabled\"><i class=\"fa fa-angle-left\"></i></a> ";
	
		if ($p < $totalposts)
			$pagination.= " <a href=\"?dashboard=user&page=message&tab=inbox&pg=$next\" class=\"btn btn-default next-page\"><i class=\"fa fa-angle-right\"></i></a>";
		else
			$pagination.= " <a class=\"btn btn-default disabled\"><i class=\"fa fa-angle-right\"></i></a>";
		$pagination.= "</div>\n";
	}
	return $pagination;
}
function gmgt_get_message_by_id($id)
{
	global $wpdb;
	$table_name = $wpdb->prefix . "gmgt_message";
	return $retrieve_subject = $wpdb->get_row( "SELECT * FROM $table_name WHERE message_id=".$id);

}
function gmgt_fronted_sentbox_pagination($totalposts,$p,$lpm1,$prev,$next){
	$adjacents = 1;
	$page_order = "";
	$pagination = "";
	$form_id = 1;
	if(isset($_REQUEST['form_id']))
		$form_id=$_REQUEST['form_id'];
	if(isset($_GET['orderby']))
	{
		$page_order='&orderby='.$_GET['orderby'].'&order='.$_GET['order'];
	}
	if($totalposts > 1)
	{
		$pagination .= '<div class="btn-group">';
		
		if ($p > 1)
			$pagination.= "<a href=\"?dashboard=user&page=message&tab=sentbox&pg=$prev$page_order\" class=\"btn btn-default\"><i class=\"fa fa-angle-left\"></i></a> ";
		else
			$pagination.= "<a class=\"btn btn-default disabled\"><i class=\"fa fa-angle-left\"></i></a> ";

		if ($p < $totalposts)
			$pagination.= " <a href=\"?dashboard=user&page=message&tab=sentbox&pg=$next\" class=\"btn btn-default next-page\"><i class=\"fa fa-angle-right\"></i></a>";
		else
			$pagination.= " <a class=\"btn btn-default disabled\"><i class=\"fa fa-angle-right\"></i></a>";
		$pagination.= "</div>\n";
	}
	return $pagination;
}
function get_userworkout($id)
{
	global $wpdb;
	$workouttable = $wpdb->prefix."gmgt_assign_workout";
	$workoutdata =$wpdb->get_results("SELECT *FROM ".$workouttable." Where user_id = $id");
	return $workoutdata;
}

function get_workoutdata($id)
{
	
	global $wpdb;
	$workouttable = $wpdb->prefix."gmgt_workout_data";
	$workoutdata =$wpdb->get_results("SELECT *FROM ".$workouttable." Where workout_id = $id");
	
	return $workoutdata;
	
}



function set_workoutarray($data)
{
	$workout_array=array();
	foreach($data as $row)
	{
		$workout_array[$row->day_name][]= "<span class='col-md-3 col-sm-3 col-xs-12'>".$row->workout_name."</span>   
			<span class='col-md-3 col-sm-3 col-xs-6'>".$row->sets." "._('Sets','gym_mgt')."</span>
		<span class='col-md-2 col-sm-2 col-xs-6'> ".$row->reps." "._('Reps','gym_mgt')."</span>
			<span class='col-md-2 col-sm-2 col-xs-6'> ".$row->kg." "._('KG','gym_mgt')."</span>
		<span class='col-md-2 col-sm-2  col-xs-6'> ".$row->time." "._('Min','gym_mgt')."</span>";
	}
	return $workout_array;
	
}
function check_user_workouts($id,$date)
{
	global $wpdb;
	$workouttable = $wpdb->prefix."gmgt_daily_workouts";
	$count_rec =$wpdb->get_var("SELECT COUNT(*) FROM ".$workouttable." Where member_id = $id AND record_date='$date'");
	return $count_rec;
}
function get_user_nutrition($id)
{
	global $wpdb;
	$workouttable = $wpdb->prefix."gmgt_nutrition";
	$workoutdata =$wpdb->get_results("SELECT *FROM ".$workouttable." Where user_id = $id");
	return $workoutdata;
}

function get_nutritiondata($id)
{

	global $wpdb;
	$workouttable = $wpdb->prefix."gmgt_nutrition_data";
	$workoutdata =$wpdb->get_results("SELECT *FROM ".$workouttable." Where nutrition_id = $id");

	return $workoutdata;

}
function set_nutrition_array($data)
{
	$workout_array=array();
	foreach($data as $row)
	{
		$workout_array[$row->day_name][]= "<span class='col-md-3 col-sm-3 col-xs-12 nutrition_time'>".$row->nutrition_time."</span>
			<span class='col-md-9 col-sm-9 col-xs-12'>".$row->nutrition_value." </span>";
		
	}
	return $workout_array;

}
?>