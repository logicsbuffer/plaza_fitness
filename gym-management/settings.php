<?php 
require_once GMS_PLUGIN_DIR. '/gmgt_function.php';
require_once GMS_PLUGIN_DIR. '/class/membership.php';
require_once GMS_PLUGIN_DIR. '/class/group.php';
require_once GMS_PLUGIN_DIR. '/class/member.php';
require_once GMS_PLUGIN_DIR. '/class/class_schedule.php';
require_once GMS_PLUGIN_DIR. '/class/product.php';
require_once GMS_PLUGIN_DIR. '/class/store.php';
require_once GMS_PLUGIN_DIR. '/class/reservation.php';
require_once GMS_PLUGIN_DIR. '/class/attendence.php';
require_once GMS_PLUGIN_DIR. '/class/membership_payment.php';
require_once GMS_PLUGIN_DIR. '/class/payment.php';
require_once GMS_PLUGIN_DIR. '/class/activity.php';
require_once GMS_PLUGIN_DIR. '/class/workout_type.php';
require_once GMS_PLUGIN_DIR. '/class/workout.php';
require_once GMS_PLUGIN_DIR. '/class/notice.php';
require_once GMS_PLUGIN_DIR. '/class/nutrition.php';
require_once GMS_PLUGIN_DIR. '/class/MailChimp.php';
require_once GMS_PLUGIN_DIR. '/class/MCAPI.class.php';
require_once GMS_PLUGIN_DIR. '/class/gym-management.php';
require_once GMS_PLUGIN_DIR. '/class/dashboard.php';
require_once GMS_PLUGIN_DIR. '/lib/phpAutocomplete_Lite/conf.php';
require_once GMS_PLUGIN_DIR. '/lib/paypal/paypal_class.php';


add_action( 'admin_head', 'gmgt_admin_css' );

function gmgt_admin_css(){
	?>
     <style>
      a.toplevel_page_gym_system:hover,  a.toplevel_page_gym_system:focus,.toplevel_page_gym_system.opensub a.wp-has-submenu{
  background: url("<?php echo GMS_PLUGIN_URL;?>/assets/images/gym-2.png") no-repeat scroll 8px 9px rgba(0, 0, 0, 0) !important;
  
}
.toplevel_page_gym_system:hover .wp-menu-image.dashicons-before img {
  display: none;
}

.toplevel_page_gym_system:hover .wp-menu-image.dashicons-before {
  min-width: 23px !important;
}
    
     </style>
<?php
}



if ( is_admin() ){
	require_once GMS_PLUGIN_DIR. '/admin/admin.php';
	function gym_install()
	{
			
			add_role('staff_member', __( 'Trainer' ,'gym_mgt'),array( 'read' => true, 'level_1' => true ));
			add_role('accountant', __( 'Gym Owner' ,'gym_mgt'),array( 'read' => true, 'level_1' => true ));
			add_role('member', __( 'Member' ,'gym_mgt'),array( 'read' => true, 'level_0' => true ));
			
			//gmgt_register_post();
			gmgt_install_tables();			
	}
	register_activation_hook(GMS_PLUGIN_BASENAME, 'gym_install' );

	function gmgt_option(){
		$options=array("gmgt_system_name"=> __( 'Gym Management System' ,'gym_mgt'),
					"gmgt_staring_year"=>"2015",
					"gmgt_gym_address"=>"",
					"gmgt_contact_number"=>"9999999999",
					"gmgt_contry"=>"India",
					"gmgt_email"=>get_option('admin_email'),
					"gmgt_system_logo"=>plugins_url( 'gym-management/assets/images/Thumbnail-img.png' ),
					"gmgt_gym_background_image"=>plugins_url('gym-management/assets/images/gym-background.png' ),
					"gmgt_instructor_thumb"=>plugins_url( 'gym-management/assets/images/useriamge/instructor.png' ),
					"gmgt_member_thumb"=>plugins_url( 'gym-management/assets/images/useriamge/member.png' ),
					
					"gmgt_assistant_thumb"=>plugins_url( 'gym-management/assets/images/useriamge/assistant.png' ),
					
					"gmgt_accountant_thumb"=>plugins_url( 'gym-management/assets/images/useriamge/accountant.png' ),
					
					"gmgt_mailchimp_api"=>"",
					"gmgt_sms_service"=>"",
					"gmgt_sms_service_enable"=> 0,					
					"gmgt_clickatell_sms_service"=>array(),
					"gmgt_twillo_sms_service"=>array(),
					"gmgt_weight_unit"=>'KG',
					"gmgt_height_unit"=>'Centemeter',
					"gmgt_chest_unit"=>'Inches',
					"gmgt_waist_unit"=>'Inches',
					"gmgt_thigh_unit"=>'Inches',
					"gmgt_arms_unit"=>'Inches',
					"gmgt_fat_unit"=>'Percentage',
					"gmgt_paypal_email"=>'',
					"gym_enable_sandbox"=>'yes',
					"gmgt_currency_code" => 'USD',
					"gym_enable_memberlist_for_member" => 'yes'
					
		);
		return $options;
	}
	add_action('admin_init','gmgt_general_setting');	
	function gmgt_general_setting()
	{
		$options=gmgt_option();
		foreach($options as $key=>$val)
		{
			add_option($key,$val); 
			
		}
	}
	
function gmgt_call_script_page()
{
	$page_array = array('gym_system','gmgt_membership_type','gmgt_group','gmgt_staff','gmgt_accountant','gmgt_class','gmgt_member',
			'gmgt_product','gmgt_reservation','gmgt_attendence','gmgt_fees_payment','gmgt_payment','gmgt_message','gmgt_newsletter','gmgt_activity',
			'gmgt_notice','gmgt_workouttype','gmgt_workout','gmgt_store','gmgt_nutrition','gmgt_report','gmgt_gnrl_settings');
	return  $page_array;
}


function gmgt_change_adminbar_css($hook) {	
				$current_page = $_REQUEST['page'];
				$page_array = gmgt_call_script_page();
				if(in_array($current_page,$page_array))
		{
				wp_register_script( 'jquery-1.8.2', plugins_url( '/assets/js/jquery-1.11.1.min.js', __FILE__), array( 'jquery' ) );
			 	wp_enqueue_script( 'jquery-1.8.2' );		
				wp_enqueue_style( 'accordian-jquery-ui-css', plugins_url( '/assets/accordian/jquery-ui.css', __FILE__) );		
				wp_enqueue_script('accordian-jquery-ui', plugins_url( '/assets/accordian/jquery-ui.js',__FILE__ ));
			
				wp_enqueue_style( 'gmgt  -calender-css', plugins_url( '/assets/css/fullcalendar.css', __FILE__) );
				wp_enqueue_style( 'gmgt  -datatable-css', plugins_url( '/assets/css/dataTables.css', __FILE__) );
				wp_enqueue_style( 'gmgt-dataTables.responsive-css', plugins_url( '/assets/css/dataTables.responsive.css', __FILE__) );
				//wp_enqueue_style( 'gmgt  -admin-style-css', plugins_url( '/admin/css/admin-style.css', __FILE__) );
				wp_enqueue_style( 'gmgt-style-css', plugins_url( '/assets/css/style1.css', __FILE__) );
				wp_enqueue_style( 'gmgt-popup-css', plugins_url( '/assets/css/popup.css', __FILE__) );
				wp_enqueue_style( 'gmgt-custom-css', plugins_url( '/assets/css/custom.css', __FILE__) );
				wp_enqueue_style( 'gmgt-select2-css', plugins_url( '/lib/select2-3.5.3/select2.css', __FILE__) );
				
				wp_enqueue_script('gmgt-select2', plugins_url( '/lib/select2-3.5.3/select2.min.js', __FILE__ ), array( 'jquery' ), '4.1.1', true );
				wp_enqueue_script('gmgt  -calender_moment', plugins_url( '/assets/js/moment.min.js', __FILE__ ), array( 'jquery' ), '4.1.1', true );
				wp_enqueue_script('gmgt  -calender', plugins_url( '/assets/js/fullcalendar.min.js', __FILE__ ), array( 'jquery' ), '4.1.1', true );
				wp_enqueue_script('gmgt  -datatable', plugins_url( '/assets/js/jquery.dataTables.min.js',__FILE__ ), array( 'jquery' ), '4.1.1', true);
				wp_enqueue_script('gmgt  -datatable-tools', plugins_url( '/assets/js/dataTables.tableTools.min.js',__FILE__ ), array( 'jquery' ), '4.1.1', true);
				wp_enqueue_script('gmgt  -datatable-editor', plugins_url( '/assets/js/dataTables.editor.min.js',__FILE__ ), array( 'jquery' ), '4.1.1', true);	
				wp_enqueue_script('gmgt-dataTables.responsive-js', plugins_url( '/assets/js/dataTables.responsive.js',__FILE__ ), array( 'jquery' ), '4.1.1', true);	
				wp_enqueue_script('gmgt-customjs', plugins_url( '/assets/js/gmgt_custom.js', __FILE__ ), array( 'jquery' ), '4.1.1', true );
			
			
			
				wp_enqueue_script('gmgt-popup', plugins_url( '/assets/js/popup.js', __FILE__ ), array( 'jquery' ), '4.1.1', false );
				wp_localize_script( 'gmgt-popup', 'gmgt  ', array( 'ajax' => admin_url( 'admin-ajax.php' ) ) );
			 	wp_enqueue_script('jquery');
			 	wp_enqueue_media();
		       	wp_enqueue_script('thickbox');
		       	wp_enqueue_style('thickbox');
		 
		      
			 	wp_enqueue_script('gmgt -image-upload', plugins_url( '/assets/js/image-upload.js', __FILE__ ), array( 'jquery' ), '4.1.1', true );
			 
			
				wp_enqueue_style( 'gmgt  -bootstrap-css', plugins_url( '/assets/css/bootstrap.min.css', __FILE__) );
				wp_enqueue_style( 'gmgt  -bootstrap-multiselect-css', plugins_url( '/assets/css/bootstrap-multiselect.css', __FILE__) );
				wp_enqueue_style( 'gmgt  -bootstrap-timepicker-css', plugins_url( '/assets/css/bootstrap-timepicker.min.css', __FILE__) );
			 	wp_enqueue_style( 'gmgt  -font-awesome-css', plugins_url( '/assets/css/font-awesome.min.css', __FILE__) );
			 	wp_enqueue_style( 'gmgt  -white-css', plugins_url( '/assets/css/white.css', __FILE__) );
			 	wp_enqueue_style( 'gmgt-gymmgt-min-css', plugins_url( '/assets/css/gymmgt.min.css', __FILE__) );
				 if (is_rtl())
				 {
					wp_enqueue_style( 'gmgt-bootstrap-rtl-css', plugins_url( '/assets/css/bootstrap-rtl.min.css', __FILE__) );
					
				 }
				 wp_enqueue_style( 'gmgt-gym-responsive-css', plugins_url( '/assets/css/gym-responsive.css', __FILE__) );
			  
			 	wp_enqueue_script('gmgt  -bootstrap-js', plugins_url( '/assets/js/bootstrap.min.js', __FILE__ ) );
			 	wp_enqueue_script('gmgt  -bootstrap-multiselect-js', plugins_url( '/assets/js/bootstrap-multiselect.js', __FILE__ ) );
			 	wp_enqueue_script('gmgt  -bootstrap-timepicker-js', plugins_url( '/assets/js/bootstrap-timepicker.min.js', __FILE__ ) );
			 	wp_enqueue_script('gmgt-gym-js', plugins_url( '/assets/js/gymjs.js', __FILE__ ) );
			 	
			 	//Validation style And Script
			 	
			 	//validation lib
				
			 	wp_enqueue_style( 'wcwm-validate-css', plugins_url( '/lib/validationEngine/css/validationEngine.jquery.css', __FILE__) );	 	
			 	wp_register_script( 'jquery-validationEngine-en', plugins_url( '/lib/validationEngine/js/languages/jquery.validationEngine-en.js', __FILE__), array( 'jquery' ) );
			 	wp_enqueue_script( 'jquery-validationEngine-en' );
			 	wp_register_script( 'jquery-validationEngine', plugins_url( '/lib/validationEngine/js/jquery.validationEngine.js', __FILE__), array( 'jquery' ) );
			 	wp_enqueue_script( 'jquery-validationEngine' );
				
				
			 	
			 
			 
		}
		
	}
	if(isset($_REQUEST['page']))
	add_action( 'admin_enqueue_scripts', 'gmgt_change_adminbar_css' );
}






function hmgt_remove_all_theme_styles() {
	global $wp_styles;
	$wp_styles->queue = array();
}
if(isset($_REQUEST['dashboard']) && $_REQUEST['dashboard'] == 'user')
{
add_action('wp_print_styles', 'hmgt_remove_all_theme_styles', 100);
}

function gmgt_load_script1()
{
	if(isset($_REQUEST['dashboard']) && $_REQUEST['dashboard'] == 'user')
	{
	
	
	wp_register_script('gmgt  -popup-front', plugins_url( 'assets/js/popup.js', __FILE__ ), array( 'jquery' ));
	wp_enqueue_script('gmgt  -popup-front');
	
	wp_localize_script( 'gmgt  -popup-front', 'gmgt  ', array( 'ajax' => admin_url( 'admin-ajax.php' ) ) );
	 wp_enqueue_script('jquery');
	
	}

}

function gmgt_domain_load(){
	load_plugin_textdomain( 'gym_mgt', false, dirname( plugin_basename( __FILE__ ) ). '/languages/' );
}
function gmgt_install_login_page()
{
	
	if ( !get_option('gmgt_login_page') ) {
		

		$curr_page = array(
				'post_title' => __('Gym Management Login Page', 'gym_mgt'),
				'post_content' => '[gmgt_login]',
				'post_status' => 'publish',
				'post_type' => 'page',
				'comment_status' => 'closed',
				'ping_status' => 'closed',
				'post_category' => array(1),
				'post_parent' => 0 );
		

		$curr_created = wp_insert_post( $curr_page );

		update_option( 'gmgt_login_page', $curr_created );

	}
	
}
function gmgt_user_dashboard()
{
	
	if(isset($_REQUEST['dashboard']))
	{
		
		require_once GMS_PLUGIN_DIR. '/fronted_template.php';
		exit;
	}
	
}

add_action( 'plugins_loaded', 'gmgt_domain_load' );
add_action('wp_enqueue_scripts','gmgt_load_script1');
add_action('init','gmgt_install_login_page');
add_shortcode( 'gmgt_login','gmgt_login_link' );
add_action('wp_head','gmgt_user_dashboard');

add_action('init','gmgt_output_ob_start');
function gmgt_output_ob_start()
{
	ob_start();
}

//Inatall Table
function gmgt_install_tables()
{
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	global $wpdb;
	

	
	
	$table_gmgt_activity = $wpdb->prefix . 'gmgt_activity';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_activity ." (
				  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
				  `activity_cat_id` int(11) NOT NULL,
				  `activity_title` varchar(200) NOT NULL,
				  `activity_assigned_to` int(11) NOT NULL,
				  `activity_added_by` int(11) NOT NULL,
				  `activity_added_date` date NOT NULL,
				  PRIMARY KEY (`activity_id`)
				) DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		$table_gmgt_assign_workout = $wpdb->prefix . 'gmgt_assign_workout';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_assign_workout." (
				  `workout_id` bigint(20) NOT NULL AUTO_INCREMENT,
				  `user_id` bigint(20) NOT NULL,
				  `start_date` date NOT NULL,
				  `end_date` date NOT NULL,
				  `level_id` int(11) NOT NULL,
				  `description` text NOT NULL,
				  `created_date` datetime NOT NULL,
				  `created_by` bigint(20) NOT NULL,
				  PRIMARY KEY (`workout_id`)
				) DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		$table_gmgt_attendence = $wpdb->prefix . 'gmgt_attendence';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_attendence." (
				 `attendence_id` int(11) NOT NULL AUTO_INCREMENT,
				  `user_id` int(11) NOT NULL,
				  `class_id` int(11) NOT NULL,
				  `attendence_date` date NOT NULL,
				  `status` varchar(50) NOT NULL,
				  `attendence_by` int(11) NOT NULL,
				  `role_name` varchar(50) NOT NULL,
				  PRIMARY KEY (`attendence_id`)
				) DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		
		$table_gmgt_class_schedule = $wpdb->prefix . 'gmgt_class_schedule';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_class_schedule." (
				 `class_id` int(11) NOT NULL AUTO_INCREMENT,
				  `class_name` varchar(100) NOT NULL,
				  `day` text NOT NULL,
				  `staff_id` int(11) NOT NULL,
				  `asst_staff_id` int(11) NOT NULL,
				  `start_time` varchar(20) NOT NULL,
				  `end_time` varchar(20) NOT NULL,
				  `class_created_id` int(11) NOT NULL,
				  `class_creat_date` date NOT NULL,
				  PRIMARY KEY (`class_id`)
				) DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		
		$table_gmgt_daily_workouts = $wpdb->prefix . 'gmgt_daily_workouts';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_daily_workouts." (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `workout_id` int(11) NOT NULL,
				  `member_id` int(11) NOT NULL,
				  `record_date` date NOT NULL,
				  `result_measurment` varchar(50) NOT NULL,
				  `result` varchar(100) NOT NULL,
				  `duration` varchar(100) NOT NULL,
				  `assigned_by` int(11) NOT NULL,
				  `due_date` date NOT NULL,
				  `time_of_workout` varchar(50) NOT NULL,
				  `status` varchar(100) NOT NULL,
				  `note` text NOT NULL,
				  `created_by` int(11) NOT NULL,
				  `created_date` date NOT NULL,
				  PRIMARY KEY (`id`)
				)  DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		
		$table_gmgt_groups = $wpdb->prefix . 'gmgt_groups';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_groups." (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `group_name` varchar(100) NOT NULL,
				  `gmgt_groupimage` varchar(255) NOT NULL,
				  `created_by` int(11) NOT NULL,
				  `created_date` date NOT NULL,
				  PRIMARY KEY (`id`)
				) DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		$table_gmgt_groupmember = $wpdb->prefix . 'gmgt_groupmember';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_groupmember." (
				  `id` bigint(20) NOT NULL AUTO_INCREMENT,
				  `group_id` int(11) NOT NULL,
				  `member_id` int(11) NOT NULL,
				  `created_by` int(11) NOT NULL,
				  `created_date` datetime NOT NULL,
				  PRIMARY KEY (`id`)
				) DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		$table_gmgt_income_expense = $wpdb->prefix . 'gmgt_income_expense';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_income_expense." (
				  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
				  `invoice_type` varchar(100) NOT NULL,
				  `invoice_label` varchar(100) NOT NULL,
				  `supplier_name` varchar(100) NOT NULL,
				  `entry` text NOT NULL,
				  `payment_status` varchar(50) NOT NULL,
				  `receiver_id` int(11) NOT NULL,
				  `invoice_date` date NOT NULL,
				  PRIMARY KEY (`invoice_id`)
				)  DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		
		$table_gmgt_membershiptype= $wpdb->prefix . 'gmgt_membershiptype';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_membershiptype." (
				  `membership_id` int(11) NOT NULL AUTO_INCREMENT,
				  `membership_label` varchar(100) NOT NULL,
				  `membership_cat_id` int(11) NOT NULL,
				  `membership_length_id` int(11) NOT NULL,
				  `membership_class_limit` varchar(20) NOT NULL,
				  `install_plan_id` int(11) NOT NULL,
				  `membership_amount` double NOT NULL,
				  `installment_amount` double NOT NULL,
				  `signup_fee` double NOT NULL,
				  `gmgt_membershipimage` varchar(255) NOT NULL,
				  `created_date` date NOT NULL,
				  `created_by_id` int(11) NOT NULL,
				  PRIMARY KEY (`membership_id`)
				)  DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		
		
		$table_gmgt_nutrition = $wpdb->prefix . 'gmgt_nutrition';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_nutrition." (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `user_id` int(11) NOT NULL,
				  `day` varchar(50) NOT NULL,
				  `breakfast` text NOT NULL,
				  `midmorning_snack` text NOT NULL,
				  `lunch` text NOT NULL,
				  `afternoon_snack` text NOT NULL,
				  `dinner` text NOT NULL,
				  `afterdinner_snack` text NOT NULL,
				  `start_date` varchar(20) NOT NULL,
				  `expire_date` varchar(20) NOT NULL,
				  `created_by` int(11) NOT NULL,
				  `created_date` date NOT NULL,
				  PRIMARY KEY (`id`)
				)DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		$table_gmgt_payment = $wpdb->prefix . 'gmgt_payment';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_payment." (
				 `payment_id` int(11) NOT NULL AUTO_INCREMENT,
				  `title` varchar(100) NOT NULL,
				  `member_id` int(11) NOT NULL,
				  `due_date` date NOT NULL,
				  `unit_price` double NOT NULL,
				  `discount` double NOT NULL,
				  `total_amount` double NOT NULL,
				  `payment_status` varchar(50) NOT NULL,
				  `payment_date` date NOT NULL,
				  `receiver_id` int(11) NOT NULL,
				  `description` text NOT NULL,
				  PRIMARY KEY (`payment_id`)
				)DEFAULT CHARSET=utf8";
					
		$wpdb->query($sql);
		
		
		$table_gmgt_product = $wpdb->prefix . 'gmgt_product';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_product." (
				 `id` int(11) NOT NULL AUTO_INCREMENT,
				  `product_name` varchar(100) NOT NULL,
				  `price` double NOT NULL,
				  `quentity` int(11) NOT NULL,
				  `created_by` int(11) NOT NULL,
				  `created_date` date NOT NULL,
				  PRIMARY KEY (`id`)
				)DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		
		$table_gmgt_reservation = $wpdb->prefix . 'gmgt_reservation';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_reservation." (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `event_name` varchar(100) NOT NULL,
				  `event_date` date NOT NULL,
				  `start_time` varchar(20) NOT NULL,
				  `end_time` varchar(20) NOT NULL,
				  `place_id` int(11) NOT NULL,
				  `created_by` int(11) NOT NULL,
				  `created_date` date NOT NULL,
				  PRIMARY KEY (`id`)
				)DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		$table_gmgt_store = $wpdb->prefix . 'gmgt_store';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_store."(
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `member_id` int(11) NOT NULL,
				  `sell_date` date NOT NULL,
				  `product_id` int(11) NOT NULL,
				  `price` double NOT NULL,
				  `quentity` int(11) NOT NULL,
				  `sell_by` int(11) NOT NULL,
				  PRIMARY KEY (`id`)
				) DEFAULT CHARSET=utf8";
					
		$wpdb->query($sql);
		
		
		
		$table_gmgt_message= $wpdb->prefix . 'gmgt_message';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_message." (
			  `message_id` int(11) NOT NULL AUTO_INCREMENT,
			  `sender` int(11) NOT NULL,
			  `receiver` int(11) NOT NULL,
			  `date` datetime NOT NULL,
			  `subject` varchar(150) NOT NULL,
			  `message_body` text NOT NULL,
			  `status` int(11) NOT NULL,
			  PRIMARY KEY (`message_id`)
			)DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		$table_gmgt_workout_data= $wpdb->prefix . 'gmgt_workout_data';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_workout_data." (
			  `id` bigint(20) NOT NULL AUTO_INCREMENT,
			  `day_name` varchar(15) NOT NULL,
			  `workout_name` varchar(100) NOT NULL,
			  `sets` int(11) NOT NULL,
			  `reps` int(11) NOT NULL,
			  `kg` float NOT NULL,
			  `time` int(11) NOT NULL,
			  `workout_id` bigint(20) NOT NULL,
			  `created_date` datetime NOT NULL,
			  `create_by` bigint(20) NOT NULL,
			  PRIMARY KEY (`id`)
			)DEFAULT CHARSET=utf8";
	
		$wpdb->query($sql);
		
		$table_gmgt_measurment= $wpdb->prefix . 'gmgt_measurment';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_measurment." (
			  `measurment_id` int(11) NOT NULL AUTO_INCREMENT,
			  `result_measurment` varchar(100) NOT NULL,
			  `body_weight` varchar(11) NOT NULL,
			  `result` int(11) NOT NULL,
			  `user_id` int(11) NOT NULL,
			  `result_date` date NOT NULL,
			  `created_by` int(11) NOT NULL,
			  `created_date` date NOT NULL,
			  PRIMARY KEY (`measurment_id`)
			)DEFAULT CHARSET=utf8";
				
		$wpdb->query($sql);
		
		$table_gmgt_user_workouts= $wpdb->prefix . 'gmgt_user_workouts';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_user_workouts." (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `user_workout_id` int(11) NOT NULL,
			  `workout_name` varchar(200) NOT NULL,
			  `sets` int(11) NOT NULL,
			  `reps` int(11) NOT NULL,
			  `kg` float NOT NULL,
			  `rest_time` int(11) NOT NULL,
			  PRIMARY KEY (`id`)
			)DEFAULT CHARSET=utf8";
				
		$wpdb->query($sql);
		
		$table_gmgt_nutrition_data= $wpdb->prefix . 'gmgt_nutrition_data';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_nutrition_data." (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `day_name` varchar(30) NOT NULL,
				  `nutrition_time` varchar(30) NOT NULL,
				  `nutrition_value` text NOT NULL,
				  `nutrition_id` int(11) NOT NULL,
				  `created_date` date NOT NULL,
				  `create_by` int(11) NOT NULL,
				  PRIMARY KEY (`id`)
				)DEFAULT CHARSET=utf8";
				
		$wpdb->query($sql);
		
		$table_gmgt_membership_payment= $wpdb->prefix . 'gmgt_membership_payment';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_membership_payment." (
				  `mp_id` int(11) NOT NULL AUTO_INCREMENT,
				  `member_id` int(11) NOT NULL,
				  `membership_id` int(11) NOT NULL,
				  `membership_amount` double NOT NULL,
				  `paid_amount` double NOT NULL,
				  `start_date` date NOT NULL,
				  `end_date` date NOT NULL,
				  `membership_status` varchar(50) NOT NULL,
				  `payment_status` varchar(20) NOT NULL,
				  `created_date` date NOT NULL,
				  `created_by` int(11) NOT NULL,
				  PRIMARY KEY (`mp_id`)
				)DEFAULT CHARSET=utf8";
				
		$wpdb->query($sql);
		
		$table_gmgt_membership_payment_history = $wpdb->prefix . 'gmgt_membership_payment_history';
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_gmgt_membership_payment_history." (
				  `payment_history_id` bigint(20) NOT NULL AUTO_INCREMENT,
				  `mp_id` int(11) NOT NULL,
				  `amount` int(11) NOT NULL,
				  `payment_method` varchar(50) NOT NULL,
				  `paid_by_date` date NOT NULL,
				  `created_by` int(11) NOT NULL,
				  `trasaction_id` int(11) NOT NULL,
				  PRIMARY KEY (`payment_history_id`)
				)DEFAULT CHARSET=utf8";
				
		$wpdb->query($sql);
		
		$wpdb->query("ALTER TABLE $table_gmgt_measurment  MODIFY COLUMN result FLOAT");

	
	
}
?>