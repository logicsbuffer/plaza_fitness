<?php ?>
<script type="text/javascript">
$(document).ready(function() {
	$(".display-members").select2();
	$('#nutrition_form').validationEngine();
	$('.datepicker').datepicker({
		  changeMonth: true,
	        changeYear: true,
	        dateFormat: 'yy-mm-dd',
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
} );
</script>
     <?php 	
	if($active_tab == 'addnutrition')
	 {
        	
        	$nutrition_id=0;
        	$edit=0;
	 if(isset($_REQUEST['workouttype_id']))
				$workouttype_id=$_REQUEST['workouttype_id'];
			if(isset($_REQUEST['workoutmember_id'])){
				$edit=0;
				$workoutmember_id=$_REQUEST['workoutmember_id'];
				
				$nutrition_logdata=get_user_nutrition($workoutmember_id);
				
			}?>
		
       <div class="panel-body">
        <form name="nutrition_form" action="" method="post" class="form-horizontal" id="nutrition_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="nutrition_id" value="<?php echo $nutrition_id;?>"  />
			<div class="form-group">
			<label class="col-sm-2 control-label" for="day"><?php _e('Member','gym_mgt');?><span class="require-field">*</span></label>	
			<div class="col-sm-8">
				<?php if($edit){ $member_id=$workoutmember_id; }elseif(isset($_POST['member_id'])){$member_id=$_POST['member_id'];}else{$member_id='';}?>
				<select id="member_list" class="display-members" name="member_id" required="true">
				<option value=""><?php _e('Select Member','gym_mgt');?></option>
					<?php $get_members = array('role' => 'member');
					$membersdata=get_users($get_members);
					 if(!empty($membersdata))
					 {
						foreach ($membersdata as $member){?>
							<option value="<?php echo $member->ID;?>" <?php selected($member_id,$member->ID);?>><?php echo $member->display_name." - ".$member->member_id; ?> </option>
						<?php }
					 }?>
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="notice_content"><?php _e('Start Date','gym_mgt');?><span class="require-field">*</span></label>
			
			<div class="col-sm-8">
			<input id="Start_date" class="datepicker form-control validate[required] text-input" type="text" value="<?php if($edit){ echo $result->start_date;}elseif(isset($_POST['start_date'])){echo $_POST['start_date'];}?>" name="start_date">
				
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="notice_content"><?php _e('End Date','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			<input id="end_date" class="datepicker form-control validate[required] text-input" type="text" value="<?php if($edit){ echo $result->expire_date;}elseif(isset($_POST['end_date'])){echo $_POST['end_date'];}?>" name="end_date">
				
			</div>
		</div>
		<div class="form-group">
					<label class="col-sm-1 control-label"></label>
			<div class="col-sm-10">
			<div class="col-md-3">
				<?php foreach (days_array() as $key=>$name){?>
				<div class="checkbox">
				  <label><input type="checkbox" value="" name="day[]" value="<?php echo $key;?>" id="<?php echo $key;?>" data-val="day"><?php echo $name; ?> </label>
				</div>
				<?php }?>
			</div>
			<div class="col-md-8 activity_list">
					<label class="activity_title checkbox">				  		
				  		<strong>
				  			<input type="checkbox" value="" name="avtivity_id[]" value="breakfast" class="nutrition_check" 
				  			id="breakfast"  activity_title = "" data-val="nutrition_time"><?php _e('Break Fast','gym_mgt');?></strong></label>	
				  			<div id="txt_breakfast"></div>
				  			<label class="activity_title checkbox">
				  			<strong>
				  			<input type="checkbox" value="" name="avtivity_id[]" value="lunch" class="nutrition_check" 
				  			id="lunch"  activity_title = "" data-val="nutrition_time"><?php _e('Lunch','gym_mgt');?></strong></label>
				  			<div id="txt_lunch"></div>
				  			<label class="activity_title checkbox"><strong>
				  			<input type="checkbox" value="" name="avtivity_id[]" value="dinner" class="nutrition_check" 
				  			id="dinner"  activity_title = "" data-val="nutrition_time"><?php _e('Dinner','gym_mgt');?></strong></label>	
				  			<div id="txt_dinner"></div>							
						<div class="clear"></div>
					
			</div>
			
			
			
			</div>
		</div>
		
		<div class="col-sm-offset-2 col-sm-8">
			<div class="form-group">
				<div class="col-md-8">
					<input type="button" value="<?php _e('Step-1 Add Nutrition','gym_mgt');?>" name="save_nutrition" id="add_nutrition" class="btn btn-success"/>
				</div>
			</div>
			</div>
		<div id="display_nutrition_list"></div>
		<div class="clear"></div>
		</hr>
		<div class="col-sm-offset-2 col-sm-8 schedule-save-button ">
        	<input type="submit" value="<?php if($edit){ _e('Save Nutrition','gym_mgt'); }else{ _e('Save','gym_mgt');}?>" name="save_nutrition" class="btn btn-success"/>
        </div>
		</form>
        </div>
        
     <?php 
	 }
	 if(isset($nutrition_logdata))
	 	foreach($nutrition_logdata as $row){
	 	$all_logdata=get_nutritiondata($row->id); //var_dump($workout_logdata);
	 		
	 	$arranged_workout=set_nutrition_array($all_logdata);
	 		
	 	?>
	 				<div class="workout_<?php echo $row->id;?> workout-block">
	 				<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-calendar"></i> <?php echo "Start From <span class='work_date'>".$row->start_date."</span> To <span class='work_date'>".$row->expire_date; ?> <span class="removenutrition badge badge-delete pull-right" id="<?php echo $row->id;?>">X</span>	</h3>						
					</div>
	 				<div class="panel panel-white">
	 					<?php
	 					if(!empty($arranged_workout))
	 					{
	 					?>
	 					<div class="work_out_datalist_header">
	 					<div class="col-md-4 col-sm-4 col-xs-4">  
	 					<strong><?php _e('Day Name','gym_mgt');?></strong>
	 					</div>
	 					<div class="col-md-8 col-sm-8 col-xs-8">
	 					<span class="col-md-3 hidden-xs"><?php _e('Time','gym_mgt');?></span>
	 					<span class="col-md-3"><?php _e('Description','gym_mgt');?></span>
	 					
	 					</div>
	 					</div>
	 					<?php 
	 					foreach($arranged_workout as $key=>$rowdata){?>
	 				<div class="work_out_datalist">
	 				<div class="col-md-4 col-sm-4 col-xs-12 day_name">  
	 					<?php echo $key;?>
	 				</div>
	 				<div class="col-md-8 col-sm-8 col-xs-12">
	 						<?php foreach($rowdata as $row){
	 								echo $row."<br>";
	 						} ?>
	 				</div>
	 				</div>
	 				<?php } 
	 					}?>
	 				
	 				</div>
	 			</div>
	 			<?php }	
	 ?>