<?php 
$edit = 0;
if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
		
	$edit=1;
	$result = $obj_workout->get_single_measurement($_REQUEST['measurment_id']);
		
}
?>
<script type="text/javascript">
$(document).ready(function() {
	    
              
	$('#workout_form').validationEngine();
	
	$(".display-members").select2();
	$('#result_date').datepicker({dateFormat : 'yy-mm-dd',
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                
            }); 
} );
</script>
<div class="panel-body">
	<form name="workout_form" action="" method="post" class="form-horizontal" id="workout_form">
	   <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="measurment_id" value="<?php if(isset($_REQUEST['measurment_id']))echo $_REQUEST['measurment_id'];?>">
    	<div class="form-group">
			<label class="col-sm-2 control-label" for="day"><?php _e('Member','gym_mgt');?><span class="require-field">*</span></label>	
			<div class="col-sm-8">
				<?php if($edit){ $member_id=$result->user_id; }elseif(isset($_REQUEST['user_id'])){$member_id=$_REQUEST['user_id'];}else{$member_id='';}?>
				<select id="member_list" class="display-members" name="user_id">
				<option value=""><?php _e('Select Member','gym_mgt');?></option>
					<?php $get_members = array('role' => 'member');
					$membersdata=get_users($get_members);
					 if(!empty($membersdata))
					 {
						foreach ($membersdata as $member){?>
							<option value="<?php echo $member->ID;?>" <?php selected($member_id,$member->ID);?>><?php echo $member->display_name." - ".$member->member_id; ?> </option>
						<?php }
					 }?>
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="result_measurment"><?php _e('Result Measurement','gym_mgt');?> <span class="require-field">*</span></label>
			<div class="col-sm-8">
			<?php if($edit){
					$measument=$result->result_measurment;
				}
				elseif(isset($_REQUEST['result_measurment']))
				{
					$measument = $_REQUEST['result_measurment'];
				}
				else
				{
					$measument="";
				}?>
				<select name="result_measurment" class="form-control validate[required] " id="result_measurment">
				<option value=""><?php  _e('Select Result Measurement ','gym_mgt');?></option>
				<?php 	foreach(measurement_array() as $key=>$element)
						{
							
							echo '<option value='.$key.' '.selected($measument,$key).'>'.$element.'</option>';
						}
					
					?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="result"><?php _e('Result','gym_mgt');?> <span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="result" class="form-control validate[required,custom[number]] text-input" type="text" value="<?php if($edit){ echo $result->result;}elseif(isset($_POST['result'])) echo $_POST['result'];?>" name="result">
			</div>
			<!--<div class="col-sm-1">
				<label id="workout_mesurement" class="control-label"></label>
			</div>-->
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="result_date"><?php _e('Record Date','gym_mgt');?>  <span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="result_date" class="form-control validate[required]" type="text"  name="result_date" 
				value="<?php if($edit){ echo $result->result_date;}elseif(isset($_POST['result_date'])){ echo $_POST['result_date'];} else echo date('Y-m-d');?>">
			</div>
		</div>
		<div class="col-sm-offset-2 col-sm-8">
        	
        	<input type="submit" value="<?php if($edit){ _e('Save Measurement','gym_mgt'); }else{ _e('Save Measurement','gym_mgt');}?>" name="save_measurement" class="btn btn-success"/>
        </div>
    </form>
</div>