<?php 
$month =array('1'=>"January",'2'=>"February",'3'=>"March",'4'=>"April",
		'5'=>"May",'6'=>"June",'7'=>"July",'8'=>"August",
		'9'=>"September",'10'=>"Octomber",'11'=>"November",'12'=>"December",);
$year =isset($_POST['year'])?$_POST['year']:date('Y');
//$year =2015;

global $wpdb;
$table_name = $wpdb->prefix."gmgt_payment";
$q="SELECT EXTRACT(MONTH FROM payment_date) as date,sum(total_amount) as count FROM ".$table_name." WHERE YEAR(payment_date) =".$year." group by month(payment_date) ORDER BY payment_date ASC";
//$q="SELECT EXTRACT(DAY FROM date) as date,count(*) as count FROM ".$table_name." group by date(date)";
//echo $q;
$result=$wpdb->get_results($q);
$chart_array = array();
$chart_array[] = array('Month','Payment ');
foreach($result as $r)
{

	$chart_array[]=array( $month[$r->date],(int)$r->count);
}

 $options = Array(
			'title' => __('Payment Report By Month','gym_mgt'),
			'titleTextStyle' => Array('color' => '#66707e','fontSize' => 14,'bold'=>true,'italic'=>false,'fontName' =>'open sans'),
			'legend' =>Array('position' => 'right',
						'textStyle'=> Array('color' => '#66707e','fontSize' => 14,'bold'=>true,'italic'=>false,'fontName' =>'open sans')),
			
			
			//'bar'  => Array('groupWidth' => '70%'),
			//'lagend' => Array('position' => 'none'),
			'hAxis' => Array(
				'title' => __('Month','gym_mgt'),
				'titleTextStyle' => Array('color' => '#66707e','fontSize' => 14,'bold'=>true,'italic'=>false,'fontName' =>'open sans'),
				'textStyle' => Array('color' => '#66707e','fontSize' => 11),
				'maxAlternation' => 2
				
				//'annotations' =>Array('textStyle'=>Array('fontSize'=>5))
				),
			'vAxis' => Array(
				'title' => __('Payment','gym_mgt'),
				 'minValue' => 0,
				'maxValue' => 5,
				 'format' => '#',
				'titleTextStyle' => Array('color' => '#66707e','fontSize' => 14,'bold'=>true,'italic'=>false,'fontName' =>'open sans'),
				'textStyle' => Array('color' => '#66707e','fontSize' => 12)
				),
 		'colors' => array('#22BAA0')
 		
			);
require_once GMS_PLUGIN_DIR. '/lib/chart/GoogleCharts.class.php';

$GoogleCharts = new GoogleCharts;

$chart = $GoogleCharts->load( 'column' , 'chart_div' )->get( $chart_array , $options );
?>
<script type="text/javascript">
$(document).ready(function() {
	
	$('.sdate').datepicker({dateFormat: "yy-mm-dd"}); 
	$('.edate').datepicker({dateFormat: "yy-mm-dd"}); 

 
} );
</script>

    	
    
  <div id="chart_div" style="width: 100%; height: 500px;"></div>
  
  <!-- Javascript --> 
  <script type="text/javascript" src="https://www.google.com/jsapi"></script> 
  <script type="text/javascript">
			<?php if(!empty($result))
						echo $chart;?>
		</script>
 

