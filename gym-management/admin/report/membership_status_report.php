<?php 
 $mebmer = get_users(array('role'=>'member'));
 global $wpdb;
 $table_name = $wpdb->prefix."gmgt_membershiptype";
 $q="SELECT * From $table_name";
 $member_ship_array = array();
 $result=$wpdb->get_results($q);
 $membership_status = array('Continue','Active','Dropped');
foreach($membership_status as $retrive)
{
	// $membership_id = $retrive->ID;
	//echo "<BR>".get_membership_name($membership_id);
	//echo get_user_meta($membership_id, 'membership_status',true)."<BR>";
	$member_ship_count =  count(get_users(array('role'=>'member','meta_key' => 'membership_status', 'meta_value' => $retrive)));
	$member_ship_array[] = array('member_ship_id'=> $retrive,
								 'member_ship_count'=>	$member_ship_count							
								);
	//echo $member_ship.",";
	
}

//var_dump($member_ship_array);
$chart_array = array();
$chart_array[] = array('Membership','Number Of Member');
foreach($member_ship_array as $r)
{
	$chart_array[]=array( $r['member_ship_id'],$r['member_ship_count']);
}
//var_dump($chart_array);
//exit;
 $options = Array(
					 		'title' => __('Membership by status'),
 							'colors' => array('#22BAA0','#F25656','#12AFCB')
					 		);
require_once GMS_PLUGIN_DIR. '/lib/chart/GoogleCharts.class.php';

$GoogleCharts = new GoogleCharts;

$chart = $GoogleCharts->load( 'PieChart' , 'chart_div' )->get( $chart_array , $options );
?>
<script type="text/javascript">
$(document).ready(function() {
	
	$('.sdate').datepicker({dateFormat: "yy-mm-dd"}); 
	$('.edate').datepicker({dateFormat: "yy-mm-dd"}); 

 
} );
</script>

    	
    
  <div id="chart_div" style="width: 100%; height: 500px;"></div>
  
  <!-- Javascript --> 
  <script type="text/javascript" src="https://www.google.com/jsapi"></script> 
  <script type="text/javascript">
			<?php echo $chart;?>
		</script>
 

