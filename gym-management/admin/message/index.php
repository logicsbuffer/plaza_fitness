<?php 
if(isset($_POST['save_message']))
{

	$created_date = date("Y-m-d H:i:s");
	$subject = $_POST['subject'];
	$message_body = $_POST['message_body'];
	$created_date = date("Y-m-d H:i:s");
	$tablename="gmgt_message";

	$role=$_POST['receiver'];
	if(isset($_REQUEST['class_id']))
	$class_id = $_REQUEST['class_id'];
	if($role == 'member' || $role == 'staff_member' || $role == 'accountant')
	{
		
		$userdata=gmgt_get_user_notice($role,$_REQUEST['class_id']);
		
		if(!empty($userdata))
		{
			
				$mail_id = array();
				$i = 0;
					foreach($userdata as $user)
					{
						
						if($role == 'parent' && $class_id != 'all')
						$mail_id[]=$user['ID'];
						else 
							$mail_id[]=$user->ID;
						
						$i++;
					}


				foreach($mail_id as $user_id)
				{

					$reciever_id = $user_id;
					$message_data=array('sender'=>get_current_user_id(),
							'receiver'=>$user_id,
							'subject'=>$subject,
							'message_body'=>$message_body,
							'date'=>$created_date,
							'status' =>0
					);
					gmgt_insert_record($tablename,$message_data);


						
						
						
				}
				$post_id = wp_insert_post( array(
						'post_status' => 'publish',
						'post_type' => 'message',
						'post_title' => $subject,
						'post_content' =>$message_body

				) );


				$result=add_post_meta($post_id, 'message_for',$role);
				$result=add_post_meta($post_id, 'gmgt_class_id',$_REQUEST['class_id']);

		}
		else
		{
			
				$user_id =$_POST['receiver'];
				$message_data=array('sender'=>get_current_user_id(),
						'receiver'=>$user_id,
						'subject'=>$subject,
						'message_body'=>$message_body,
						'date'=>$created_date,
						'status' =>0
				);
				gmgt_insert_record($tablename,$message_data);
			
			$post_id = wp_insert_post( array(
					'post_status' => 'publish',
					'post_type' => 'message',
					'post_title' => $subject,
					'post_content' =>$message_body
			
			) );
			
			
			$result=add_post_meta($post_id, 'message_for','user');
			$result=add_post_meta($post_id, 'message_gmgt_user_id',$user_id);

		}
	}
	else
	{
				$user_id =$_POST['receiver'];
				$message_data=array('sender'=>get_current_user_id(),
						'receiver'=>$user_id,
						'subject'=>$subject,
						'message_body'=>$message_body,
						'date'=>$created_date,
						'status' =>0
				);
				gmgt_insert_record($tablename,$message_data);
			
				$post_id = wp_insert_post( array(
						'post_status' => 'publish',
						'post_type' => 'message',
						'post_title' => $subject,
						'post_content' =>$message_body
				
				) );
			
			
			$result=add_post_meta($post_id, 'message_for','user');
			$result=add_post_meta($post_id, 'message_gmgt_user_id',$user_id);
	}
	
}
if(isset($result))
{?>
	<div id="message" class="updated below-h2">
		<p><?php _e('Message Sent Successfully!','gym_mgt');?></p>
	</div>
<?php }		
	
$active_tab=isset($_REQUEST['tab'])?$_REQUEST['tab']:'inbox';?>
<script type="text/javascript">
$(document).ready(function() {
	
	$('.sdate').datepicker({dateFormat: "yy-mm-dd"}); 
	$('.edate').datepicker({dateFormat: "yy-mm-dd"}); 

 
} );
</script>
<div class="page-inner" style="min-height:1631px !important">
	<div class="page-title">
		<h3><img src="<?php echo get_option( 'gmgt_system_logo' ) ?>" class="img-circle head_logo" width="40" height="40" /><?php echo get_option( 'gmgt_system_name' );?></h3>
	</div>
	<div id="main-wrapper">
		<div class="row mailbox-header">
                                <div class="col-md-2">
                                    <a class="btn btn-success btn-block" href="?page=gmgt_message&tab=compose"><?php _e('Compose','gym_mgt');?></a>
                                </div>
                                <div class="col-md-6">
                                    <h2>
                                    <?php
									if(!isset($_REQUEST['tab']) || ($_REQUEST['tab'] == 'inbox'))
                                    echo esc_html( __( 'Inbox', 'gym_mgt' ) );
									else if(isset($_REQUEST['page']) && $_REQUEST['tab'] == 'sentbox')
									echo esc_html( __( 'Sent Item', 'gym_mgt' ) );
									else if(isset($_REQUEST['page']) && $_REQUEST['tab'] == 'compose')
										echo esc_html( __( 'Compose', 'gym_mgt' ) );
									?>
								
                                    
                                    </h2>
                                </div>
                               
                            </div>
 <div class="col-md-2">
                            <ul class="list-unstyled mailbox-nav">
                                <li <?php if(!isset($_REQUEST['tab']) || ($_REQUEST['tab'] == 'inbox')){?>class="active"<?php }?>>
                                <a href="?page=gmgt_message&tab=inbox"><i class="fa fa-inbox"></i> <?php _e('Inbox','gym_mgt');?><span class="badge badge-success pull-right"><?php echo count(gmgt_get_inbox_message(get_current_user_id()));?></span></a></li>
                                <li <?php if(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'sentbox'){?>class="active"<?php }?>><a href="?page=gmgt_message&tab=sentbox"><i class="fa fa-sign-out"></i><?php _e('Sent','gym_mgt');?></a></li>                                
                            </ul>
                        </div>
 <div class="col-md-10">
 <?php  
 	if(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'sentbox')
 		require_once GMS_PLUGIN_DIR. '/admin/message/sendbox.php';
 	if(!isset($_REQUEST['tab']) || ($_REQUEST['tab'] == 'inbox'))
 		require_once GMS_PLUGIN_DIR. '/admin/message/inbox.php';
 	if(isset($_REQUEST['tab']) && ($_REQUEST['tab'] == 'compose'))
 		require_once GMS_PLUGIN_DIR. '/admin/message/composemail.php';
 	if(isset($_REQUEST['tab']) && ($_REQUEST['tab'] == 'view_message'))
 		require_once GMS_PLUGIN_DIR. '/admin/message/view_message.php';
 	
 	?>
</div>
</div><!-- Main-wrapper -->
</div><!-- Page-inner -->
