<?php ?>
<script type="text/javascript">
$(document).ready(function() {
	$('#store_form').validationEngine();
	$('#sell_date').datepicker({dateFormat : 'yy-mm-dd',
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
		$(".display-members").select2();
} );
</script>
     <?php 	
	if($active_tab == 'sellproduct')
	 {
        	
        	$sell_id=0;
			if(isset($_REQUEST['sell_id']))
				$sell_id=$_REQUEST['sell_id'];
			$edit=0;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
					
					$edit=1;
					$result = $obj_store->get_single_selling($sell_id);
					
				}?>
		
       <div class="panel-body">
        <form name="store_form" action="" method="post" class="form-horizontal" id="store_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="sell_id" value="<?php echo $sell_id;?>"  />
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="day"><?php _e('Member','gym_mgt');?><span class="require-field">*</span></label>	
			<div class="col-sm-8">
				<?php if($edit){ $member_id=$result->member_id; }elseif(isset($_POST['member_id'])){$member_id=$_POST['member_id'];}else{$member_id='';}?>
				<select id="member_list" class="display-members" name="member_id">
				<option value=""><?php _e('Select Member','gym_mgt');?></option>
					<?php $get_members = array('role' => 'member');
					$membersdata=get_users($get_members);
					 if(!empty($membersdata))
					 {
						foreach ($membersdata as $member){?>
							<option value="<?php echo $member->ID;?>" <?php selected($member_id,$member->ID);?>><?php echo $member->display_name." - ".$member->member_id; ?> </option>
						<?php }
					 }?>
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="sell_date"><?php _e('Date','gym_mgt');?></label>
			<div class="col-sm-8">
				<input id="sell_date" class="form-control" type="text"  name="sell_date" 
				value="<?php if($edit){ echo $result->sell_date;}elseif(isset($_POST['sell_date'])){ echo $_POST['sell_date'];}else{ echo date("Y-m-d"); }?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="product_id"><?php _e('Product','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<?php if($edit){ $product_id=$result->product_id; }elseif(isset($_POST['product_id'])){$product_id=$_POST['product_id'];}else{$product_id='';}?>
				<select id="product_id" class="form-control validate[required]" name="product_id">
				<option value=""><?php _e('Select Product','gym_mgt');?></option>
			<?php $productdata=$obj_product->get_all_product();
					 if(!empty($productdata))
					 {
						foreach ($productdata as $product){?>
						<option value="<?php echo $product->id;?>" <?php selected($product_id,$product->id);  ?>><?php echo $product->product_name; ?> </option>
			<?php } } ?>
			</select>
			</div>
			<div class="col-sm-2">
			<a href="?page=gmgt_product&tab=addproduct" class="btn btn-default"> <?php _e('Add Product','gym_mgt');?></a>
			</div>
		</div>
		
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="quentity"><?php _e('Quentity','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="group_name" class="form-control validate[required,custom[number]] text-input" type="text" value="<?php if($edit){ echo $result->quentity;}elseif(isset($_POST['quentity'])) echo $_POST['quentity'];?>" name="quentity" <?php if($edit){?> readonly <?php } ?>>
			</div>
		</div>
		
		
		
		
		<div class="col-sm-offset-2 col-sm-8">
        	
        	<input type="submit" value="<?php if($edit){ _e('Save','gym_mgt'); }else{ _e('Sell Product','gym_mgt');}?>" name="save_selling" class="btn btn-success"/>
        </div>
		
		
		
        </form>
        </div>
        
     <?php 
	 }
	 ?>