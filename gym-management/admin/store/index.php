<?php $obj_class=new Gmgtclassschedule;
$obj_product=new Gmgtproduct;
$obj_store=new Gmgtstore;
$active_tab = isset($_GET['tab'])?$_GET['tab']:'store';
?>


<div class="page-inner" style="min-height:1631px !important">
<div class="page-title">
		<h3><img src="<?php echo get_option( 'gmgt_system_logo' ) ?>" class="img-circle head_logo" width="40" height="40" /><?php echo get_option( 'gmgt_system_name' );?></h3>
	</div>
	<?php 
	
	if(isset($_POST['save_selling']))
	{
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='edit')
		{
				
			$result=$obj_store->gmgt_sell_product($_POST);
			if($result)
			{
				wp_redirect ( admin_url().'admin.php?page=gmgt_store&tab=store&message=2');
			}
			
				
				
		}
		else
		{
			$result=$obj_store->gmgt_sell_product($_POST);
				
				if($result!='no_stock')
				{
					wp_redirect ( admin_url().'admin.php?page=gmgt_store&tab=store&message=1');
				}
				else
				{ 
					wp_redirect ( admin_url().'admin.php?page=gmgt_store&tab=sellproduct&message=4');
				}
			
			}
			
			
		
	}
	
		
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='delete')
			{
				
				$result=$obj_store->delete_selling($_REQUEST['sell_id']);
				if($result)
				{
					wp_redirect ( admin_url().'admin.php?page=gmgt_store&tab=store&message=3');
				}
			}
		if(isset($_REQUEST['message']))
	{
		$message =$_REQUEST['message'];
		if($message == 1)
		{?>
				<div id="message" class="updated below-h2 ">
				<p>
				<?php 
					_e('Record inserted successfully','gym_mgt');
				?></p></div>
				<?php 
			
		}
		elseif($message == 2)
		{?><div id="message" class="updated below-h2 "><p><?php
					_e("Record updated successfully.",'gym_mgt');
					?></p>
					</div>
				<?php 
			
		}
		elseif($message == 3) 
		{?>
		<div id="message" class="updated below-h2"><p>
		<?php 
			_e('Record deleted successfully','gym_mgt');
		?></div></p><?php
				
		}
		elseif($message == 4) 
		{?>
		<div id="message" class="updated below-h2"><p>
		<?php 
			_e('Not Enough Stock of This Product','gym_mgt');
		?></div></p><?php
				
		}
	}
	?>
	<div id="main-wrapper">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
	<h2 class="nav-tab-wrapper">
    	<a href="?page=gmgt_store&tab=store" class="nav-tab <?php echo $active_tab == 'store' ? 'nav-tab-active' : ''; ?>">
		<?php echo '<span class="dashicons dashicons-menu"></span> '.__('Sells Record', 'gym_mgt'); ?></a>
    	
        <?php  if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
		{?>
        <a href="?page=gmgt_store&tab=sellproduct&action=edit&sell_id=<?php echo $_REQUEST['sell_id'];?>" class="nav-tab <?php echo $active_tab == 'sellproduct' ? 'nav-tab-active' : ''; ?>">
		<?php _e('Edit Sold Product', 'gym_mgt'); ?></a>  
		<?php 
		}
		else
		{?>
			<a href="?page=gmgt_store&tab=sellproduct" class="nav-tab <?php echo $active_tab == 'sellproduct' ? 'nav-tab-active' : ''; ?>">
		<?php echo '<span class="dashicons dashicons-plus-alt"></span> '.__('Sell Product', 'gym_mgt'); ?></a>
			
		<?php  }?>
       
    </h2>
     <?php 
	//Report 1 
	
	if($active_tab == 'store')
	{ 
	
	?>	
    <script type="text/javascript">
$(document).ready(function() {
	jQuery('#selling_list').DataTable({
		"responsive": true,
		"order": [[ 0, "asc" ]],
		"aoColumns":[
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": false}]
		});
} );
</script>
    <form name="wcwm_report" action="" method="post">
    
        <div class="panel-body">
        	<div class="table-responsive">
        <table id="selling_list" class="display" cellspacing="0" width="100%">
        	 <thead>
            <tr>
			<th><?php  _e( 'Product Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Member Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Product Quentity', 'gym_mgt' ) ;?></th>
               <th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
			<th><?php  _e( 'Product Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Member Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Product Quentity', 'gym_mgt' ) ;?></th>
               <th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
            </tr>
        </tfoot>
 
        <tbody>
         <?php 
		
		
			$storedata=$obj_store->get_all_selling();
		 if(!empty($storedata))
		 {
		 	foreach ($storedata as $retrieved_data){

		 ?>
            <tr><td class="productname"><a href="?page=gmgt_store&tab=sellproduct&action=edit&sell_id=<?php echo $retrieved_data->id;?>"><?php $product = $obj_product->get_single_product($retrieved_data->product_id); 
				echo $product->product_name;?></a></td>
			<td class="membername"><?php $userdata=get_userdata($retrieved_data->member_id);
			echo $userdata->display_name;?></td>
				
				<td class="productquentity"><?php echo $retrieved_data->quentity;?></td>
                <td class="action"> <a href="?page=gmgt_store&tab=sellproduct&action=edit&sell_id=<?php echo $retrieved_data->id?>" class="btn btn-info"> <?php _e('Edit', 'gym_mgt' ) ;?></a>
                <a href="?page=gmgt_store&tab=store&action=delete&sell_id=<?php echo $retrieved_data->id;?>" class="btn btn-danger" 
                onclick="return confirm('<?php _e('Are you sure you want to delete this record?','gym_mgt');?>');">
                <?php _e( 'Delete', 'gym_mgt' ) ;?> </a>
                
                </td>
               
            </tr>
            <?php } 
			
		}?>
     
        </tbody>
        
        </table>
        </div>
        </div>
       
</form>
     <?php 
	 }
	
	if($active_tab == 'sellproduct')
	 {
	require_once GMS_PLUGIN_DIR. '/admin/store/sell_product.php';
	 }
	
	 ?>
</div>
			
	</div>
	</div>
</div>


<?php //} ?>