<?php ?>
<script type="text/javascript">
$(document).ready(function() {
	$('#group_form').validationEngine();
	$('#day').multiselect();
} );
</script>
     <?php 	
	if($active_tab == 'addclass')
	 {
        	
        	$class_id=0;
			if(isset($_REQUEST['class_id']))
				$class_id=$_REQUEST['class_id'];
			$edit=0;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
					
					$edit=1;
					$result = $obj_class->get_single_class($class_id);
					
				}?>
		
       <div class="panel-body">
        <form name="group_form" action="" method="post" class="form-horizontal" id="group_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="class_id" value="<?php echo $class_id;?>"  />
		<div class="form-group">
			<label class="col-sm-2 control-label" for="class_name"><?php _e('Class Name','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="group_name" class="form-control validate[required] text-input" type="text" value="<?php if($edit){ echo $result->class_name;}elseif(isset($_POST['class_name'])) echo $_POST['class_name'];?>" name="class_name">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="staff_name"><?php _e('Select Staff Member','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<?php $get_staff = array('role' => 'Staff_member');
					$staffdata=get_users($get_staff);?>
				<select name="staff_id" class="form-control validate[required] " id="staff_id">
				<option value=""><?php  _e('Select Staff Member ','gym_mgt');?></option>
				<?php 
					if($edit)
						$staff_data=$result->staff_id;
					elseif(isset($_POST['staff_id']))
						$staff_data=$_POST['staff_id'];
					else
						$staff_data="";
					if(!empty($staffdata))
					{
					foreach($staffdata as $staff)
					{
						
						echo '<option value='.$staff->ID.' '.selected($staff_data,$staff->ID).'>'.$staff->display_name.'</option>';
					}
					}
					?>
				</select>
				
			</div>
			<div class="col-sm-2">
			<a href="?page=gmgt_staff&tab=add_staffmember" class="btn btn-default"> <?php _e('Add Staff Member','gym_mgt');?></a>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="middle_name"><?php _e('Select Assistant Staff Member','gym_mgt');?></label>
			<div class="col-sm-8">
				<?php $get_staff = array('role' => 'Staff_member');
					$staffdata=get_users($get_staff);?>
				<select name="asst_staff_id" class="form-control" id="asst_staff_id">
				<option value=""><?php  _e('Select Assistant Staff Member ','gym_mgt');?></option>
				<?php if($edit)
						$assi_staff_data=$result->asst_staff_id;
					elseif(isset($_POST['asst_staff_id']))
						$assi_staff_data=$_POST['asst_staff_id'];
					else
						$assi_staff_data="";
					
					if(!empty($staffdata))
					{
						foreach($staffdata as $staff)
						{
							
							echo '<option value='.$staff->ID.' '.selected($assi_staff_data,$staff->ID).'>'.$staff->display_name.'</option>';
						}
					}
					?>
				</select>
			</div>
			<div class="col-sm-2">
			<a href="?page=gmgt_staff&tab=add_staffmember" class="btn btn-default"> <?php _e('Add Staff Member','gym_mgt');?></a>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="day"><?php _e('Select Day','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<select name="day[]" class="form-control validate[required] " id="day" multiple="multiple">
				<!--<option value=""><?php  _e('Select Day ','gym_mgt');?></option>-->
				<?php $class_days=array();
				if($edit){$class_days=json_decode($result->day);}
					foreach(days_array() as $key=>$day)
					{
						$selected = "";
						if(in_array($key,$class_days))
							$selected = "selected";
						echo '<option value='.$key.' '.$selected.'>'.$day.'</option>';
					}?>
					
					
					
				</select>
			</div>
			
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="starttime"><?php _e('Start Time','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-2">
			<?php 
			if($edit)
			{
				$start_time_data = explode(":", $result->start_time);
				
			}
			?>
				 <select name="start_time" class="form-control validate[required]">
				 <option value=""><?php _e('Select Time','gym_mgt');?></option>
                         <?php 
						 	for($i =0 ; $i <= 12 ; $i++)
							{
							?>
							<option value="<?php echo $i;?>" <?php  if($edit) selected($start_time_data[0],$i);  ?>><?php echo $i;?></option>
							<?php
							}
						 ?>
                         </select>
			</div>
			<div class="col-sm-2">
				 <select name="start_min" class="form-control validate[required]">
                         <?php 
						 	foreach(minute_array() as $key=>$value)
							{?>
							<option value="<?php echo $key;?>" <?php  if($edit) selected($start_time_data[1],$key);  ?>><?php echo $value;?></option>
							<?php
							}
						 ?>
                         </select>
			</div>
			<div class="col-sm-2">
				 <select name="start_ampm" class="form-control validate[required]">
                         	<option value="am" <?php  if($edit) if(isset($start_time_data[2])) selected($start_time_data[2],'am');  ?>><?php _e('am','gym_mgt');?></option>
                            <option value="pm" <?php  if($edit) if(isset($start_time_data[2])) selected($start_time_data[2],'pm');  ?>><?php _e('pm','gym_mgt');?></option>
                         </select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="weekday"><?php _e('End Time','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-2">
			<?php 
			if($edit)
			{
				$end_time_data = explode(":", $result->end_time);
			}
			?>
				 <select name="end_time" class="form-control validate[required]">
				 <option value=""><?php _e('Select Time','gym_mgt');?></option>
                         <?php 
						 	for($i =0 ; $i <= 12 ; $i++)
							{
							?>
							<option value="<?php echo $i;?>" <?php  if($edit) selected($end_time_data[0],$i);  ?>><?php echo $i;?></option>
							<?php
							}
						 ?>
                         </select>
			</div>
			<div class="col-sm-2">
				 <select name="end_min" class="form-control validate[required]">
                         <?php 
						 	foreach(minute_array() as $key=>$value)
							{
							?>
							<option value="<?php echo $key;?>" <?php  if($edit) selected($end_time_data[1],$key);  ?>><?php echo $value;?></option>
							<?php
							}
						 ?>
                         </select>
			</div>
			<div class="col-sm-2">
				  <select name="end_ampm" class="form-control validate[required]">
                         	<option value="am" <?php  if($edit) if(isset($end_time_data[2])) selected($end_time_data[2],'am');  ?> ><?php _e('am','gym_mgt');?></option>
                            <option value="pm" <?php  if($edit) if(isset($end_time_data[2]))selected($end_time_data[2],'pm');  ?>><?php _e('pm','gym_mgt');?></option>
                         </select>
			</div>	
		</div>
		
		<div class="col-sm-offset-2 col-sm-8">
        	
        	<input type="submit" value="<?php if($edit){ _e('Save','gym_mgt'); }else{ _e('Save','gym_mgt');}?>" name="save_class" class="btn btn-success"/>
        </div>
		
		
		
        </form>
        </div>
        
     <?php 
	 }
	 ?>