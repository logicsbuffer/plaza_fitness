<?php 
 // This is adminside main First page of school management plugin 
 add_action( 'admin_head', 'gym_admin_menu_icon' );
function gym_admin_menu_icon() {
?>
<style type="text/css">
#adminmenu #toplevel_page_hospital div.wp-menu-image:before {
  content: "\f512";
}
</style>
 <?php 
}
 add_action( 'admin_menu', 'gym_system_menu' );
function gym_system_menu() {
	add_menu_page('PFP Settings', __('PFP Settings','gym_mgt'),'manage_options','gym_system','gym_system_dashboard',plugins_url('gym-management/assets/images/gym-1.png' )); 

	add_submenu_page('gym_system', 'Dashboard', __( 'Dashboard', 'gym_mgt' ), 'administrator', 'gym_system', 'gym_system_dashboard');
	
	//add_submenu_page('gym_system', 'Membership Type', __( 'Membership Type', 'gym_mgt' ), 'administrator', 'gmgt_membership_type', 'membership_manage');
	
	//add_submenu_page('gym_system', 'Group', __( 'Group', 'gym_mgt' ), 'administrator', 'gmgt_group', 'group_manage');
	
	add_submenu_page('gym_system', 'Staff Members', __( 'Trainer', 'gym_mgt' ), 'administrator', 'gmgt_staff', 'staff_manage');
	
	//add_submenu_page('gym_system', 'Class Schedule', __( 'Class Schedule', 'gym_mgt' ), 'administrator', 'gmgt_class', 'class_manage');
	
	add_submenu_page('gym_system', 'Member', __( 'Member', 'gym_mgt' ), 'administrator', 'gmgt_member', 'member_manage');
	
	
	//add_submenu_page('gym_system', 'Activity', __( 'Activity', 'gym_mgt' ), 'administrator', 'gmgt_activity', 'activity_manage');
	
	//add_submenu_page('gym_system', 'Assign Workout', __( 'Assign Workout', 'gym_mgt' ), 'administrator', 'gmgt_workouttype', 'workouttype_manage');
	
	//add_submenu_page('gym_system', 'Daily Workout', __( 'Daily Workout', 'gym_mgt' ), 'administrator', 'gmgt_workout', 'workout_manage');
	
	//add_submenu_page('gym_system', 'Product', __( 'Product', 'gym_mgt' ), 'administrator', 'gmgt_product', 'product_manage');
	
	//add_submenu_page('gym_system', 'Store', __( 'Store', 'gym_mgt' ), 'administrator', 'gmgt_store', 'store_manage');
	
	//add_submenu_page('gym_system', 'Nutrition Schedule', __( 'Nutrition Schedule', 'gym_mgt' ), 'administrator', 'gmgt_nutrition', 'nutrition_manage');
	
	//add_submenu_page('gym_system', 'Reservation', __( 'Reservation', 'gym_mgt' ), 'administrator', 'gmgt_reservation', 'reservation_manage');
	
	//add_submenu_page('gym_system', 'Attendance', __( 'Attendance', 'gym_mgt' ), 'administrator', 'gmgt_attendence', 'attendence_manage');
	
		
	//add_submenu_page('gym_system', 'Accountant', __( 'Accountant', 'gym_mgt' ), 'administrator', 'gmgt_accountant', 'accountant_manage');
	//add_submenu_page('gym_system', 'Fees Payment', __( 'Fees Payment', 'gym_mgt' ), 'administrator', 'gmgt_fees_payment', 'gmgt_fees_payment');
	//add_submenu_page('gym_system', 'Payment', __( 'Payment', 'gym_mgt' ), 'administrator', 'gmgt_payment', 'payment_manage');
	
	add_submenu_page('gym_system', 'Message', __( 'Message', 'gym_mgt' ), 'administrator', 'gmgt_message', 'message_manage');
	
	//add_submenu_page('gym_system', 'News Letter', __( 'News Letter', 'gym_mgt' ), 'administrator', 'gmgt_newsletter', 'newsletter_manage');
	
	add_submenu_page('gym_system', 'Notice', __( 'Notice', 'gym_mgt' ), 'administrator', 'gmgt_notice', 'notice_manage');
	
	//add_submenu_page('gym_system', 'Report', __( 'Report', 'gym_mgt' ), 'administrator', 'gmgt_report', 'report_manage');
	
	add_submenu_page('gym_system', 'Gnrl_setting', __( 'General Settings', 'gym_mgt' ), 'administrator', 'gmgt_gnrl_settings', 'gym_gnrl_settings');
}

function gym_system_dashboard()
{
	require_once GMS_PLUGIN_DIR. '/admin/dasboard.php';
	
}	
function membership_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/membership/index.php';
}
function group_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/group/index.php';
}
function staff_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/staff-members/index.php';
}
function accountant_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/accountant/index.php';
}
function class_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/class-schedule/index.php';
}
function member_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/member/index.php';
}
function product_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/product/index.php';
}
function store_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/store/index.php';
}
function nutrition_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/nutrition/index.php';
}
function reservation_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/reservation/index.php';
}
function attendence_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/attendence/index.php';
}
function gmgt_fees_payment()
{
	require_once GMS_PLUGIN_DIR. '/admin/membership_payment/index.php';
}
function payment_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/payment/index.php';
}
function message_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/message/index.php';
}
function newsletter_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/news-letter/index.php';
}
function activity_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/activity/index.php';
}
function workouttype_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/workout-type/index.php';
}
function workout_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/workout/index.php';
}
function notice_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/notice/index.php';
}
function report_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/report/index.php';
}
function gym_gnrl_settings()
{
	require_once GMS_PLUGIN_DIR. '/admin/general-settings.php';
}
?>