<?php 
$role="member";
?>
<script type="text/javascript">
$(document).ready(function() {
	$('#member_form').validationEngine();
	 
	$('#group_id').multiselect();
	$('#birth_date').datepicker({
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
	$('#inqiury_date').datepicker({dateFormat : 'yy-mm-dd',
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
	$('#triel_date').datepicker({dateFormat : 'yy-mm-dd',
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
	$('#begin_date').datepicker({dateFormat : 'yy-mm-dd',
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
	$('#first_payment_date').datepicker({dateFormat : 'yy-mm-dd',
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
} );
</script>
     <?php 	
	if($active_tab == 'addmember')
	 {
        	
        	$member_id=0;
			if(isset($_REQUEST['member_id']))
				$member_id=$_REQUEST['member_id'];
			$edit=0;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
					
					$edit=1;
					$user_info = get_userdata($member_id);
					
				}
				else
				{
				 $lastmember_id=get_lastmember_id($role);
				$nodate=substr($lastmember_id,0,-4);
				$memberno=substr($nodate,1);
				$memberno+=1;
				$newmember='M'.$memberno.date("my");
				}?>
<?php
if (isset($_REQUEST['save_member'])){
	
	
}

?>

       <div class="panel-body">
        <form name="member_form" action="" method="post" class="form-horizontal" id="member_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="role" value="<?php echo $role;?>"  />
		<input type="hidden" name="user_id" value="<?php echo $member_id;?>"  />
		<div class="header">	
			<h3><?php _e('Personal Information','gym_mgt');?></h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="member_id"><?php _e('Member Id','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="member_id" class="form-control validate[required]" type="text" 
				value="<?php if($edit){ echo $user_info->member_id;}else echo $newmember;?>"  readonly name="member_id">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="first_name"><?php _e('First Name','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="first_name" class="form-control validate[required,custom[onlyLetterSp]] text-input" type="text" value="<?php if($edit){ echo $user_info->first_name;}elseif(isset($_POST['first_name'])) echo $_POST['first_name'];?>" name="first_name">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="middle_name"><?php _e('Middle Name','gym_mgt');?></label>
			<div class="col-sm-8">
				<input id="middle_name" class="form-control " type="text"  value="<?php if($edit){ echo $user_info->middle_name;}elseif(isset($_POST['middle_name'])) echo $_POST['middle_name'];?>" name="middle_name">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="last_name"><?php _e('Last Name','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="last_name" class="form-control validate[required,custom[onlyLetterSp]] text-input" type="text"  value="<?php if($edit){ echo $user_info->last_name;}elseif(isset($_POST['last_name'])) echo $_POST['last_name'];?>" name="last_name">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gender"><?php _e('Gender','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			<?php $genderval = "male"; if($edit){ $genderval=$user_info->gender; }elseif(isset($_POST['gender'])) {$genderval=$_POST['gender'];}?>
				<label class="radio-inline">
			     <input type="radio" value="male" class="tog validate[required]" name="gender"  <?php  checked( 'male', $genderval);  ?>/><?php _e('Male','gym_mgt');?>
			    </label>
			    <label class="radio-inline">
			      <input type="radio" value="female" class="tog validate[required]" name="gender"  <?php  checked( 'female', $genderval);  ?>/><?php _e('Female','gym_mgt');?> 
			    </label>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="birth_date"><?php _e('Date of birth','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="birth_date" class="form-control validate[required]" type="text"  name="birth_date" 
				value="<?php if($edit){ echo $user_info->birth_date;}elseif(isset($_POST['birth_date'])) echo $_POST['birth_date'];?>">
			</div>
		</div>
		<?php
		$member_trainer_id = get_user_meta($member_id, 'member_trainer',true);
		$member_trainer = get_userdata($member_trainer_id);
			
				?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="birth_date"><?php _e('Select Trainer','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			<select id="member_trainer" class="form-control display-members" name="member_trainer" required>
				<?php if($member_trainer){
					?>
					<option value="<?php echo $member_trainer_id ?>" selected><?php echo $member_trainer->display_name." - ".$member_trainer->ID; ?> </option>
								

					<?php
				}else{
?>
				<option value=""><?php _e('Select Trainer','gym_mgt');?></option>
<?php
				}
					//$get_members = array('role' => 'member');
					$get_members = array(
						 'role' => 'staff_member',
						 'orderby' => 'user_nicename',
						 'order' => 'ASC'
						);
					
					$membersdata = get_users($get_members);
					 if(!empty($membersdata))
					 {
						foreach ($membersdata as $member){?>
							<?php $member_id = $member->ID ?> 
							<option value="<?php echo $member->ID;?>" <?php selected($member_id,$user_id);?>><?php echo $member->display_name." - ".$member->ID; ?> </option>
						<?php }
					 }?>
			</select>
			</div>
		</div>
		<div class="header">	<hr>
			<h3><?php _e('Contact Information','gym_mgt');?></h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="address"><?php _e('Address','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="address" class="form-control validate[required]" type="text"  name="address" 
				value="<?php if($edit){ echo $user_info->address;}elseif(isset($_POST['address'])) echo $_POST['address'];?>">
			</div>
		</div>
		
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="city_name"><?php _e('City','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="city_name" class="form-control validate[required]" type="text"  name="city_name" 
				value="<?php if($edit){ echo $user_info->city_name;}elseif(isset($_POST['city_name'])) echo $_POST['city_name'];?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="state_name"><?php _e('State','school-mgt');?></label>
			<div class="col-sm-8">
				<input id="state_name" class="form-control" type="text"  name="state_name" 
				value="<?php if($edit){ echo $user_info->state;}elseif(isset($_POST['state_name'])) echo $_POST['state_name'];?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="zip_code"><?php _e('Zip Code','school');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="zip_code" class="form-control  validate[required,custom[onlyLetterNumber]]" type="text"  name="zip_code" 
				value="<?php if($edit){ echo $user_info->zip_code;}elseif(isset($_POST['zip_code'])) echo $_POST['zip_code'];?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label " for="mobile"><?php _e('Mobile Number','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input type="hidden" readonly value=""  class="form-control" name="phonecode">
				<input id="mobile" class="form-control validate[required,custom[phone]] text-input" type="text"  name="mobile" maxlength="10"
				value="<?php if($edit){ echo $user_info->mobile;}elseif(isset($_POST['mobile'])) echo $_POST['mobile'];?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label " for="phone"><?php _e('Phone','gym_mgt');?></label>
			<div class="col-sm-8">
				<input id="phone" class="form-control validate[,custom[phone]] text-input" type="text"  name="phone" 
				value="<?php if($edit){ echo $user_info->phone;}elseif(isset($_POST['phone'])) echo $_POST['phone'];?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label " for="email"><?php _e('Email','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="email" class="form-control validate[required,custom[email]] text-input" type="text"  name="email" 
				value="<?php if($edit){ echo $user_info->user_email;}elseif(isset($_POST['email'])) echo $_POST['email'];?>">
			</div>
		</div>
		<?php
/* 	<div class="physical_info">	
		<hr>
		<div class="header">	<hr>
			<h3><?php _e('Physical Information','gym_mgt');?></h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="weight"><?php _e('Weight','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="weight" class="form-control validate[required] text-input" type="text" 
				palceholder = "Enter in centimeter"
				value="<?php if($edit){ echo $user_info->weight;}elseif(isset($_POST['weight'])) echo $_POST['weight'];?>" 
				name="weight" placeholder="<?php echo get_option( 'gmgt_weight_unit' );?>">
				
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="height"><?php _e('Height','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="height" class="form-control validate[required] text-input" type="text" value="<?php if($edit){ echo $user_info->height;}elseif(isset($_POST['height'])) echo $_POST['height'];?>" 
				name="height" placeholder="<?php echo get_option( 'gmgt_height_unit' );?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="Chest"><?php _e('Chest','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="Chest" class="form-control validate[required] text-input" type="text" 
				value="<?php if($edit){ echo $user_info->chest;}elseif(isset($_POST['chest'])) echo $_POST['chest'];?>" name="chest" 
				placeholder="<?php echo get_option( 'gmgt_chest_unit' );?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="Waist"><?php _e('Waist','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="waist" class="form-control validate[required] text-input" type="text" 
				value="<?php if($edit){ echo $user_info->waist;}elseif(isset($_POST['waist'])) echo $_POST['waist'];?>" name="waist" 
				placeholder="<?php echo get_option( 'gmgt_waist_unit' );?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="thigh"><?php _e('Thigh','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="thigh" class="form-control validate[required] text-input" type="text" 
				value="<?php if($edit){ echo $user_info->thigh;}elseif(isset($_POST['thigh'])) echo $_POST['thigh'];?>" name="thigh" 
				placeholder="<?php echo get_option( 'gmgt_thigh_unit' );?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="arms"><?php _e('Arms','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="arms" class="form-control validate[required] text-input" type="text" 
				value="<?php if($edit){ echo $user_info->arms;}elseif(isset($_POST['arms'])) echo $_POST['arms'];?>" name="arms" 
				placeholder="<?php echo get_option( 'gmgt_arms_unit' );?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fat"><?php _e('Fat','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="fat" class="form-control validate[required] text-input" type="text" 
				value="<?php if($edit){ echo $user_info->fat;}elseif(isset($_POST['fat'])) echo $_POST['fat'];?>" name="fat" 
				placeholder="<?php echo get_option( 'gmgt_fat_unit' );?>">
			</div>
		</div>
	</div> */ ?>
		<div class="header">
			<hr>
			<h3><?php _e('Login Information','gym_mgt');?></h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="username"><?php _e('User Name','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="username" class="form-control validate[required]" type="text"  name="username" 
				value="<?php if($edit){ echo $user_info->user_login;}elseif(isset($_POST['username'])) echo $_POST['username'];?>" <?php if($edit) echo "readonly";?>>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="password"><?php _e('Password','gym_mgt');?><?php if(!$edit) {?><span class="require-field">*</span><?php }?></label>
			<div class="col-sm-8">
				<input id="password" class="form-control <?php if(!$edit) echo 'validate[required]';?>" type="password"  name="password" value="">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="photo"><?php _e('Image','gym_mgt');?></label>
			<div class="col-sm-2">
				<input type="text" id="gmgt_user_avatar_url" class="form-control" name="gmgt_user_avatar"  
				value="<?php if($edit)echo esc_url( $user_info->gmgt_user_avatar );elseif(isset($_POST['gmgt_user_avatar'])) echo $_POST['gmgt_user_avatar']; ?>" />
			</div>	
				<div class="col-sm-3">
       				 <input id="upload_user_avatar_button" type="button" class="button" value="<?php _e( 'Upload image', 'gym_mgt' ); ?>" />
       				 <span class="description"><?php _e('Upload image', 'gym_mgt' ); ?></span>
       		
			</div>
			<div class="clearfix"></div>
			
			<div class="col-sm-offset-2 col-sm-8">
                     <div id="upload_user_avatar_preview" >
	                     <?php if($edit) 
	                     	{
	                     	if($user_info->gmgt_user_avatar == "")
	                     	{?>
	                     	<img alt="" src="<?php echo get_option( 'gmgt_system_logo' ); ?>">
	                     	<?php }
	                     	else {
	                     		?>
					        <img style="max-width:100%;" src="<?php if($edit)echo esc_url( $user_info->gmgt_user_avatar ); ?>" />
					        <?php 
	                     	}
	                     	}
					        else {
					        	?>
					        	<img alt="" src="<?php echo get_option( 'gmgt_system_logo' ); ?>">
					        	<?php 
					        }?>
    				</div>
   		 </div>
		</div>
		<?php
		/* <div class="header">	<hr>
			<h3><?php _e('More Information','gym_mgt');?></h3>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="staff_name"><?php _e('Select Staff Member','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<?php $get_staff = array('role' => 'Staff_member');
					$staffdata=get_users($get_staff);
					
					?>
				<select name="staff_id" class="form-control validate[required] " id="staff_id">
				<option value=""><?php  _e('Select Staff Member','gym_mgt');?></option>
				<?php if($edit)
						$staff_data=$user_info->staff_id;
					elseif(isset($_POST['staff_id']))
						$staff_data=$_POST['staff_id'];
					else
						$staff_data="";
					if(!empty($staffdata))
					{
					foreach($staffdata as $staff)
					{
						
						echo '<option value='.$staff->ID.' '.selected($staff_data,$staff->ID).'>'.$staff->display_name.'</option>';
					}
					}
					?>
				</select>
			</div>
			<div class="col-sm-2">
			<a href="?page=gmgt_staff&tab=add_staffmember" class="btn btn-default"> <?php _e('Add Staff Member','gym_mgt');?></a>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="intrest"><?php _e('Intrest Area','gym_mgt');?></label>
			<div class="col-sm-8">
			
				<select class="form-control" name="intrest_area" id="intrest_area">
				<option value=""><?php _e('Select Intrest','gym_mgt');?></option>
				<?php 
				
				if(isset($_REQUEST['intrest']))
					$category =$_REQUEST['intrest'];  
				elseif($edit)
					$category =$user_info->intrest_area;
				else 
					$category = "";
				
				$role_type=gmgt_get_all_category('intrest_area');
				if(!empty($role_type))
				{
					foreach ($role_type as $retrive_data)
					{
						echo '<option value="'.$retrive_data->ID.'" '.selected($category,$retrive_data->ID).'>'.$retrive_data->post_title.'</option>';
					}
				}
				?>
				
				</select>
			</div>
			<div class="col-sm-2"><button id="addremove" model="intrest_area"><?php _e('Add Or Remove','gym_mgt');?></button></div>
		</div>
		<?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="member_convert"><?php  _e(' Convert into Staff Member','gym_mgt');?></label>
				<div class="col-sm-8">
				<input type="checkbox"  name="member_convert" value="staff_member">
				
				</div>
		</div>
		<?php }?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="Source"><?php _e('Source','gym_mgt');?></label>
			<div class="col-sm-8">
			
				<select class="form-control" name="source" id="source">
				<option value=""><?php _e('Select Source','gym_mgt');?></option>
				<?php 
				
				if(isset($_REQUEST['source']))
					$category =$_REQUEST['source'];  
				elseif($edit)
					$category =$user_info->source;
				else 
					$category = "";
				
				$role_type=gmgt_get_all_category('source');
				if(!empty($role_type))
				{
					foreach ($role_type as $retrive_data)
					{
						echo '<option value="'.$retrive_data->ID.'" '.selected($category,$retrive_data->ID).'>'.$retrive_data->post_title.'</option>';
					}
				}
				?>
				
				</select>
			</div>
			<div class="col-sm-2"><button id="addremove" model="source"><?php _e('Add Or Remove','gym_mgt');?></button></div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="refered"><?php _e('Reffered By','gym_mgt');?></label>
			<div class="col-sm-8">
				<?php $get_staff = array('role' => 'Staff_member');
					$staffdata=get_users($get_staff);
					
					?>
				<select name="reference_id" class="form-control" id="reference_id">
				<option value=""><?php  _e('Select Reffered Member','gym_mgt');?></option>
				<?php if($edit)
						$staff_data=$user_info->reference_id;
					elseif(isset($_POST['reference_id']))
						$staff_data=$_POST['reference_id'];
					else
						$staff_data="";
					
					
					if(!empty($staffdata))
					{
					foreach($staffdata as $staff)
					{
						
						echo '<option value='.$staff->ID.' '.selected($staff_data,$staff->ID).'>'.$staff->display_name.'</option>';
					}
					}
					?>
				</select>
			</div>
			<div class="col-sm-2">
			<a href="?page=gmgt_staff&tab=add_staffmember" class="btn btn-default"> <?php _e('Add Staff Member','gym_mgt');?></a>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="inqiury_date"><?php _e('Inquiry Date','gym_mgt');?></label>
			<div class="col-sm-8">
				<input id="inqiury_date" class="form-control" type="text"  name="inqiury_date" 
				value="<?php if($edit){ echo $user_info->inqiury_date;}elseif(isset($_POST['inqiury_date'])) echo $_POST['inqiury_date'];?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="triel_date"><?php _e('Trial End Date','gym_mgt');?></label>
			<div class="col-sm-8">
				<input id="triel_date" class="form-control" type="text"  name="triel_date" 
				value="<?php if($edit){ echo $user_info->triel_date;}elseif(isset($_POST['triel_date'])) echo $_POST['triel_date'];?>">
			</div>
		</div>

		<?php if($edit)
		{?>
			<div class="form-group">
			<label class="col-sm-2 control-label" for="membership_status"><?php _e('Membership Status','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			<?php $membership_statusval = "Continue"; if($edit){ $membership_statusval=$user_info->membership_status; }elseif(isset($_POST['membership_status'])) {$membership_statusval=$_POST['membership_status'];}?>
				<label class="radio-inline">
			     <input type="radio" value="Continue" class="tog validate[required]" name="membership_status"  <?php  checked( 'Continue', $membership_statusval);  ?>/><?php _e('Continue','gym_mgt');?>
			    </label>
				<label class="radio-inline">
			     <input type="radio" value="Expired" class="tog validate[required]" name="membership_status"  <?php  checked( 'Expired', $membership_statusval);  ?>/><?php _e('Expired','gym_mgt');?>
			    </label>
			    <label class="radio-inline">
			      <input type="radio" value="Dropped" class="tog validate[required]" name="membership_status"  <?php  checked( 'Dropped', $membership_statusval);  ?>/><?php _e('Dropped','gym_mgt');?> 
			    </label>
			</div>
		</div>
		<?php }?>
		<!--
		<div class="form-group">
			<label class="col-sm-2 control-label" for="auto-renew"><?php _e('Auto Renew','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			<?php $auto_renewval = "No"; if($edit){ $auto_renewval=$user_info->auto_renew; }elseif(isset($_POST['auto_renew'])) {$auto_renewval=$_POST['auto_renew'];}?>
				<label class="radio-inline">
			     <input type="radio" value="Yes" class="tog validate[required]" name="auto_renew"  <?php  checked( 'Yes', $auto_renewval);  ?>/><?php _e('Yes','gym_mgt');?>
			    </label>
			    <label class="radio-inline">
			      <input type="radio" value="No" class="tog validate[required]" name="auto_renew"  <?php  checked( 'No', $auto_renewval);  ?>/><?php _e('No','gym_mgt');?> 
			    </label>
			</div>
		</div>
		-->
		<input type="hidden" name="auto_renew" value="No">
		<div class="form-group">
			<label class="col-sm-2 control-label" for="begin_date"><?php _e('Membership Valid From','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-3">
				<input id="begin_date" class="form-control validate[required]" type="text"  name="begin_date" 
				value="<?php if($edit){ echo $user_info->begin_date;}elseif(isset($_POST['begin_date'])) echo $_POST['begin_date'];?>">
			</div>
			<div class="col-sm-1 text-center">
				<?php _e('To','gym_mgt');?>
			</div>
			<div class="col-sm-4">
				<input id="end_date" class="form-control validate[required]" type="text"  name="end_date" 
				value="<?php if($edit){ echo $user_info->end_date;}elseif(isset($_POST['end_date'])) echo $_POST['end_date'];?>" readonly>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="first_payment_date"><?php _e('First Payment Date','gym_mgt');?></label>
			<div class="col-sm-8">
				<input id="first_payment_date" class="form-control" type="text"  name="first_payment_date" 
				value="<?php if($edit){ echo $user_info->first_payment_date;}elseif(isset($_POST['first_payment_date'])) echo $_POST['first_payment_date'];?>">
			</div>
		</div> */
		?>
		
		<?php if($edit)
		{?>
			<div class="form-group">
			<label class="col-sm-2 control-label" for="membership_status"><?php _e('Membership Status','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			<?php $membership_statusval = "Continue"; if($edit){ $membership_statusval=$user_info->membership_status; }elseif(isset($_POST['membership_status'])) {$membership_statusval=$_POST['membership_status'];}?>
			
				<label class="radio-inline">
			     <input type="radio" value="Active" class="tog validate[required]" name="membership_status"  <?php  checked( 'Active', $membership_statusval);  ?>/><?php _e('Active','gym_mgt');?>
			    </label>
			    <label class="radio-inline">
			      <input type="radio" value="Dropped" class="tog validate[required]" name="membership_status"  <?php  checked( 'Dropped', $membership_statusval);  ?>/><?php _e('Dropped','gym_mgt');?> 
			    </label>
			</div>
		</div>
		<?php }?>

		<div class="col-sm-offset-2 col-sm-8">
        	
        	<input type="submit" value="<?php if($edit){ _e('Save','gym_mgt'); }else{ _e('Save Member','gym_mgt');}?>" name="save_member" class="btn btn-success"/>
        </div>
		
		
		
        </form>
        </div>
        
     <?php 
	 }
	 ?>