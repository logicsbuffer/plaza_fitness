<?php 
$obj_gyme = new Gym_management();
?>
<script type="text/javascript">
$(document).ready(function() {
	$('#member_form').validationEngine();
	$('#birth_date').datepicker({
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
} );
</script>
<?php

$user_id=0;

$user_id = get_current_user_id();
$user_meta = get_userdata($user_id);
$user_roles = $user_meta->roles;
if($user_roles[0] == 'staff_member'){
	$trainer_user_id = get_current_user_id();
	if(isset($_REQUEST['member_id'])){
		$user_id = $_REQUEST['member_id'];
	}elseif($_REQUEST['submit_userid']){
		$selected_member_id = $_POST['user_id'];
		update_user_meta($trainer_user_id,'selected_member',$selected_member_id);
		$user_id = get_user_meta ($trainer_user_id,'selected_member',true);

	}else{
			$selected_member_user_id = get_user_meta ($trainer_user_id,'selected_member',true);
			if($selected_member_user_id){
			$user_id = $selected_member_user_id;
			}else{
			$get_members = array(
				 'role' => 'member',
				 'orderby' => 'user_nicename',
				 'order' => 'ASC'
				);
			$membersdata = get_users($get_members);
			$first_member_id = $membersdata[0]->data->ID;
			$user_id = $first_member_id;
			}
	}
}elseif($user_roles[0] == 'member'){
	$user_id = get_current_user_id();
}

	
if (isset($_REQUEST['milestone_submit'])){
	$mile_stone = $_POST['mile_stone'];
	update_user_meta ($user_id,'mile_stone',$mile_stone);
}
if (isset($_REQUEST['strength_band_submit'])){
	$strength_band = $_POST['strength_band'];
	update_user_meta ($user_id,'strength_band',$strength_band);
}
if (isset($_REQUEST['body_weight_submit'])){
	$body_weight = $_REQUEST['body_weight'];
	$body_weight_date = $_REQUEST['body_weight_date'];

	// body weight
	$body_weight_data = get_user_meta ($user_id,'body_weight',true);
	if($body_weight_data){
		$body_weight_data = explode(",",$body_weight_data);
		array_push($body_weight_data , $body_weight);
	}else{
		$body_weight_data = array($body_weight);
	}
	$body_weight_str = implode(",",$body_weight_data);
	update_user_meta( $user_id, 'body_weight', $body_weight_str );

	//Body Weight date
	$body_weight_date_data = get_user_meta ($user_id,'body_weight_date',true);
	if($body_weight_date_data){
		$body_weight_date_data = explode(",",$body_weight_date_data);
		array_push($body_weight_date_data , $body_weight_date);
	}else{
		$body_weight_date_data = array($body_weight_date);
	}
	$body_weight_date_str = implode(",",$body_weight_date_data);
	
	update_user_meta( $user_id, 'body_weight_date', $body_weight_date_str );
}

if (isset($_REQUEST['body_fat_submit'])){

	$body_fat = $_REQUEST['body_fat'];
	$body_fat_date = $_REQUEST['body_fat_date'];
	// body fat
	$body_fat_data = get_user_meta ($user_id,'body_fat',true);
	if($body_fat_data){
		$body_fat_data = explode(",",$body_fat_data);
		array_push($body_fat_data , $body_fat);
	}else{
		$body_fat_data = array($body_fat);
	}
	$body_fat_str = implode(",",$body_fat_data);
	update_user_meta( $user_id, 'body_fat', $body_fat_str );

	//Body Fat date
	$body_fat_date_data = get_user_meta ($user_id,'body_fat_date',true);
	if($body_fat_date_data){
		$body_fat_date_data = explode(",",$body_fat_date_data);
		array_push($body_fat_date_data , $body_fat_date);
	}else{
		$body_fat_date_data = array($body_fat_date);
	}
	$body_fat_date_str = implode(",",$body_fat_date_data);
	
	update_user_meta( $user_id, 'body_fat_date', $body_fat_date_str );

}
if (isset($_REQUEST['body_mas_index_submit'])){

	$body_mass_index = $_REQUEST['body_mass_index'];
	$body_massi_date = $_REQUEST['body_massi_date'];
	//echo $body_massi_date;
	// body mass index
	$body_mass_index_data = get_user_meta ($user_id,'body_mass_index',true);
	if($body_mass_index_data){
		$body_mass_index_data = explode(",",$body_mass_index_data);
		array_push($body_mass_index_data , $body_mass_index);
	}else{
		$body_mass_index_data = array($body_mass_index);
	}
	$body_mass_index_str = implode(",",$body_mass_index_data);
	update_user_meta( $user_id, 'body_mass_index', $body_mass_index_str);


	//Body Mas Index date
	$body_massi_date_data = get_user_meta ($user_id,'body_massi_date',true);
	if($body_massi_date_data){
		$body_massi_date_data = explode(",",$body_massi_date_data);
		array_push($body_massi_date_data , $body_massi_date);
	}else{
		$body_massi_date_data = array($body_massi_date);
	}
	$body_massi_date_str = implode(",",$body_massi_date_data);
	update_user_meta( $user_id, 'body_massi_date', $body_massi_date_str );
	
}

if (isset($_REQUEST['push_up_max_submit'])){

	$push_up_max = $_REQUEST['push_up_max'];
	$pushup_max_date = $_REQUEST['pushup_max_date'];

	// push_up_max
	$push_up_max_data = get_user_meta ($user_id,'push_up_max',true);
	if($push_up_max_data){
		$push_up_max_data = explode(",",$push_up_max_data);
		array_push($push_up_max_data , $push_up_max);
	}else{
		$push_up_max_data = array($push_up_max);
	}
	$push_up_max_str = implode(",",$push_up_max_data);
	update_user_meta( $user_id, 'push_up_max', $push_up_max_str );	
	//update_user_meta( $user_id, 'push_up_max', '' );	

	//Push Up max date
	$pushup_max_date_data = get_user_meta ($user_id,'pushup_max_date',true);
	if($pushup_max_date_data){
		$pushup_max_date_data = explode(",",$pushup_max_date_data);
		array_push($pushup_max_date_data , $pushup_max_date);
	}else{
		$pushup_max_date_data = array($pushup_max_date);
	}
	$pushup_max_date_str = implode(",",$pushup_max_date_data);
	
	update_user_meta( $user_id, 'pushup_max_date', $pushup_max_date_str );
	//update_user_meta( $user_id, 'pushup_max_date', '' );
}



if (isset($_REQUEST['crawl_max_submit'])){

	$crawl_max = $_REQUEST['crawl_max'];
	$crawl_max_date = $_REQUEST['crawl_max_date'];

	// crawl_max
	$crawl_max_data = get_user_meta ($user_id,'crawl_max',true);
	if($crawl_max_data){
		$crawl_max_data = explode(",",$crawl_max_data);
		array_push($crawl_max_data , $crawl_max);
	}else{
		$crawl_max_data = array($crawl_max);
	}
	$crawl_max_str = implode(",",$crawl_max_data);
	update_user_meta( $user_id, 'crawl_max', $crawl_max_str );

	//Crawl max date
	$crawl_max_date_data = get_user_meta ($user_id,'crawl_max_date',true);
	if($crawl_max_date_data){
		$crawl_max_date_data = explode(",",$crawl_max_date_data);
		array_push($crawl_max_date_data , $crawl_max_date);
	}else{
		$crawl_max_date_data = array($crawl_max_date);
	}
	$crawl_max_date_str = implode(",",$crawl_max_date_data);
	
	update_user_meta( $user_id, 'crawl_max_date', $crawl_max_date_str );
	
}
if (isset($_REQUEST['plank_max_submit'])){

	$plank_max = $_REQUEST['plank_max'];
	$plank_max_date = $_REQUEST['plank_max_date'];

	// plank_max
	$plank_max_data = get_user_meta ($user_id,'plank_max',true);
	if($plank_max_data){
		$plank_max_data = explode(",",$plank_max_data);
		array_push($plank_max_data , $plank_max);
	}else{
		$plank_max_data = array($plank_max);
	}
	$plank_max_str = implode(",",$plank_max_data);
	update_user_meta( $user_id, 'plank_max', $plank_max_str );
	
	//Plank max date
	$plank_max_date_data = get_user_meta ($user_id,'plank_max_date',true);
	if($plank_max_date_data){
		$plank_max_date_data = explode(",",$plank_max_date_data);
		array_push($plank_max_date_data , $plank_max_date);
	}else{
		$plank_max_date_data = array($plank_max_date);
	}
	$plank_max_date_str = implode(",",$plank_max_date_data);
	update_user_meta( $user_id, 'plank_max_date', $plank_max_date_str );
}
if (isset($_REQUEST['pullup_max_submit'])){
	
	$pull_up_max = $_REQUEST['pull_up_max'];
	$pullup_max_date = $_REQUEST['pullup_max_date'];

	// pull_up_max
	$pull_up_max_data = get_user_meta ($user_id,'pull_up_max',true);
	if($pull_up_max_data){
		$pull_up_max_data = explode(",",$pull_up_max_data);
		array_push($pull_up_max_data , $pull_up_max);
	}else{
		$pull_up_max_data = array($pull_up_max);
	}
	$pull_up_max_str = implode(",",$pull_up_max_data);
	update_user_meta( $user_id, 'pull_up_max', $pull_up_max_str );


	//Pullup max date
	$pullup_max_date_data = get_user_meta ($user_id,'pullup_max_date',true);
	if($pullup_max_date_data){
		$pullup_max_date_data = explode(",",$pullup_max_date_data);
		array_push($pullup_max_date_data , $pullup_max_date);
	}else{
		$pullup_max_date_data = array($pullup_max_date);
	}
	$pullup_max_date_str = implode(",",$pullup_max_date_data);
	update_user_meta( $user_id, 'pullup_max_date', $pullup_max_date_str );
	
}


if (isset($_REQUEST['save_measurement'])){
			
			//$user_id = $_REQUEST['user_id'];
			/* $body_weight = implode(",",$body_weight);
			$body_fat = implode(",",$body_fat);
			$body_mass_index = implode(",",$body_mass_index);
			$push_up_max = implode(",",$push_up_max);
			$pull_up_max = implode(",",$pull_up_max);
			$crawl_max = implode(",",$crawl_max); 
			$plank_max = implode(",",$plank_max);*/
		
			//$array1= array('Mathematics','Physics');
			
			
		/* 	update_user_meta( $user_id, 'body_weight', '' );
			update_user_meta( $user_id, 'body_weight_date', '' );
			update_user_meta( $user_id, 'body_fat', '' ); */
						
		}
$edit ='allowed';
				
$user_info = get_userdata($user_id);

$user_info = get_userdata($user_id);

$strength_band_meta = get_user_meta ($user_id,'strength_band',true);
if($strength_band_meta){
	$strength_band_arr = explode("|",$strength_band_meta);
}

$strength_band_arr = '';
$mile_stone_arr ='';
$strength_band ='';
$strength_band_name ='';
$mile_stone ='';
$mile_stone_name ='';

$strength_band = get_user_meta ($user_id,'strength_band',true);
if($strength_band){
	$strength_band_arr = explode("|",$strength_band);
	$strength_band = $strength_band_arr[1];
	$strength_band_name = $strength_band_arr[0];
}

$mile_stone = get_user_meta ($user_id,'mile_stone',true);
if($mile_stone){
	$mile_stone_arr = explode("|",$mile_stone);
	$mile_stone = $mile_stone_arr[1];
	$mile_stone_name = $mile_stone_arr[0];
}
 
$body_weight = get_user_meta( $user_id, 'body_weight',true);
$body_weight_date = get_user_meta( $user_id, 'body_weight_date',true);
$body_fat = get_user_meta( $user_id, 'body_fat',true);
$body_mass_index = get_user_meta( $user_id, 'body_mass_index',true);
$push_up_max = get_user_meta( $user_id, 'push_up_max',true);
$crawl_max = get_user_meta( $user_id, 'crawl_max',true);
$plank_max = get_user_meta( $user_id, 'plank_max',true);
$pull_up_max = get_user_meta( $user_id, 'pull_up_max',true);
$body_massi_date_data = get_user_meta ($user_id,'body_massi_date',true);
$push_upmax_date_set = get_user_meta( $user_id, 'pushup_max_date',true); 
$crawl_max_date_set = get_user_meta( $user_id, 'crawl_max_date',true);  		
$plank_max_date_set = get_user_meta( $user_id, 'plank_max_date',true); 
$pullup_max_date_set = get_user_meta( $user_id, 'pullup_max_date',true); 
				
if($body_weight){
	$body_weight_arr = explode(",",$body_weight);
}
$body_weight_recent = end($body_weight_arr);

if($body_fat){
	$body_fat_arr = explode(",",$body_fat);
}
$body_fat_recent = end($body_fat_arr);

//BMI
if($body_mass_index){
	$body_mass_index_arr = explode(",",$body_mass_index);
}
$body_mass_index_recent = end($body_mass_index_arr);

if($body_massi_date_data){
	$body_mass_index_date_arr = explode(",",$body_massi_date_data);
}
$body_massi_recent_date = end($body_mass_index_date_arr);

// Push up max
if($push_up_max){
	$push_up_max_arr = explode(",",$push_up_max);
}
$push_up_max_recent = end($push_up_max_arr);

if($push_upmax_date_set){
	$push_up_max_date_arr = explode(",",$push_upmax_date_set);
}
$pushup_max_recent_date = end($push_up_max_date_arr);

// Crawl Max
if($crawl_max){
	$crawl_max_arr = explode(",",$crawl_max);
}
$crawl_max_recent = end($crawl_max_arr);

if($crawl_max_date_set){
	$crawl_max_date_date_arr = explode(",",$crawl_max_date_set);
}
$crawl_max_recent_date = end($crawl_max_date_date_arr);
// Plank Max
if($plank_max){
	$plank_max_arr = explode(",",$plank_max);
}
$plank_max_recent = end($plank_max_arr);

if($plank_max_date_set){
	$plank_max_arr = explode(",",$plank_max_date_set);
}
$plank_max_recent_date = end($plank_max_arr);

// PUll Up max
if($pull_up_max){
	$pull_up_max_arr = explode(",",$pull_up_max);
}
$pull_up_max_recent = end($pull_up_max_arr);
if($pullup_max_date_set){
$pullup_max_date_arr = explode(",",$pullup_max_date_set);
}
$pullup_max_recent_date = end($pullup_max_date_arr);

//$user_id = get_option( "sel_userid" ); ?>	

		
<div class="panel-body">
<div class="member_view_row1">
	<div class="user_sel_main row">
			
		<form name="user_sel_form" action="" method="POST" class="form-horizontal" id="user_sel_form">
			
			<?php if($obj_gym->role=='staff_member' || $obj_gym->role=='accountant'){ ?>
			<label class="col-sm-2 control-label" for="day"><?php _e('Select Member','gym_mgt');?><span class="require-field">*</span></label>	
			<div class="col-sm-6">
				<select id="member_list" class="form-control display-members" name="user_id" required>
				<option value=""><?php _e('Select Member','gym_mgt');?></option>
					<?php 
					//$get_members = array('role' => 'member');
					$get_members = array(
						 'role' => 'member',
						 'orderby' => 'user_nicename',
						 'order' => 'ASC'
						);
					
					$membersdata = get_users($get_members);
					 if(!empty($membersdata))
					 	 {
						foreach ($membersdata as $member){?>
							<?php 
							$member_id = $member->ID;
							$trainer_id = get_current_user_id();	
							$member_trainer = get_user_meta($member_id, 'member_trainer',true);

							if($trainer_id == $member_trainer){ ?> 
							<option value="<?php echo $member->ID;?>" <?php selected($member_id,$user_id);?>><?php echo $member->display_name." - ".$member->ID; ?> </option>
						<?php }
						}
					 }?>
			</select>
			</div>
			<div class="col-sm-2">
				<input type="submit" class="btn btn-info" value="Load member Data" name="submit_userid">
			</div>
			<?php }
			else
			{?>
				<input type="hidden" id="member_list" name="user_id" value="<?php echo $user_id;?>">
			<?php } ?>
			

		</form>
	</div>
	<div class="col-md-8 col-sm-12 membr_left">
		<div class="col-md-6 col-sm-12 left_side">
		<?php 
		if($user_info->gmgt_user_avatar == "")
			{?>
			<img alt="" src="<?php echo get_option( 'gmgt_system_logo' ); ?>">
			<?php }
			else {
				?>
			<img style="max-width:100%;" src="<?php if($edit)echo esc_url( $user_info->gmgt_user_avatar ); ?>" />
			<?php 
			}
		?>
		</div>
		<div class="col-md-6 col-sm-12 right_side">
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-angle-double-right"></i> 
				<?php _e('Member Name','gym_mgt'); ?>	
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color">
					<?php echo $user_info->display_name;?> 
				</span>
			</div>
		</div>	
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-angle-double-right"></i> 
				<?php _e('Body Weight','gym_mgt'); ?>	
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color">
					<?php echo $body_weight_recent;?> 
				</span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-angle-double-right"></i> 
				<?php _e('Body Fat','gym_mgt');?> 	
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color"><?php echo $body_fat_recent; ?></span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td"><i class="fa fa-angle-double-right"></i> <?php _e('Body Mass Index','gym_mgt');?> </div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color">
					<span class="txt_color"><?php echo $body_mass_index_recent; ?> </span>
				</span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-angle-double-right"></i> <?php _e('Push Up Max','gym_mgt');?>	
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color"><?php echo $push_up_max_recent; ?></span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-angle-double-right"></i> <?php _e('Crawl Max','gym_mgt');?> 
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color"><?php echo $crawl_max_recent; ?></span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-angle-double-right"></i> <?php _e('Plank Max','gym_mgt');?>
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color"><?php echo $plank_max_recent; ?></span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-angle-double-right"></i> <?php _e('Pull Up Max','gym_mgt');?>
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color"><?php echo $pull_up_max_recent; ?> </span>
			</div>
		</div>
		
		</div>
	</div>
	<div class="col-md-4 col-sm-12 member_right">	
			<h2 class="section_heading">Biometrics</h2>
				<div class="panel panel-white">
					<div class="panel-body biometric_data">
						<h3><?php _e('Strength Band','gym_mgt'); ?></h3><p><?php echo $strength_band_name; ?></p>	
						<?php if($strength_band){ ?>
							<img src="<?php echo $strength_band; ?>">
						<?php } ?>
					</div>
				</div>
				<div class="panel panel-white">
				
					<div class="panel-body biometric_data">
						<h3><?php _e('Milestone','gym_mgt');?></h3><p><?php echo $mile_stone_name; ?></p>	
						<?php if($mile_stone){ ?>
							<img src="<?php echo $mile_stone; ?>">
						<?php } ?>
					</div>
				</div>
			
	</div>
	</div>
</div>
<div class="panel-body hide">
	
	<div class="clear"></div>
	<div class="col-md-6  col-sm-6  col-xs-12 border">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Weight','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Weight" 
			class="btn btn-danger right"> <?php _e('Add Weight','gym_mgt');?></a>	
		</span>
		
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Waist Report','gym_mgt');?></span>
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Waist" 
			class="btn btn-danger right"> <?php _e('Add Waist','gym_mgt');?></a>		
		</span>
		
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Height Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Height" 
			class="btn btn-danger right"> <?php _e('Add Height','gym_mgt');?></a>	
		</span>
	
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Chest Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Chest" 
			class="btn btn-danger right"> <?php _e('Add Chest','gym_mgt');?></a>	
		</span>
		
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Thigh Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Thigh" 
			class="btn btn-danger right"> <?php _e('Add Thigh','gym_mgt');?></a>	
		</span>
		
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Arms Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Arms" 
			class="btn btn-danger right"> <?php _e('Add Arms','gym_mgt');?></a>	
		</span>
		
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Fat Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Fat" 
			class="btn btn-danger right"> <?php _e('Add Fat','gym_mgt');?></a>	
		</span>
		
	</div>
</div>

<?php

?>
<div class="panel-body">
	<!-- <form name="workout_form" action="" method="post" class="form-horizontal" id="workout_form"> -->
	   <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="measurment_id" value="<?php if(isset($_REQUEST['measurment_id']))echo $_REQUEST['measurment_id'];?>">
    	
<?php

	//echo 'Body Weight:'.$body_weight;
	
	$php_array_bw = explode(",",$body_weight);
	$body_weight_array = json_encode($php_array_bw);	

	//echo 'Body Weight:'.$body_weight;
	$php_array_bf = explode(",",$body_fat);
	$body_fat_array = json_encode($php_array_bf);
	
	//$php_array_bm = explode(",",$body_mass_index);
	$body_mass_index_array = json_encode($body_mass_index_arr);

	//$php_array_pu = explode(",",$push_up_max);
	$push_up_max_array = json_encode($push_up_max_arr);
	
	$php_array_cm = explode(",",$crawl_max);
	$crawl_max_array = json_encode($php_array_cm);
	
	$php_array_pm = explode(",",$plank_max);
	$plank_max_array = json_encode($php_array_pm);
	
	$php_array_pum = explode(",",$pull_up_max);
	$pull_up_max_array = json_encode($php_array_pum);
	
?>

	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Milestone','gym_mgt');?></span>
		</span>
		<form name="milestone_form" action="" method="post" class="form-horizontal" id="milestone_form">
			
			<?php 
			$mile_stone_meta = get_user_meta ($user_id,'mile_stone',true);
			$mile_stone_current = explode("|",$mile_stone_meta);
			?>
			
			<div class="form-group">
			<label class="col-sm-4 control-label" for="weight"><?php _e('Select Milestone','gym_mgt');?><span class="require-field">*</span></label>

			<div class="col-sm-6">
				<select name="mile_stone" class="form-control" id="mile_stone" required>
					<?php if($mile_stone_current){ ?>
					<option value="<?php echo $mile_stone_meta; ?>"><?php echo $mile_stone_name; ?></option>
					<?php }else{ ?>
						<option value="">Select Milestone</option>
				  <?php	} ?>
				<option value="#:1 Find Your Flexibility | <? echo plugins_url( '/images/milestone/milestone1.png', __FILE__ ); ?>">Milestone #1: Find Your Flexibility</option>
				<option value="#2: Build A Stronger Core |<? echo plugins_url( '/images/milestone/milestone2.png', __FILE__ ); ?>">Milestone #2: Build A Stronger Core</option>
				<option value="#3: Build A Stronger Core |<? echo plugins_url( '/images/milestone/milestone3.png', __FILE__ ); ?>">Milestone #3: Move Better By Hip Hinging</option>
				<option value="#4: Improve Your Posture |<? echo plugins_url( '/images/milestone/milestone4.png', __FILE__ ); ?>">Milestone #4: Improve Your Posture</option>
				<option value="#5: Find Shoulder Flexibility |<? echo plugins_url( '/images/milestone/milestone5.png', __FILE__ ); ?>">Milestone #5: Find Shoulder Flexibility</option>
				<option value="#6: Establish Thoracic Spine Rotation |<? echo plugins_url( '/images/milestone/milestone6.png', __FILE__ ); ?>">Milestone #6: Establish Thoracic Spine Rotation</option>
				<option value="#7: Improve Mobility From The Ground Up |<? echo plugins_url( '/images/milestone/milestone7.png', __FILE__ ); ?>">Milestone #7: Improve Mobility From The Ground Up</option>
				<option value="#8: Pattern The Squat<? echo plugins_url( '/images/milestone/milestone8.png', __FILE__ ); ?>">Milestone #8: Pattern The Squat</option>
				<option value="#9: Apply The Squat And Connect To Strength |<? echo plugins_url( '/images/milestone/milestone9.png', __FILE__ ); ?>">Milestone #9: Apply The Squat And Connect To Strength</option>
				<option value="#10: Train The 5 Human Movements |<? echo plugins_url( '/images/milestone/milestone10.png', __FILE__ ); ?>">Milestone #10: Train The 5 Human Movements</option>
				<option value="#11: Develop Kettlebell Skills |<? echo plugins_url( '/images/milestone/milestone11.png', __FILE__ ); ?>">Milestone #11: Develop Kettlebell Skills</option>
				<option value="#12: Be Consistent To Look And Feel Better |<? echo plugins_url( '/images/milestone/milestone12.png', __FILE__ ); ?>">Milestone #12: Be Consistent To Look And Feel Better</option>
				<option value="#13: Develop Double Kettlebell Skills To Crush Your Workouts |<? echo plugins_url( '/images/milestone/milestone13.png', __FILE__ ); ?>">Milestone #13: Develop Double Kettlebell Skills To Crush Your Workouts</option>
				<option value="#14: Create More Strength And Power |<? echo plugins_url( '/images/milestone/milestone14.png', __FILE__ ); ?>">Milestone #14: Create More Strength And Power</option>
				<option value="#15: Confidence Is The Best Payoff |<? echo plugins_url( '/images/milestone/milestone15.png', __FILE__ ); ?>">Milestone #15: Confidence Is The Best Payoff</option>
				<option value="#16: Link The Unbreakable Chain |<? echo plugins_url( '/images/milestone/milestone16.png', __FILE__ ); ?>">Milestone #16: Link The Unbreakable Chain</option>
				<option value="#17: Looking In The Mirror Never Looked |<? echo plugins_url( '/images/milestone/milestone17.png', __FILE__ ); ?>">Milestone #17: Looking In The Mirror Never Looked So Good</option>
				<option value="#18: Activate Peak Performance |<? echo plugins_url( '/images/milestone/milestone18.png', __FILE__ ); ?>">Milestone #18: Activate Peak Performance</option>
				<option value="#19: Maintain Peak Performance |<? echo plugins_url( '/images/milestone/milestone19.png', __FILE__ ); ?>">Milestone #19: Maintain Peak Performance</option>
				<option value="#20: Live An Active Life And Win |<? echo plugins_url( '/images/milestone/milestone20.png', __FILE__ ); ?>">Milestone #20: Live An Active Life And Win</option>
				<option value="#21: Respect The Practice And The Process |<? echo plugins_url( '/images/milestone/milestone21.png', __FILE__ ); ?>">Milestone #21: Respect The Practice And The Process</option>
				</select>
			</div>
			<div class="col-sm-2">
			<?php if($mile_stone){ ?>
				<img style="width:auto;height: 45px;" src="<?php echo $mile_stone; ?>">
			<?php } ?>
			</div>
			<div class="col-sm-8 col-sm-offset-4" style="height: 65px;overflow: hidden;">
				<input type="submit" class="btn btn-success" value="Save" name="milestone_submit">
			</div>
		
			</div>
		</form>
	</div>


	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Strength Band','gym_mgt');?></span>
		</span>
		
		<form name="strength_band_form" action="" method="post" class="form-horizontal" id="strength_band_form">
			<div class="form-group">
			<label class="col-sm-4 control-label" for="weight"><?php _e('Select Strength Band','gym_mgt');?><span class="require-field">*</span></label>

			<div class="col-sm-6">
				<select name="strength_band" class="form-control" id="strength_band" required>
					<?php if($strength_band_meta){ ?>
					<option value="<?php echo $strength_band_meta; ?>"><?php echo $strength_band_name; ?></option>
					<?php }else{ ?>
						<option value="">Select Strength Band</option>
				  <?php	} ?>
					<option value="Black Band | <? echo plugins_url( '/images/strength_band/black-band.png', __FILE__ ); ?>">Black Band</option>
					<option value="Blue Band | <? echo plugins_url( '/images/strength_band/blue-band.png', __FILE__ ); ?>">Blue Band</option>
					<option value="Orange Band | <? echo plugins_url( '/images/strength_band/orange-band.png', __FILE__ ); ?>">Orange Band</option>
					<option value="Red Band | <? echo plugins_url( '/images/strength_band/red-band.png', __FILE__ ); ?>">Red Band</option>
					<option value="Green Band | <? echo plugins_url( '/images/strength_band/green-band.png', __FILE__ ); ?>">Green Band</option>
					<option value="White Band | <? echo plugins_url( '/images/strength_band/white-band.png', __FILE__ ); ?>">White Band</option>
					<option value="Yellow Band | <? echo plugins_url( '/images/strength_band/yellow-band.png', __FILE__ ); ?>">Yellow Band</option>

				</select>
			</div>
			<div class="col-sm-2">
			<?php if($strength_band){ ?>
				<img style="width:auto;height: 45px;" src="<?php echo $strength_band; ?>">
			<?php } ?>
			</div>
			</div>
			<div class="col-sm-8 col-sm-offset-4" style="height: 65px;overflow: hidden;">
				<input type="submit" class="btn btn-success" value="Save" name="strength_band_submit">
			</div>
		
		</form>
	</div>

	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Body Weight','gym_mgt');?></span>	
			<a id="add_body_weight" class="btn btn-danger right"> <?php _e('Add body weight','gym_mgt');?></a>	
		</span>
		<form name="body_weight_form" action="" method="post" class="form-horizontal" id="body_weight_form">
			
			<div class="biometric_section">
				<canvas id="body_weight"></canvas>
			</div>
			<br>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group body_weight_feilds" style="display:block;">
				<label class="col-sm-4 control-label" for="result"><?php _e('Most Recent','gym_mgt');?> <span class="require-field">*</span></label>
				<div class="col-sm-8">
					<input id="body_weight1" class="form-control validate[required,custom[number]] text-input" placeholder="0lb -500lbs" type="text" value="<?php echo $body_weight_recent; ?>" name="body_weight" required>
				</div>	
				</div>
			</div>
			
			<?php 
				$body_weight_date_set = get_user_meta( $user_id, 'body_weight_date',true); 
				
				if($body_weight_date_set){
					$body_weight_arr = explode(",",$body_weight_date_set);
				}
					$body_weight_recent = end($body_weight_arr);
			?>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="result_date"><?php _e('Record Date','gym_mgt');?>  <span class="require-field">*</span></label>
					<div class="col-sm-8">
					<input id="body_weight_date" class="form-control validate[required,custom[number]] text-input" placeholder="Record Date" type="date" value="<?php echo $body_weight_recent; ?>" name="body_weight_date" required>
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-sm-offset-4" style="height: 65px;overflow: hidden;">
				<input type="submit" class="btn btn-success" value="Save" name="body_weight_submit">
			</div>
		</form>
	</div>


	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Body Fat','gym_mgt');?></span>	
		<a id="add_body_fat" class="btn btn-danger right"> <?php _e('Add Body Fat','gym_mgt');?></a>
		</span>
		<form name="body_fat_form" action="" method="post" class="form-horizontal" id="body_fat_form">
			<?php 
				$body_fat_date_set = get_user_meta( $user_id, 'body_fat_date',true); 
				if($body_fat_date_set){
					$body_fat_arr = explode(",",$body_fat_date_set);
				}
					$body_fat_recent_date = end($body_fat_arr);

			?>
			<div class="biometric_section">
				<canvas id="body_fat"></canvas>
			</div>
			<br>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group body_fat_feilds" style="display:block;">
				<label class="col-sm-4 control-label" for="result"><?php _e('Most Recent','gym_mgt');?> <span class="require-field">*</span></label>
				<div class="col-sm-8">
					<input id="body_fat1" class="form-control validate[required,custom[number]] text-input" placeholder="0% - 60%" type="text" value="<?php echo $body_fat_recent; ?>" name="body_fat" required>
				</div>
				</div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="result_date"><?php _e('Record Date','gym_mgt');?>  <span class="require-field">*</span></label>
					<div class="col-sm-8">
					<input id="body_fat_date" class="form-control validate[required,custom[number]] text-input" placeholder="Record Date" type="date" value="<?php echo $body_fat_recent_date; ?>" name="body_fat_date" required>
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-sm-offset-4" style="height: 65px;overflow: hidden;">
				<input type="submit" class="btn btn-success" value="Save" name="body_fat_submit">
			</div>
		</form>
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Body Mass Index','gym_mgt');?></span>	
			<a id="add_body_mass_index" class="btn btn-danger right"> <?php _e('Body Mass Index','gym_mgt');?></a>
		</span>
		<form name="body_mas_index_form" action="" method="post" class="form-horizontal" id="body_mas_index_form">
			<div class="biometric_section">
				<canvas id="body_mass_index"></canvas>
			</div>
			<br>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group body_mass_index_feilds" style="display:block;">
				<label class="col-sm-4 control-label" for="result"><?php _e('Most Recent','gym_mgt');?> <span class="require-field">*</span></label>
				<div class="col-sm-8">
					<input id="body_mass_index1" class="form-control validate[required,custom[number]] text-input" placeholder="15 - 50" type="text" value="<?php echo $body_mass_index_recent; ?>" name="body_mass_index" required>
				</div>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="result_date"><?php _e('Record Date','gym_mgt');?>  <span class="require-field">*</span></label>
					<div class="col-sm-8">
					<input id="body_fat_date" class="form-control validate[required,custom[number]] text-input" placeholder="Record Date" type="date" value="<?php echo $body_massi_recent_date; ?>" name="body_massi_date" required>
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-sm-offset-4" style="height: 65px;overflow: hidden;">
				<input type="submit" class="btn btn-success" value="Save" name="body_mas_index_submit">
			</div>
		</form>		
	</div>

	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Push up max','gym_mgt');?></span>	
			<a id="add_push_up_max" class="btn btn-danger right"> <?php _e('Add body weight','gym_mgt');?></a>
		</span>
		<form name="push_up_max_form" action="" method="post" class="form-horizontal" id="push_up_max_form">
	
			
			<div class="biometric_section">
				<canvas id="push_up_max"></canvas>
			</div>
			<br>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group push_up_max_feilds" style="display:block;">
				<label class="col-sm-4 control-label" for="result"><?php _e('Most Recent','gym_mgt');?> <span class="require-field">*</span></label>
				<div class="col-sm-8">
					<input id="push_up_max1" class="form-control validate[required,custom[number]] text-input" placeholder="0 - 200" type="text" value="<?php echo $push_up_max_recent; ?>" name="push_up_max" required>
				</div>
				</div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="result_date"><?php _e('Record Date','gym_mgt');?>  <span class="require-field">*</span></label>
					<div class="col-sm-8">
					<input id="body_fat_date" class="form-control validate[required,custom[number]] text-input" placeholder="Record Date" type="date" value="<?php echo $pushup_max_recent_date; ?>" name="pushup_max_date" required>
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-sm-offset-4" style="height: 65px;overflow: hidden;">
				<input type="submit" class="btn btn-success" value="Save" name="push_up_max_submit">
			</div>
		</form>	
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Crawl Max','gym_mgt');?></span>	
			<a id="add_crawl_max" class="btn btn-danger right"> <?php _e('Add Crawl Max','gym_mgt');?></a>
		</span>
		<form name="crawl_max_form" action="" method="post" class="form-horizontal" id="crawl_max_form">	
			<div class="biometric_section">
				<canvas id="crawl_max"></canvas>
			</div>
			<br>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group crawl_max_feilds" style="display:block;">
				<label class="col-sm-4 control-label" for="result"><?php _e('Most Recent','gym_mgt');?> <span class="require-field">*</span></label>
				<div class="col-sm-8">
					<input id="crawl_max1" class="form-control validate[required,custom[number]] text-input" placeholder="0 min - 40 min" type="text" value="<?php echo $crawl_max_recent; ?>" name="crawl_max" required>
				</div>
				</div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="result_date"><?php _e('Record Date','gym_mgt');?>  <span class="require-field">*</span></label>
					<div class="col-sm-8">
					<input id="body_fat_date" class="form-control validate[required,custom[number]] text-input" placeholder="Record Date" type="date" value="<?php echo $crawl_max_recent_date; ?>" name="crawl_max_date" required>
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-sm-offset-4" style="height: 65px;overflow: hidden;">
				<input type="submit" class="btn btn-success" value="Save" name="crawl_max_submit">
			</div>
		</form>			
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Plank Max','gym_mgt');?></span>	
		<a id="add_plank_max" class="btn btn-danger right"> <?php _e('Add Plank Max','gym_mgt');?></a>
		</span>
		<form name="plank_max_form" action="" method="post" class="form-horizontal" id="plank_max_form">
			<div class="biometric_section">
				<canvas id="plank_max"></canvas>
			</div>
			<br>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group plank_max_feilds" style="display:block;">
				<label class="col-sm-4 control-label" for="result"><?php _e('Most Recent','gym_mgt');?> <span class="require-field">*</span></label>
				<div class="col-sm-8">
					<input id="plank_max1" class="form-control validate[required,custom[number]] text-input" placeholder="0 min - 40 min" type="text" value="<?php echo $plank_max_recent; ?>" name="plank_max" required>
				</div>
				</div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="result_date"><?php _e('Record Date','gym_mgt');?>  <span class="require-field">*</span></label>
					<div class="col-sm-8">
					<input id="body_fat_date" class="form-control validate[required,custom[number]] text-input" placeholder="Body Fat Date" type="date" value="<?php echo $plank_max_recent_date; ?>" name="plank_max_date" required>
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-sm-offset-4" style="height: 65px;overflow: hidden;">
				<input type="submit" class="btn btn-success" value="Save" name="plank_max_submit">
			</div>
		</form>			
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Pull up Max','gym_mgt');?></span>	
		<a id="add_pull_up_max" class="btn btn-danger right"> <?php _e('Add Pull up Max','gym_mgt');?></a>
		</span>
		
		<form name="pullup_max_form" action="" method="post" class="form-horizontal" id="pullup_max_form">
			<div class="biometric_section">
				<canvas id="pull_up_max"></canvas>
			</div>
			<br>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group pull_up_max_feilds" style="display:block;">
				<label class="col-sm-4 control-label" for="result"><?php _e('Pull up max','gym_mgt');?> <span class="require-field">*</span></label>
				<div class="col-sm-8">
					<input id="pull_up_max1" class="form-control validate[required,custom[number]] text-input" placeholder="Recent Value" type="text" value="<?php echo $pull_up_max_recent; ?>" name="pull_up_max" required>
				</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="pullup_max_date"><?php _e('Record Date','gym_mgt');?>  <span class="require-field">*</span></label>
					<div class="col-sm-8">
					<input id="body_fat_date" class="form-control validate[required,custom[number]] text-input" placeholder="Body Fat Date" type="date" value="<?php echo $pullup_max_recent_date; ?>" name="pullup_max_date" required>
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-sm-offset-4" style="height: 65px;overflow: hidden;">
				<input type="submit" class="btn btn-success" value="Save" name="pullup_max_submit">
			</div>
		</form>	
	</div>
	
		<!--
		<div class="col-sm-offset-4 col-sm-4" style="text-align: center;">
        	<input value="Save Biometerics" name="save_measurement" class="btn btn-success" style="width: 80%;" type="submit">
        </div>
   		</form>
		-->
</div>

<?php
/* update_user_meta ($user_id,'body_weight','');
update_user_meta ($user_id,'body_fat','');
update_user_meta ($user_id,'push_up_max','');
update_user_meta ($user_id,'crawl_max','');
update_user_meta ($user_id,'pull_up_max','');
update_user_meta ($user_id,'plank_max','');
update_user_meta ($user_id,'body_mass_index','');
update_user_meta ($user_id,'body_weight_date','');
update_user_meta ($user_id,'body_fat_date','');
update_user_meta ($user_id,'pushup_max_date','');
update_user_meta ($user_id,'crawl_max_date','');
update_user_meta ($user_id,'pullup_max_date','');
update_user_meta ($user_id,'plank_max_date','');
update_user_meta ($user_id,'body_massi_date','');  */


// Set date on graph
$php_array_pm = explode(",",$pullup_max_date_set);
$pull_up_max_date_array = json_encode($php_array_pm);

$php_array_plm = explode(",",$plank_max_date_set);
$plank_max_date_array = json_encode($php_array_plm);

$php_array_cm = explode(",",$crawl_max_date_set);
$crawl_max_date_array = json_encode($php_array_cm);

$php_array_bw = explode(",",$body_weight_date_set);
$body_weight_date_array = json_encode($php_array_bw);

$php_array_bf = explode(",",$body_fat_date_set);
$body_fat_date_array = json_encode($php_array_bf);

$php_array_pu = explode(",",$push_upmax_date_set);
$push_up_max_date_array = json_encode($php_array_pu);

$php_array_bm_date = explode(",",$body_massi_date_data);
$body_mass_date_array = json_encode($php_array_bm_date);

?>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>

<script>
$(document).ready(function() {

  jQuery("#body_weight1").inputmask("numeric", {
    min: 0,
    max: 500,
    rightAlign: false
  });
  jQuery("#body_fat1").inputmask("numeric", {
    min: 0,
    max: 60,
    rightAlign: false
  });
  jQuery("#body_mass_index1").inputmask("numeric", {
    min: 15,
    max: 50,
    rightAlign: false
  });
  jQuery("#push_up_max1").inputmask("numeric", {
    min: 0,
    max: 200,
    rightAlign: false
  });
  jQuery("#crawl_max1").inputmask("numeric", {
    min: 0,
    max: 40,
    rightAlign: false
  });
  jQuery("#plank_max1").inputmask("numeric", {
    min: 0,
    max: 40,
    rightAlign: false
  });
  jQuery("#plank_max1").inputmask("numeric", {
    min: 0,
    max: 40,
    rightAlign: false
  });

});

	var body_weight = document.getElementById('body_weight').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $body_weight_date_array; ?>,
			datasets: [{
				label: 'Body Weight',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $body_weight_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'lbs'
						},
						ticks: {
							max: 500,
							min: 0,
							suggestedMax: 500,
							suggestedMin: 0,
						}
					}]
				}
			}

		// Configuration options go here
		});
	var body_weight = document.getElementById('body_fat').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $body_fat_date_array; ?>,
			datasets: [{
				label: 'Body Fat',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $body_fat_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'percentage'
						},
						ticks: {
							max: 60,
							min: 0,
							suggestedMax: 60,
							suggestedMin: 0,
						}
					}]
				}
			}

		// Configuration options go here
		});
	var body_weight = document.getElementById('body_mass_index').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $body_mass_date_array; ?>,
			datasets: [{
				label: 'Body Mass Index',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $body_mass_index_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'numbers'
						},
						ticks: {
							max: 50,
							min: 15,
							suggestedMax: 50,
							suggestedMin: 15,
						}
					}]
				}
			}

		// Configuration options go here
		});
		
	var body_weight = document.getElementById('push_up_max').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $push_up_max_date_array; ?>,
			datasets: [{
				label: 'Push up',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $push_up_max_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'numbers'
						},
						ticks: {
							max: 200,
							min: 0,
							suggestedMax: 200,
							suggestedMin: 0,
						}
					}]
				}
			}

		// Configuration options go here
		});
		
	var body_weight = document.getElementById('pull_up_max').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $pull_up_max_date_array; ?>,
			datasets: [{
				label: 'Pull Up',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $pull_up_max_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'numbers'
						},
						ticks: {
							max: 40,
							min: 0,
							suggestedMax: 40,
							suggestedMin: 0,
						}
					}]
				}
			}

		// Configuration options go here
		});
		
	var body_weight = document.getElementById('crawl_max').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $crawl_max_date_array; ?>,
			datasets: [{
				label: 'Crawl',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $crawl_max_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'time'
						},
						ticks: {
							max: 40,
							min: 0,
							suggestedMax: 40,
							suggestedMin: 0,
						}
					}]
				}
			}

		// Configuration options go here
		});
		
	var body_weight = document.getElementById('plank_max').getContext('2d');
	var chart = new Chart(body_weight, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: <?php echo $plank_max_date_array; ?>,
			datasets: [{
				label: 'Plank',
				backgroundColor: 'rgba(255, 99, 132,0.5)',
				borderColor: 'rgb(255, 99, 132)',
				data: <?php echo $plank_max_array; ?>
			}]
		},
		options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'time'
						},
						ticks: {
							max: 40,
							min: 0,
							suggestedMax: 40,
							suggestedMin: 0,
						}
					}]
				}
			}

		// Configuration options go here
		});
		
		
jQuery(document).ready(function(jQuery){
jQuery("#add_body_weight").click(function(){
  jQuery(".body_weight_feilds").show(700);
  jQuery("#body_weight1").focus();
});
jQuery("#add_body_fat").click(function(){
  jQuery(".body_fat_feilds").show(700);
	jQuery("#body_fat1").focus();

});
jQuery("#add_plank_max").click(function(){
  jQuery(".plank_max_feilds").show(700);
    jQuery("#plank_max1").focus();

});
jQuery("#add_pull_up_max").click(function(){
  jQuery(".pull_up_max_feilds").show(700);
    jQuery("#pull_up_max1").focus();

});
jQuery("#add_push_up_max").click(function(){
  jQuery(".push_up_max_feilds").show(700);
    jQuery("#push_up_max1").focus();

});
jQuery("#add_plank_max").click(function(){
  jQuery(".plank_max_feilds").show(700);
    jQuery("#plank_max1").focus();

});
jQuery("#add_crawl_max").click(function(){
  jQuery(".crawl_max_feilds").show(700);
    jQuery("#crawl_max1").focus();

});
jQuery("#add_body_mass_index").click(function(){
  jQuery(".body_mass_index_feilds").show(700);
    jQuery("#body_mass_index1").focus();

});
});
</script>

