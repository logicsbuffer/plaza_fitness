<?php 
$obj_gyme = new Gym_management();
?>
<script type="text/javascript">
$(document).ready(function() {
	$('#member_form').validationEngine();
	$('#birth_date').datepicker({
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
} );
</script>
     <?php	
   /*      	$member_id=0;
			if(isset($_REQUEST['member_id']))
				$member_id=$_REQUEST['member_id'];
			$edit=0;					
					$edit=1;
					$user_info = get_userdata($member_id); */
				$member_id= get_current_user_id();
				$user_info = get_userdata($member_id);
		?>
		
<div class="panel-body">
	<div class="member_view_row1">
	<div class="col-md-8 col-sm-12 membr_left">
		<div class="col-md-6 col-sm-12 left_side">
		<?php 
		if($user_info->gmgt_user_avatar == "")
			                     	{?>
			                     	<img alt="" src="<?php echo get_option( 'gmgt_system_logo' ); ?>">
			                     	<?php }
			                     	else {
			                     		?>
							        <img style="max-width:100%;" src="<?php if($edit)echo esc_url( $user_info->gmgt_user_avatar ); ?>" />
							        <?php 
			                     	}
		?>
		</div>
		<div class="col-md-6 col-sm-12 right_side">
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-user"></i> 
				<?php _e('Name','gym_mgt'); ?>	
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color">
					<?php echo $user_info->first_name." ".$user_info->middle_name." ".$user_info->last_name;?> 
				</span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-envelope"></i> 
				<?php _e('Email','gym_mgt');?> 	
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color"><?php echo $user_info->user_email;?></span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td"><i class="fa fa-phone"></i> <?php _e('Mobile No','gym_mgt');?> </div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color">
					<span class="txt_color"><?php echo $user_info->mobile;?> </span>
				</span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-calendar"></i> <?php _e('Date Of Birth','gym_mgt');?>	
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color"><?php echo $user_info->birth_date;?></span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-mars"></i> <?php _e('Gender','gym_mgt');?> 
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color"><?php echo $user_info->gender;?></span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-graduation-cap"></i> <?php _e('Class','gym_mgt');?>
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color"><?php echo gmgt_get_class_name($user_info->class_id);?></span>
			</div>
		</div>
		<div class="table_row">
			<div class="col-md-5 col-sm-12 table_td">
				<i class="fa fa-user"></i> <?php _e('User Name','gym_mgt');?>
			</div>
			<div class="col-md-7 col-sm-12 table_td">
				<span class="txt_color"><?php echo $user_info->user_login;?> </span>
			</div>
		</div>
		
		</div>
	</div>
	<div class="col-md-4 col-sm-12 member_right">	
			<span class="report_title">
				<span class="fa-stack cutomcircle">
					<i class="fa fa-align-left fa-stack-1x"></i>
				</span> 
				<span class="shiptitle"><?php _e('More Info','gym_mgt');?></span>		
			</span>
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-user"></i> <?php _e('Staff Member','gym_mgt'); ?>	
				</div>
				<div class="col-md-6 col-sm-12 table_td">
					<span class="txt_color"><?php echo gym_get_display_name($user_info->staff_id);?></span>
				</div>
			</div>
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-heart"></i><?php _e('Interest Area','gym_mgt');?>
				</div>
				<div class="col-md-6 col-sm-12 table_td">
					<span class="txt_color"><?php echo get_the_title($user_info->intrest_area);?></span>
				</div>
			</div>
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-users"></i> <?php _e('Member Ship','gym_mgt');?>
				</div>
				<div class="col-md-6 col-sm-12 table_td">
					<span class="txt_color"><?php echo get_membership_name($user_info->membership_id);?></span>
				</div>
			</div>
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-power-off"></i> <?php _e('Status','gym_mgt');?>
				</div>
				<div class="col-md-6 col-sm-12 table_td">
					<span class="txt_color"><?php echo $user_info->membership_status;?></span>
				</div>
			</div>
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-map-marker"></i> <?php _e('Address','gym_mgt');?>
				</div>
				<div class="col-md-6 col-sm-12 table_td">
					<span class="txt_color"><?php 
			 if($user_info->address != '')
				echo $user_info->address.", <BR>";
			if($user_info->city_name != '')
				echo $user_info->city_name.", "; 
			
			
			?> </span>
				</div>
			</div>
			
	</div>
	</div>
	</div><div class="panel-body">
	
	<div class="clear"></div>
	<div class="col-md-6  col-sm-6  col-xs-12 border">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Weight','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Weight" 
			class="btn btn-danger right"> <?php _e('Add Weight','gym_mgt');?></a>	
		</span>
		
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Waist Report','gym_mgt');?></span>
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Waist" 
			class="btn btn-danger right"> <?php _e('Add Waist','gym_mgt');?></a>		
		</span>
		
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Height Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Height" 
			class="btn btn-danger right"> <?php _e('Add Height','gym_mgt');?></a>	
		</span>
	
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Chest Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Chest" 
			class="btn btn-danger right"> <?php _e('Add Chest','gym_mgt');?></a>	
		</span>
		
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Thigh Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Thigh" 
			class="btn btn-danger right"> <?php _e('Add Thigh','gym_mgt');?></a>	
		</span>
		
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Arms Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Arms" 
			class="btn btn-danger right"> <?php _e('Add Arms','gym_mgt');?></a>	
		</span>
		
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php _e('Fat Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&user_id=<?php echo $_REQUEST['member_id'];?>&result_measurment=Fat" 
			class="btn btn-danger right"> <?php _e('Add Fat','gym_mgt');?></a>	
		</span>
		
	</div>
</div>