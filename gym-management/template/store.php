<?php $curr_user_id=get_current_user_id();
$obj_gym=new Gym_management($curr_user_id);
$obj_product=new Gmgtproduct;
$obj_store=new Gmgtstore;
$obj_class=new Gmgtclassschedule;
$active_tab = isset($_GET['tab'])?$_GET['tab']:'store';

	if(isset($_POST['save_selling']))
	{
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='edit')
		{
				
			$result=$obj_store->gmgt_sell_product($_POST);
			if($result)
			{
				wp_redirect ( home_url().'?dashboard=user&page=store&tab=store&message=2');
			}
			
				
				
		}
		else
		{
			$result=$obj_store->gmgt_sell_product($_POST);
				
				if($result!='no_stock')
				{
					wp_redirect ( home_url().'?dashboard=user&page=store&tab=store&message=1');
				}
				else
				{ 
					wp_redirect ( home_url().'?dashboard=user&page=store&tab=sellproduct&message=4');
				}
			
			}
			
			
		
	}
	
		
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='delete')
			{
				
				$result=$obj_store->delete_selling($_REQUEST['sell_id']);
				if($result)
				{
					wp_redirect ( home_url().'?dashboard=user&page=store&tab=store&message=3');
				}
			}
		if(isset($_REQUEST['message']))
	{
		$message =$_REQUEST['message'];
		if($message == 1)
		{?>
				<div id="message" class="updated below-h2 ">
				<p>
				<?php 
					_e('Record inserted successfully','gym_mgt');
				?></p></div>
				<?php 
			
		}
		elseif($message == 2)
		{?><div id="message" class="updated below-h2 "><p><?php
					_e("Record updated successfully.",'gym_mgt');
					?></p>
					</div>
				<?php 
			
		}
		elseif($message == 3) 
		{?>
		<div id="message" class="updated below-h2"><p>
		<?php 
			_e('Record deleted successfully','gym_mgt');
		?></div></p><?php
				
		}
		elseif($message == 4) 
		{?>
		<div id="message" class="updated below-h2"><p>
		<?php 
			_e('Not Enough Stock of This Product','gym_mgt');
		?></div></p><?php
				
		}
	}
	?>
	

<script type="text/javascript">
$(document).ready(function() {
	$('#sell_date').datepicker({dateFormat : 'yy-mm-dd',
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
	jQuery('#selling_list').DataTable({
		"responsive": true,
		"order": [[ 0, "asc" ]],
		"aoColumns":[
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": false}]
		});
		$('#store_form').validationEngine();
		$(".display-members").select2();
	
} );
</script>

<div class="panel-body panel-white">
 <ul class="nav nav-tabs panel_tabs" role="tablist">
     
	  
	  	<li class="<?php if($active_tab=='store'){?>active<?php }?>">
			<a href="?dashboard=user&page=store&tab=store" class="nav-tab <?php echo $active_tab == 'store' ? 'nav-tab-active' : ''; ?>">
		 <i class="fa fa-align-justify"></i> <?php _e('Sells Record', 'gym_mgt'); ?></a>
         
      </li>
	 
       <li class="<?php if($active_tab=='sellproduct'){?>active<?php }?>">
		  <?php  if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit' && isset($_REQUEST['sell_id']))
			{?>
			<a href="?dashboard=user&page=store&tab=sellproduct&action=edit&sell_id=<?php echo $_REQUEST['sell_id'];?>" class="nav-tab <?php echo $active_tab == 'sellproduct' ? 'nav-tab-active' : ''; ?>">
             <i class="fa fa"></i> <?php _e('Edit  Sold Product', 'gym_mgt'); ?></a>
			 <?php }
			else
			{?>
				<a href="?dashboard=user&page=store&tab=sellproduct" class="nav-tab <?php echo $active_tab == 'sellproduct' ? 'nav-tab-active' : ''; ?>">
			<i class="fa fa-plus-circle"></i> <?php _e('Sell Product', 'gym_mgt'); ?></a>
	  <?php } ?>
	  
	</li>
	  
</ul>
	<div class="tab-content">
	<?php if($active_tab == 'store')
	{ ?>	
    	<div class="panel-body">
        <div class="table-responsive">
       <table id="selling_list" class="display" cellspacing="0" width="100%">
        	 <thead>
            <tr>
			<th><?php  _e( 'Product Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Member Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Product Quentity', 'gym_mgt' ) ;?></th>
               <th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
			<th><?php  _e( 'Product Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Member Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Product Quentity', 'gym_mgt' ) ;?></th>
               <th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
            </tr>
        </tfoot>
 
        <tbody>
         <?php 
		
		
			$storedata=$obj_store->get_all_selling();
		 if(!empty($storedata))
		 {
		 	foreach ($storedata as $retrieved_data){

		 ?>
            <tr><td class="productname"><a href="?dashboard=user&page=store&tab=sellproduct&action=edit&sell_id=<?php echo $retrieved_data->id;?>"><?php $product = $obj_product->get_single_product($retrieved_data->product_id); 
				echo $product->product_name;?></a></td>
			<td class="membername"><?php $userdata=get_userdata($retrieved_data->member_id);
			echo $userdata->display_name;?></td>
				
				<td class="productquentity"><?php echo $retrieved_data->quentity;?></td>
                <td class="action"> <a href="?dashboard=user&page=store&tab=sellproduct&action=edit&sell_id=<?php echo $retrieved_data->id?>" class="btn btn-info"> <?php _e('Edit', 'gym_mgt' ) ;?></a>
                <a href="?dashboard=user&page=store&tab=store&action=delete&sell_id=<?php echo $retrieved_data->id;?>" class="btn btn-danger" 
                onclick="return confirm('<?php _e('Are you sure you want to delete this record?','gym_mgt');?>');">
                <?php _e( 'Delete', 'gym_mgt' ) ;?> </a>
                
                </td>
               
            </tr>
            <?php } 
			
		}?>
     
        </tbody>
        
        </table>

 		</div>
		</div>
		<?php 
	}
	if($active_tab == 'sellproduct')
	 {
        	
        	$sell_id=0;
			if(isset($_REQUEST['sell_id']))
				$sell_id=$_REQUEST['sell_id'];
			$edit=0;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
					
					$edit=1;
					$result = $obj_store->get_single_selling($sell_id);
					
				}?>
		
       <div class="panel-body">
        <form name="store_form" action="" method="post" class="form-horizontal" id="store_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="sell_id" value="<?php echo $sell_id;?>"  />
		
		
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="day"><?php _e('Member','gym_mgt');?><span class="require-field">*</span></label>	
			<div class="col-sm-8">
				<?php if($edit){ $member_id=$result->member_id; }elseif(isset($_POST['member_id'])){$member_id=$_POST['member_id'];}else{$member_id='';}?>
				<select id="member_list" class="display-members" name="member_id">
				<option value=""><?php _e('Select Member','gym_mgt');?></option>
					<?php $get_members = array('role' => 'member');
					$membersdata=get_users($get_members);
					 if(!empty($membersdata))
					 {
						foreach ($membersdata as $member){?>
							<option value="<?php echo $member->ID;?>" <?php selected($member_id,$member->ID);?>><?php echo $member->display_name." - ".$member->member_id; ?> </option>
						<?php }
					 }?>
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="sell_date"><?php _e('Date','gym_mgt');?></label>
			<div class="col-sm-8">
				<input id="sell_date" class="form-control" type="text"  name="sell_date" 
				value="<?php if($edit){ echo $result->sell_date;}elseif(isset($_POST['sell_date'])){ echo $_POST['sell_date'];}else{ echo date("Y-m-d"); }?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="product_id"><?php _e('Product','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<?php if($edit){ $product_id=$result->product_id; }elseif(isset($_POST['product_id'])){$product_id=$_POST['product_id'];}else{$product_id='';}?>
				<select id="product_id" class="form-control validate[required]" name="product_id">
				<option value=""><?php _e('Select Product','gym_mgt');?></option>
			<?php $productdata=$obj_product->get_all_product();
					 if(!empty($productdata))
					 {
						foreach ($productdata as $product){?>
						<option value="<?php echo $product->id;?>" <?php selected($product_id,$product->id);  ?>><?php echo $product->product_name; ?> </option>
			<?php } } ?>
			</select>
			</div>
		</div>
		
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="quentity"><?php _e('Quentity','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="group_name" class="form-control validate[required,custom[number]] text-input" type="text" value="<?php if($edit){ echo $result->quentity;}elseif(isset($_POST['quentity'])) echo $_POST['quentity'];?>" name="quentity" <?php if($edit){?> readonly <?php } ?>>
			</div>
		</div>
		
		
		
		
		<div class="col-sm-offset-2 col-sm-8">
        	
        	<input type="submit" value="<?php if($edit){ _e('Save','gym_mgt'); }else{ _e('Sell Product','gym_mgt');}?>" name="save_selling" class="btn btn-success"/>
        </div>
		
		
		
        </form>
        </div>
        
     <?php 
	 }
	 ?>
		
	
	</div>
</div>
<?php ?>