<?php $curr_user_id=get_current_user_id();
$obj_gym=new Gym_management($curr_user_id);
$obj_reservation=new Gmgtreservation;
$active_tab = isset($_GET['tab'])?$_GET['tab']:'reservationlist';


	if(isset($_POST['save_group']))
	{
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='edit')
		{
				
			$result=$obj_reservation->gmgt_add_reservation($_POST);
			
			if($result['msg']!='reserved')
			{
				wp_redirect ( home_url().'?dashboard=user&page=reservation&tab=reservationlist&message=2');
			}
			else
			{
				if(isset($result['msg'])){
					$_REQUEST['reservation_id']=$result['id'];
					?>
					<div id="message" class="updated below-h2">
						<p><p><?php _e('This Date is Allready Reserved.','gyml_mgt');?></p></p>
						</div>
					<?php }
				 
				
			 }
				
				
		}
		else
		{
				
			
				$result=$obj_reservation->gmgt_add_reservation($_POST);
				
				if($result!="reserved")
				{
					wp_redirect ( home_url().'?dashboard=user&page=reservation&tab=reservationlist&message=1');
				}
				else{?>
					<div id="message" class="updated below-h2">
						<p><p><?php _e('This Date is Allready Reserved.','gyml_mgt');?></p></p>
						</div>
				<?php }		
			
			}
			
			
		
	}
	
		
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='delete')
			{
				
				$result=$obj_reservation->delete_reservation($_REQUEST['reservation_id']);
				if($result)
				{
					wp_redirect ( home_url().'?dashboard=user&page=reservation&tab=reservationlist&message=3');
				}
			}
		if(isset($_REQUEST['message']))
	{
		$message =$_REQUEST['message'];
		if($message == 1)
		{?>
				<div id="message" class="updated below-h2 ">
				<p>
				<?php 
					_e('Record inserted successfully','gym_mgt');
				?></p></div>
				<?php 
			
		}
		elseif($message == 2)
		{?><div id="message" class="updated below-h2 "><p><?php
					_e("Record updated successfully.",'gym_mgt');
					?></p>
					</div>
				<?php 
			
		}
		elseif($message == 3) 
		{?>
		<div id="message" class="updated below-h2"><p>
		<?php 
			_e('Record deleted successfully','gym_mgt');
		?></div></p><?php
				
		}
	}
	?>

<script type="text/javascript">
$(document).ready(function() {
	$('#event_date').datepicker({dateFormat : 'yy-mm-dd',
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
	jQuery('#reservation_list').DataTable({
		"responsive": true,
		"order": [[ 0, "asc" ]],
		"aoColumns":[
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true},	
	                  {"bSortable": true},
		<?php if($obj_gym->role == 'staff_member' || $obj_gym->role == 'accountant'){?>			  
		{"bSortable": false}<?php }?>]
		});
		$('#reservation_form').validationEngine();
} );
</script>

<div class="panel-body panel-white">
 <ul class="nav nav-tabs panel_tabs" role="tablist">
     
	  
	  	<li class="<?php if($active_tab=='reservationlist'){?>active<?php }?>">
			<a href="?dashboard=user&page=reservation&tab=reservationlist" class="tab <?php echo $active_tab == 'reservationlist' ? 'active' : ''; ?>">
             <i class="fa fa-align-justify"></i> <?php _e('Reservation List', 'gym_mgt'); ?></a>
          </a>
      </li>
	  <?php if($obj_gym->role=='staff_member' || $obj_gym->role=='accountant'){?>
       <li class="<?php if($active_tab=='addreservation'){?>active<?php }?>">
		  <?php  if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit' && isset($_REQUEST['reservation_id']))
			{?>
			<a href="?dashboard=user&page=reservation&tab=addreservation&action=edit&reservation_id=<?php echo $_REQUEST['reservation_id'];?>" class="nav-tab <?php echo $active_tab == 'addreservation' ? 'nav-tab-active' : ''; ?>">
             <i class="fa fa"></i> <?php _e('Edit Reservation', 'gym_mgt'); ?></a>
			 <?php }
			else
			{?>
				<a href="?dashboard=user&page=reservation&tab=addreservation" class="tab <?php echo $active_tab == 'addreservation' ? 'active' : ''; ?>">
				<i class="fa fa-plus-circle"></i> <?php _e('Add Reservation', 'gym_mgt'); ?></a>
	  <?php } ?>
	  
	</li>
	  <?php }?>
</ul>
	<div class="tab-content">
	<?php if($active_tab == 'reservationlist')
	{ ?>	
    	 <form name="wcwm_report" action="" method="post">
    
        <div class="panel-body">
        	<div class="table-responsive">
        <table id="reservation_list" class="display" cellspacing="0" width="100%">
        	 <thead>
            <tr>
			<th><?php  _e( 'Event Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Event Date', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Place', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Starting Time', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Ending Time', 'gym_mgt' ) ;?></th>
			 <?php if($obj_gym->role == 'staff_member' || $obj_gym->role == 'accountant')
                   {?>
			<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
				   <?php }?>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
			<th><?php  _e( 'Event Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Event Date', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Place', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Starting Time', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Ending Time', 'gym_mgt' ) ;?></th>
            <?php if($obj_gym->role == 'staff_member' || $obj_gym->role == 'accountant')
                   {?>
			<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
				   <?php }?>
            </tr>
        </tfoot>
 
        <tbody>
         <?php 
		
		
			$reservationdata=$obj_reservation->get_all_reservation();
		 if(!empty($reservationdata))
		 {
		 	foreach ($reservationdata as $retrieved_data){

		 ?>
            <tr>
				<td class="eventname">
				<?php if($obj_gym->role == 'staff_member' || $obj_gym->role == 'accountant')
                   {?>
				<a href="?dashboard=user&page=reservation&tab=addreservation&action=edit&reservation_id=<?php echo $retrieved_data->id;?>"><?php echo $retrieved_data->event_name;?></a>
				   <?php }
				   else
				   {?>
					   <a href="#"><?php echo $retrieved_data->event_name;?></a>
				   <?php }?></td>
				<td class="date"><?php echo $retrieved_data->event_date;?></td>
                <td class="place"><?php echo  get_the_title( $retrieved_data->place_id );?></td>
				
				<td class="starttime"><?php echo $retrieved_data->start_time;?></td>
				<td class="endtime"><?php echo $retrieved_data->end_time;?></td>
               	  <?php if($obj_gym->role == 'staff_member' || $obj_gym->role == 'accountant')
                   {?>
				<td class="action">
				<a href="?dashboard=user&page=reservation&tab=addreservation&action=edit&reservation_id=<?php echo $retrieved_data->id?>" class="btn btn-info"> <?php _e('Edit', 'gym_mgt' ) ;?></a>
                <a href="?dashboard=user&page=reservation&tab=reservationlist&action=delete&reservation_id=<?php echo $retrieved_data->id;?>" class="btn btn-danger" 
                onclick="return confirm('<?php _e('Are you sure you want to delete this record?','gym_mgt');?>');">
                <?php _e( 'Delete', 'gym_mgt' ) ;?> </a>
                </td>
				  <?php }?>
               
            </tr>
            <?php } 
			
		}?>
     
        </tbody>
        
        </table>
        </div>
        </div>
       
</form>
		<?php 
	}
	if($active_tab == 'addreservation')
	 {
        	
        	$reservation_id=0;
			if(isset($_REQUEST['reservation_id']))
				$reservation_id=$_REQUEST['reservation_id'];
			$edit=0;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
					
					$edit=1;
					$result = $obj_reservation->get_single_reservation($reservation_id);
					
				}?>
		
       <div class="panel-body">
        <form name="reservation_form" action="" method="post" class="form-horizontal" id="reservation_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="reservation_id" value="<?php echo $reservation_id;?>"  />
		<div class="form-group">
			<label class="col-sm-2 control-label" for="event_name"><?php _e('Event Name','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="event_name" class="form-control validate[required,custom[onlyLetterSp]] text-input" type="text" value="<?php if($edit){ echo $result->event_name;}elseif(isset($_POST['event_name'])) echo $_POST['event_name'];?>" name="event_name">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="event_date"><?php _e('Event Date','gym_mgt');?></label>
			<div class="col-sm-8">
				<input id="event_date" class="form-control" type="text"  name="event_date" 
				value="<?php if($edit){ echo $result->event_date;}elseif(isset($_POST['event_date'])){ echo $_POST['event_date'];}?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="event_place"><?php _e('Event Place','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			
				<select class="form-control validate[required]" name="event_place" id="event_place">
				<option value=""><?php _e('Select Event Place','gym_mgt');?></option>
				<?php 
				
				if(isset($_REQUEST['event_place']))
					$category =$_REQUEST['event_place'];  
				elseif($edit)
					$category =$result->place_id;
				else 
					$category = "";
				
				$mambership_category=gmgt_get_all_category('event_place');
				if(!empty($mambership_category))
				{
					foreach ($mambership_category as $retrive_data)
					{
						echo '<option value="'.$retrive_data->ID.'" '.selected($category,$retrive_data->ID).'>'.$retrive_data->post_title.'</option>';
					}
				}
				?>
				
				</select>
			</div>
			<div class="col-sm-2"><button id="addremove" model="event_place"><?php _e('Add Or Remove','gym_mgt');?></button></div>
		</div>
		
		
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="starttime"><?php _e('Start Time','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-2">
			<?php 
			if($edit)
			{
				$start_time_data = explode(":", $result->start_time);
				
			}
			?>
				 <select name="start_time" class="form-control validate[required]">
				 <option value=""><?php _e('Start Time','gym_mgt');?></option>
                         <?php 
						 	for($i =0 ; $i <= 12 ; $i++)
							{
							?>
							<option value="<?php echo $i;?>" <?php  if($edit) selected($start_time_data[0],$i);  ?>><?php echo $i;?></option>
							<?php
							}
						 ?>
                         </select>
			</div>
			<div class="col-sm-2">
				 <select name="start_min" class="form-control validate[required]">
                         <?php 
						 	foreach(minute_array() as $key=>$value)
							{?>
							<option value="<?php echo $key;?>" <?php  if($edit) selected($start_time_data[1],$key);  ?>><?php echo $value;?></option>
							<?php
							}
						 ?>
                         </select>
			</div>
			<div class="col-sm-2">
				 <select name="start_ampm" class="form-control validate[required]">
                         	<option value="am" <?php  if($edit) if(isset($start_time_data[2])) selected($start_time_data[2],'am');  ?>><?php _e('am','gym_mgt');?></option>
                            <option value="pm" <?php  if($edit) if(isset($start_time_data[2])) selected($start_time_data[2],'pm');  ?>><?php _e('pm','gym_mgt');?></option>
                         </select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="weekday"><?php _e('End Time','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-2">
			<?php 
			if($edit)
			{
				$end_time_data = explode(":", $result->end_time);
			}
			?>
				 <select name="end_time" class="form-control validate[required]">
				  <option value=""><?php _e('End Time','gym_mgt');?></option>
                         <?php 
						 	for($i =0 ; $i <= 12 ; $i++)
							{
							?>
							<option value="<?php echo $i;?>" <?php  if($edit) selected($end_time_data[0],$i);  ?>><?php echo $i;?></option>
							<?php
							}
						 ?>
                         </select>
			</div>
			<div class="col-sm-2">
				 <select name="end_min" class="form-control validate[required]">
                          <?php 
						 	foreach(minute_array() as $key=>$value)
							{?>
							<option value="<?php echo $key;?>" <?php  if($edit) selected($end_time_data[1],$key);  ?>><?php echo $value;?></option>
							<?php
							} ?>
                         </select>
			</div>
			<div class="col-sm-2">
				  <select name="end_ampm" class="form-control validate[required]">
                         	<option value="am" <?php  if($edit) if(isset($end_time_data[2])) selected($end_time_data[2],'am');  ?> ><?php _e('am','gym_mgt');?></option>
                            <option value="pm" <?php  if($edit) if(isset($end_time_data[2]))selected($end_time_data[2],'pm');  ?>><?php _e('pm','gym_mgt');?></option>
                         </select>
			</div>
		</div>
		
		<div class="col-sm-offset-2 col-sm-8">
        	
        	<input type="submit" value="<?php if($edit){ _e('Save','gym_mgt'); }else{ _e('Save','gym_mgt');}?>" name="save_group" class="btn btn-success"/>
        </div>
		
		
		
        </form>
        </div>
        
     <?php 
	 }
	 ?>
		
	
	</div>
</div>
<?php ?>