<?php ?>
<script type="text/javascript">
$(document).ready(function() {
	jQuery('#staffmember_list').DataTable({
		"responsive": true,
		 "order": [[ 1, "asc" ]],
		 "aoColumns":[
	                  {"bSortable": false},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true}]
		});
} );
</script>
<?php
if ( !in_array( 'member', (array) $user->roles ) ) { ?>
<div class="panel-body panel-white">
 <ul class="nav nav-tabs panel_tabs" role="tablist">
      <li class="active">
          <a href="#staffmemberlist" role="tab" data-toggle="tab">
             <i class="fa fa-align-justify"></i> <?php _e('Staff Member List', 'gym_mgt'); ?></a>
          </a>
      </li>
</ul>
	<div class="tab-content">
    	
		<div class="panel-body">
        <div class="table-responsive">
       <table id="staffmember_list" class="display dataTable " cellspacing="0" width="100%">
        	<thead>
            <tr>
				<th style="width: 50px;height:50px;"><?php  _e( 'Photo', 'gym_mgt' ) ;?></th>
				<th><?php _e( 'Staff Member Name', 'gyml_mgt' ) ;?></th>
				<th><?php _e( 'Role', 'gyml_mgt' ) ;?></th>
				<th> <?php _e( 'Staff Member Email', 'gyml_mgt' ) ;?></th>
				<th> <?php _e( 'Mobile No', 'gyml_mgt' ) ;?></th>
            </tr>
        </thead>
		<tfoot>
            <tr>
				<th><?php  _e( 'Photo', 'gym_mgt' );?></th>
				<th><?php _e( 'Staff Member Name', 'gyml_mgt' ) ;?></th>
				<th><?php _e( 'Role', 'gyml_mgt' ) ;?></th>
				<th> <?php _e( 'Staff Member Email', 'gyml_mgt' ) ;?></th>
				<th> <?php _e( 'Mobile No', 'gyml_mgt' ) ;?></th>
                
            </tr>
        </tfoot>
	<tbody>
         <?php 
		$curr_user_id = get_current_user_id();
		 $get_staff = array('role' => 'Staff_member');
			$staffdata=get_users($get_staff);
		 if(!empty($staffdata))
		 {
		 	foreach ($staffdata as $retrieved_data){
		 ?>
            <tr>
				<td class="user_image">
					<?php $uid=$retrieved_data->ID;
							$userimage=get_user_meta($uid, 'gmgt_user_avatar', true);
						if(empty($userimage))
						{
										echo '<img src='.get_option( 'gmgt_system_logo' ).' height="50px" width="50px" class="img-circle" />';
						}
						else
							echo '<img src='.$userimage.' height="50px" width="50px" class="img-circle"/>';
				?></td>
				<?php if($uid == $curr_user_id){ 
				$profile_link = site_url().'?dashboard=user&page=member&tab=addmember&action=edit&member_id='.$curr_user_id;
				?>
                <td class="name"><a href="<?php echo $profile_link; ?>"><?php echo $retrieved_data->display_name;?></a></td>
				<?php }else{ ?>
                <td class="name"><a href="#"><?php echo $retrieved_data->display_name;?></a></td>
				<?php } ?>
                <td class="department"><?php 
				$postdata=get_post($retrieved_data->role_type);
				if(isset($postdata))
					echo $postdata->post_title;?>
				</td>
				<td class="email"><?php echo $retrieved_data->user_email;?></td>
                <td class="mobile"><?php echo $retrieved_data->mobile;?></td>
            </tr>
            <?php } 
			
		}?>
     
        </tbody>
        </table>
 		</div>
		</div>
	</div>
</div>
<?php }else{
	
	echo '<h3 style="text-align:center;">Sorry, This content not visible for you</h3>';
} ?>