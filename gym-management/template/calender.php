<?php $curr_user_id=get_current_user_id();
$obj_gym=new Gym_management($curr_user_id);
$obj_product=new Gmgtproduct;
$active_tab = isset($_GET['tab'])?$_GET['tab']:'productlist';


	
	if(isset($_POST['save_product']))
	{
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='edit')
		{
				
			$result=$obj_product->gmgt_add_product($_POST);
			if($result)
			{
				wp_redirect ( home_url().'?dashboard=user&page=product&tab=productlist&message=2');
			}
				
				
		}
		else
		{
			$result=$obj_product->gmgt_add_product($_POST);
	
				if($result)
				{
					wp_redirect ( home_url().'?dashboard=user&page=product&tab=productlist&message=1');
				}
			
			}
			
			
		
	}
	
		
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='delete')
			{
				
				$result=$obj_product->delete_product($_REQUEST['product_id']);
				if($result)
				{
					wp_redirect ( home_url().'?dashboard=user&page=product&tab=productlist&message=3');
				}
			}
		if(isset($_REQUEST['message']))
	{
		$message =$_REQUEST['message'];
		if($message == 1)
		{?>
				<div id="message" class="updated below-h2 ">
				<p>
				<?php 
					_e('Record inserted successfully','gym_mgt');
				?></p></div>
				<?php 
			
		}
		elseif($message == 2)
		{?><div id="message" class="updated below-h2 "><p><?php
					_e("Record updated successfully.",'gym_mgt');
					?></p>
					</div>
				<?php 
			
		}
		elseif($message == 3) 
		{?>
		<div id="message" class="updated below-h2"><p>
		<?php 
			_e('Record deleted successfully','gym_mgt');
		?></div></p><?php
				
		}
	}
	?>

<script type="text/javascript">
$(document).ready(function() {
	jQuery('#product_list').DataTable({
		responsive: true
		});
		$('#product_form').validationEngine();
} );
</script>
<?php
$obj_reservation = new Gmgtreservation;
$reservationdata = $obj_reservation->get_all_reservation();
$cal_array = array();
if(!empty($reservationdata))
{
	foreach ($reservationdata as $retrieved_data){
		$cal_array [] = array (
				'title' => $retrieved_data->event_name,
				'start' => $retrieved_data->event_date,
				'end' => $retrieved_data->event_date,


		);
	}
}
$birthday_boys=get_users(array('role'=>'member'));
$boys_list="";
if (! empty ( $birthday_boys )) {
		foreach ( $birthday_boys as $boys ) {
			 //$boys_list.=$boys->display_name." ";
			$cal_array [] = array (
					'title' => __($boys->display_name.' Birthday','gym_mgt'),
					'start' =>mysql2date('Y-m-d', $boys->birth_date) ,
					'end' => mysql2date('Y-m-d', $boys->birth_date),
					'backgroundColor' => '#F25656'
			);	
			
			
		}
		
	}
	
if (! empty ( $obj_gym->notice )) {
	foreach ( $obj_gym->notice as $notice ) {
			 $notice_start_date=get_post_meta($notice->ID,'gmgt_start_date',true);
			 $notice_end_date=get_post_meta($notice->ID,'gmgt_end_date',true);
			$i=1;
			
			$cal_array[] = array (
					'title' => $notice->post_title,
					'start' => mysql2date('Y-m-d', $notice_start_date ),
					'end' => date('Y-m-d',strtotime($notice_end_date.' +'.$i.' days')),
					'color' => '#12AFCB'
			);	
			
		}
	}	

?>

<script>
jQuery(document).ready(function() {
	
	jQuery('#calendar').fullCalendar({
		header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				defaultView: 'month',
				editable: false,
			eventLimit: true, // allow "more" link when too many events
			events: <?php echo json_encode($cal_array);?>
			
		});

		 
	});
</script>

<div class="panel-body panel-white">
<div class="row">
	<div class="col-sm-offset-2 col-md-8 col-sm-8 col-xs-12">
		<div id="calendar">
		</div>
	</div>
	<div class="col-sm-2">&nbsp;
	</div>
</div>

<?php 

//$my_postid = 18474;//This is page id or post id
$content_post = get_page_by_title( 'Calender' );
$content = $content_post->post_content;
echo do_shortcode($content);

?>

</div>
<?php ?>