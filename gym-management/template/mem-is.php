
<div class="panel-body panel-white">

 <ul class="nav nav-tabs panel_tabs" role="tablist">

      <li class="<?php if($active_tab == 'memberlist') echo "active";?>">

          <a href="?dashboard=user&page=member&tab=memberlist">

             <i class="fa fa-align-justify"></i> <?php _e('Member List', 'gym_mgt'); ?></a>

          </a>

      </li>

	<?php if($obj_gym->role == 'staff_member'){?>

	<?php 

		if(isset($_REQUEST['action']) && $_REQUEST['action'] =='view')

	{?>

	 <li class="<?php if($active_tab == 'viewmember') echo "active";?>">

      	<a href="?dashboard=user&page=member&tab=addmember">

        <i class="fa fa-plus-circle"></i> <?php		

			_e('Member Biometrics', 'gym_mgt'); 		

		?></a> 

      </li>

	<?php }else{?>

     <!-- <li class="<?php if($active_tab == 'addmember') echo "active";?>">

      	<a href="?dashboard=user&page=member&tab=addmember">

        <i class="fa fa-plus-circle"></i> <?php

		if(isset($_REQUEST['action']) && $_REQUEST['action'] =='edit')

			_e('Edit Member', 'gym_mgt'); 

		else

			_e('Add Member', 'gym_mgt'); 

		?></a> 

      </li>-->

	  

		<?php } 

	  }}?>

</ul>

<div class="tab-content">

	<?php if($active_tab == 'memberlist'){?>

    	<div class="tab-pane <?php if($active_tab == 'memberlist') echo "fade active in";?>" >

		<div class="panel-body">

        <div class="table-responsive">

        <table id="members_list" class="display" cellspacing="0" width="100%">

        	 <thead>

            <tr>

			<th style="width: 50px;height:50px;"><?php  _e( 'Photo', 'gyml_mgt' ) ;?></th>

              <th><?php _e( 'Member Name', 'gyml_mgt' ) ;?></th>

              <th><?php _e( 'Member Id', 'gyml_mgt' ) ;?></th>

			   <th><?php _e( 'Joining Date', 'gyml_mgt' ) ;?></th>

			   <th><?php _e( 'Expire Date', 'gyml_mgt' ) ;?></th>

				<th style="width: 50px;"><?php _e( 'Membership Status', 'gyml_mgt' ) ;?></th>

			<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>

			

            </tr>

        </thead>

 

        <tfoot>

            <tr>

			<th><?php  _e( 'Photo', 'gyml_mgt' ) ;?></th>

              <th><?php _e( 'Member Name', 'gyml_mgt' ) ;?></th>

			  <th><?php _e( 'Member Id', 'gyml_mgt' ) ;?></th>

			   <th><?php _e( 'Joining Date', 'gyml_mgt' ) ;?></th>

			   <th><?php _e( 'Expire Date', 'gyml_mgt' ) ;?></th>

			  <th><?php _e( 'Membership Status', 'gyml_mgt' ) ;?></th>

				<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>

			

            </tr>

           

        </tfoot>

	<tbody>

         <?php 

		

		 $get_members = array('role' => 'member');

		$membersdata=get_users($get_members);

		 if(!empty($membersdata))

		 {

		 	foreach ($membersdata as $retrieved_data){

				if(get_option('gym_enable_memberlist_for_member')=='no' && $obj_gym->role == 'member')

				{

					if($curr_user_id==$retrieved_data->ID)

					{ ?>

						<tr>

				<td class="user_image"><?php $uid=$retrieved_data->ID;

							$userimage=get_user_meta($uid, 'gmgt_user_avatar', true);

						if(empty($userimage))

						{

										echo '<img src='.get_option( 'gmgt_system_logo' ).' height="50px" width="50px" class="img-circle" />';

						}

						else

							echo '<img src='.$userimage.' height="50px" width="50px" class="img-circle"/>';

				?></td>

				<td class="name">

				<?php if($obj_gym->role == 'staff_member'){?>

				

				

                <a href="?dashboard=user&page=member&tab=addmember&action=edit&member_id=<?php echo $retrieved_data->ID;?>"><?php echo $retrieved_data->display_name;?></a>

				<?php }

				else

				{?>

					<a href="#"><?php echo $retrieved_data->display_name;?></a>

				<?php }?></td>

				<td class="memberid"><?php echo $retrieved_data->member_id;?></td>

                <td class="joining date"><?php echo $retrieved_data->begin_date;?></td>

                <td class="joining date"><?php echo gmgt_check_membership($retrieved_data->ID);?></td>

               <td class="status"><?php echo $retrieved_data->membership_status;?></td>

				<td class="action">

				<?php 

				if($obj_gym->role == 'staff_member' || ($obj_gym->role == 'member' && $retrieved_data->ID==$curr_user_id)){?>

				<a class="btn btn-success" href="?dashboard=user&page=member&tab=viewmember&action=view&member_id=<?php echo $retrieved_data->ID;?>"><?php _e('VIEW BIOMETRICS','gym_mgt');?></a>

				<a href="?dashboard=user&page=member&tab=add_attendence&member_id=<?php echo $retrieved_data->ID;?>&attendance=1" class="hide btn btn-default"  idtest="<?php echo $retrieved_data->ID; ?>"><i class="fa fa-eye"></i> <?php _e('View Attendance','gym_mgt');?> </a>

				<?php }?>

				</td>

            </tr>

				<?php }

				}

				else

				{?>

            <tr>

				<td class="user_image"><?php $uid=$retrieved_data->ID;

							$userimage=get_user_meta($uid, 'gmgt_user_avatar', true);

						if(empty($userimage))

						{

										echo '<img src='.get_option( 'gmgt_system_logo' ).' height="50px" width="50px" class="img-circle" />';

						}

						else

							echo '<img src='.$userimage.' height="50px" width="50px" class="img-circle"/>';

				?></td>

				<td class="name">

				<?php if($obj_gym->role == 'staff_member'){?>

				

				

                <a href="?dashboard=user&page=member&tab=addmember&action=edit&member_id=<?php echo $retrieved_data->ID;?>"><?php echo $retrieved_data->display_name;?></a>

				<?php }

				else

				{?>

					<a href="#"><?php echo $retrieved_data->display_name;?></a>

				<?php }?></td>

				<td class="memberid"><?php echo $retrieved_data->member_id;?></td>

                <td class="joining date"><?php echo $retrieved_data->begin_date;?></td>

                <td class="joining date"><?php echo gmgt_check_membership($retrieved_data->ID);?></td>

                <!--<td class="class"><?php $classdata=$obj_class->get_single_class($retrieved_data->class_id);

				echo $classdata->class_name;?></td>-->

				<td class="status"><?php echo $retrieved_data->membership_status;?></td>

				<!--<td class="email"><?php echo $retrieved_data->user_email;?></td>

                <td class="mobile"><?php echo $retrieved_data->mobile;?></td>-->

				<td class="action">

				

				

				<?php if($obj_gym->role == 'staff_member'){?>

				

               	<!-- <a href="?dashboard=user&page=member&tab=addmember&action=edit&member_id=<?php echo $retrieved_data->ID?>" class="btn btn-info"> <?php _e('Edit', 'gym_mgt' ) ;?></a>

                <a href="?dashboard=user&page=member&tab=memberlist&action=delete&member_id=<?php echo $retrieved_data->ID;?>" class="btn btn-danger" 

                onclick="return confirm('<?php _e('Are you sure you want to delete this record?','gym_mgt');?>');">

                <?php _e( 'Delete', 'gym_mgt' ) ;?> </a>-->

                

				<?php } 

				if($obj_gym->role == 'staff_member' || ($obj_gym->role == 'member' && $retrieved_data->ID==$curr_user_id)){?>

				<a class="btn btn-success" href="?dashboard=user&page=member&tab=viewmember&action=view&member_id=<?php echo $retrieved_data->ID;?>"><?php _e('View Biometrics','gym_mgt');?></a>

				<a href="?dashboard=user&page=member&tab=add_attendence&member_id=<?php echo $retrieved_data->ID;?>&attendance=1" class="hide btn btn-default"  idtest="<?php echo $retrieved_data->ID; ?>"><i class="fa fa-eye"></i> <?php _e('View Attendance','gym_mgt');?> </a>

				<?php }?>

				</td>

            </tr>

				<?php  }

			} 

			

		}?>

     

        </tbody>

        </table>

 		</div>

		</div>

		</div>

		

		<!--Member Step one information-->

		<?php }?>