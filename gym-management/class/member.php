<?php 
//$user = new WP_User($user_id);
	  
class Gmgtmember
{	

	
	public function gmgt_add_user($data)
	{
		
		global $wpdb;
		$table_members = $wpdb->prefix. 'usermeta';
		$table_gmgt_groupmember = $wpdb->prefix.'gmgt_groupmember';
		//-------usersmeta table data--------------
		if(isset($data['middle_name']))
		$usermetadata['middle_name']=$data['middle_name'];
		if(isset($data['gender']))
		$usermetadata['gender']=$data['gender'];
		if(isset($data['birth_date']))
		$usermetadata['birth_date']=$data['birth_date'];
		if(isset($data['address']))
		$usermetadata['address']=$data['address'];
		
		if(isset($data['city_name']))
		$usermetadata['city_name']=$data['city_name'];
		
		if(isset($data['state_name']))
		$usermetadata['state_name']=$data['state_name'];
		
		if(isset($data['zip_code']))
		$usermetadata['zip_code']=$data['zip_code'];
		
		if(isset($data['mobile']))
		$usermetadata['mobile']=$data['mobile'];
		if(isset($data['phone']))
		$usermetadata['phone']=$data['phone'];
		if(isset($data['gmgt_user_avatar']))
		$usermetadata['gmgt_user_avatar']=$data['gmgt_user_avatar'];
		if($data['role']=='staff_member')
		{
			if(isset($data['role_type']))
			$usermetadata['role_type']=$data['role_type'];
			if(isset($data['specialization']))
			$usermetadata['specialization']=json_encode($data['specialization']);
		}
		/*if($data['role']=='accountant')
		{
			if(isset($data['role_type']))
			$usermetadata['role_type']=$data['role_type'];
			
		}*/
		if($data['role']=='member')
		{
			if(isset($data['member_id']))
			$usermetadata['member_id']=$data['member_id'];
			
			//if(isset($data['group_id']))
			//$usermetadata['group_id']=$data['group_id'];
			
			
			if(isset($data['class_id']))
			$usermetadata['class_id']=$data['class_id'];
			if(isset($data['class_id']))
			$usermetadata['class_id']=$data['class_id'];
			if(isset($data['height']))
			$usermetadata['height']=$data['height'];
			if(isset($data['weight']))
				$usermetadata['weight']=$data['weight'];
			if(isset($data['chest']))
				$usermetadata['chest']=$data['chest'];
			if(isset($data['waist']))
				$usermetadata['waist']=$data['waist'];
			if(isset($data['thigh']))
				$usermetadata['thigh']=$data['thigh'];
			if(isset($data['arms']))
				$usermetadata['arms']=$data['arms'];
			if(isset($data['fat']))
				$usermetadata['fat']=$data['fat'];
			
			if(isset($data['staff_id']))
				$usermetadata['staff_id']=$data['staff_id'];
			if(isset($data['intrest_area']))
				$usermetadata['intrest_area']=$data['intrest_area'];
			if(isset($data['source']))
				$usermetadata['source']=$data['source'];
			if(isset($data['reference_id']))
				$usermetadata['reference_id']=$data['reference_id'];
			if(isset($data['inqiury_date']))
				$usermetadata['inqiury_date']=$data['inqiury_date'];
			if(isset($data['triel_date']))
				$usermetadata['triel_date']=$data['triel_date'];
			if(isset($data['membership_id']))
				$usermetadata['membership_id']=$data['membership_id'];
			if(isset($data['membership_status']))
				$usermetadata['membership_status']=$data['membership_status'];
			if(isset($data['auto_renew']))
				$usermetadata['auto_renew']=$data['auto_renew'];
			if(isset($data['begin_date']))
				$usermetadata['begin_date']=$data['begin_date'];
			if(isset($data['end_date']))
				$usermetadata['end_date']=$data['end_date'];
				
			if(isset($data['first_payment_date']))
				$usermetadata['first_payment_date']=$data['first_payment_date'];
			if(isset($data['member_convert']))
				$roledata['role']=$data['member_convert'];
		}
		
		if(isset($data['username']))
		$userdata['user_login']=$data['username'];
		if(isset($data['email']))
		$userdata['user_email']=$data['email'];
	
		$userdata['user_nicename']=NULL;
		$userdata['user_url']=NULL;
		if(isset($data['first_name']))
		$userdata['display_name']=$data['first_name']." ".$data['last_name'];
		
		
		
		if($data['password'] != "")
				$userdata['user_pass']=$data['password'];
		if($data['action']=='edit')
		{
			
			$userdata['ID']=$data['user_id'];
			$user_id = wp_update_user($userdata);
			if(!empty($roledata)){
				$u = new WP_User($user_id);
				$u->remove_role( 'member' );
				$u->add_role( 'staff_member' );
				
			}
			$returnans=update_user_meta( $user_id, 'first_name', $data['first_name'] );
			$returnans=update_user_meta( $user_id, 'last_name', $data['last_name'] );
				
				foreach($usermetadata as $key=>$val){
				$returnans=update_user_meta( $user_id, $key,$val );
				}
				if(isset($data['group_id']))
					if(!empty($data['group_id']))
					{
						if($this->member_exist_ingrouptable($user_id))
							$this->delete_member_from_grouptable($user_id);
						foreach($data['group_id'] as $id)
						{
								
							$group_data['group_id']=$id;
							$group_data['member_id']=$user_id;
							$group_data['created_date']=date("Y-m-d");
							$group_data['created_by']=get_current_user_id();
							
							$wpdb->insert( $table_gmgt_groupmember, $group_data );
						}
					}
				
				return $user_id;
		}
		else
		{
			$user_id = wp_insert_user( $userdata );
			
			$user = new WP_User($user_id);
			$user->set_role($data['role']);
			if($data['role']=='member')
				$usermetadata['membership_status']="Continue";
			foreach($usermetadata as $key=>$val){
				$returnans=add_user_meta( $user_id, $key,$val, true );
			}
			if(isset($data['first_name']))
			$returnans=update_user_meta( $user_id, 'first_name', $data['first_name'] );
			if(isset($data['last_name']))
			$returnans=update_user_meta( $user_id, 'last_name', $data['last_name'] );
			
			if(isset($data['group_id']))
				if(!empty($data['group_id']))
				{
			
					foreach($data['group_id'] as $id)
					{
							
						$group_data['group_id']=$id;
						$group_data['member_id']=$user_id;						
						$group_data['created_date']=date("Y-m-d");
						$group_data['created_by']=get_current_user_id();
						$wpdb->insert( $table_gmgt_groupmember, $group_data );
					}
				}
				if($data['role']=='member')
				{
					$membership_status = 'continue';
					$payment_data = array();
				$payment_data['member_id'] = $user_id;
				$payment_data['membership_id'] = $data['membership_id'];
				$payment_data['membership_amount'] = get_membership_price($data['membership_id']);
				$payment_data['start_date'] = $data['begin_date'];
				$payment_data['end_date'] = $data['end_date'];
				$payment_data['membership_status'] = $membership_status;
				$payment_data['payment_status'] = 0;
				$payment_data['created_date'] = date("Y-m-d");
				$payment_data['created_by'] = get_current_user_id();
				
				$plan_id = $this->add_membership_payment_detail($payment_data);
				//if(isset($plan_id))
			//$returnans=update_user_meta( $user_id, 'gym_membership_payemnt_id', $plan_id );
				}
			return $user_id;
		}
	
	}
	public function add_membership_payment_detail($data)
	{
		global $wpdb;
		$table_gmgt_membership_payment = $wpdb->prefix. 'gmgt_membership_payment';
	
		$result = $wpdb->insert($table_gmgt_membership_payment,$data);
		 $lastid = $wpdb->insert_id;
		return $lastid;
	
	}
	/*function add_prospect_info($data)
	{
		global $wpdb;
		$table_members = $wpdb->prefix. 'usermeta';
		if(isset($data['staff_id']))
		$usermetadata['staff_id']=$data['staff_id'];
		if(isset($data['intrest_area']))
		$usermetadata['intrest_area']=$data['intrest_area'];
		if(isset($data['source']))
			$usermetadata['source']=$data['source'];
		if(isset($data['reference_id']))
			$usermetadata['reference_id']=$data['reference_id'];
		if(isset($data['inqiury_date']))
			$usermetadata['inqiury_date']=$data['inqiury_date'];
		if(isset($data['triel_date']))
			$usermetadata['triel_date']=$data['triel_date'];
		if(isset($data['membership_id']))
			$usermetadata['membership_id']=$data['membership_id'];
		if(isset($data['membership_status']))
			$usermetadata['membership_status']=$data['membership_status'];
		if(isset($data['auto_renew']))
			$usermetadata['auto_renew']=$data['auto_renew'];
		if(isset($data['begin_date']))
			$usermetadata['begin_date']=$data['begin_date'];
		
		if(isset($data['first_payment_date']))
			$usermetadata['first_payment_date']=$data['first_payment_date'];
	
		if(isset($data['member_convert']))
				$roledata['role']=$data['member_convert'];
		if($data['action']=='edit')
		{
			if(isset($data['user_id']))
				$user_id = $data['user_id'];
			if(!empty($roledata)){
				$u = new WP_User($user_id);
				$u->remove_role( 'member' );
				$u->add_role( 'staff_member' );
				
			}	
				foreach($usermetadata as $key=>$val){
				$returnans=update_user_meta( $user_id, $key,$val );
				}
				return $user_id;
		}
		else
		{
			if(isset($data['user_id']))
				$user_id = $data['user_id'];
			$usermetadata['membership_status']="Continue";
			foreach($usermetadata as $key=>$val){
				$returnans=add_user_meta( $user_id, $key,$val, true );
			}
			return $user_id;
		}
	
		
	}*/
	public function get_all_groups()
	{
		global $wpdb;
		$table_members = $wpdb->prefix. 'gmgt_groups';
	
		$result = $wpdb->get_results("SELECT * FROM $table_members");
		return $result;
	
	}
	public function get_single_group($id)
	{
		global $wpdb;
		$table_members = $wpdb->prefix. 'gmgt_groups';
		$result = $wpdb->get_row("SELECT * FROM $table_members where id=".$id);
		return $result;
	}
	public function delete_usedata($record_id)
	{
		global $wpdb;
		
		$table_name = $wpdb->prefix . 'usermeta';
		$result=$wpdb->query($wpdb->prepare("DELETE FROM $table_name WHERE user_id= %d",$record_id));
		$retuenval=wp_delete_user( $record_id );
		return $retuenval;
	}
	public function member_exist_ingrouptable($member_id)
	{
		global $wpdb;
		$table_gmgt_groupmember = $wpdb->prefix. 'gmgt_groupmember';
		$result = $wpdb->get_row("SELECT * FROM $table_gmgt_groupmember where member_id=".$member_id);
		if(!empty($result))
			return true;
		return false;
	}
	public function delete_member_from_grouptable($member_id)
	{
		global $wpdb;
		$table_gmgt_groupmember = $wpdb->prefix. 'gmgt_groupmember';
		$result=$wpdb->query($wpdb->prepare("DELETE FROM $table_gmgt_groupmember WHERE member_id= %d",$member_id));
	}
	
	public function get_all_joingroup($member_id)
	{
		global $wpdb;
		$table_gmgt_groupmember = $wpdb->prefix. 'gmgt_groupmember';
		$result = $wpdb->get_results("SELECT group_id FROM $table_gmgt_groupmember where member_id=".$member_id,ARRAY_A);
		return $result;
	}
	
	public function convert_grouparray($join_group)
	{
		$groups = array();
		foreach($join_group as $group)
			$groups[] = $group['group_id'];
		return $groups;
	}
	
	
}
?>