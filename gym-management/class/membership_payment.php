<?php 
//$user = new WP_User($user_id);
	  
class Gmgt_membership_payment
{	
	public function add_membership_payment($data)
	{	
		global $wpdb;
		$table_gmgt_membership_payment=$wpdb->prefix.'gmgt_membership_payment';
		$payment_data['member_id']=$data['member_id'];
		$payment_data['membership_id']=$data['membership_id'];
		$payment_data['membership_amount']=$data['membership_amount'];		
		$payment_data['start_date']=$data['start_date'];		
		$payment_data['end_date']=$data['end_date'];
		$payment_data['created_date']=date('Y-m-d');
		$payment_data['payment_status']='0';
		$payment_data['created_by']=get_current_user_id();
		
		if($data['action']=='edit')
		{
			$income_dataid['invoice_id']=$data['income_id'];
			$result=$wpdb->update( $table_gmgt_membership_payment, $payment_data ,$income_dataid);
			return $result;
		}
		else
		{
			$result=$wpdb->insert( $table_gmgt_membership_payment,$payment_data);
			return $result;
		}
	}
	public function add_feespayment_history($data)
	{
		global $wpdb;
		$table_gmgt_membership_payment_history = $wpdb->prefix. 'gmgt_membership_payment_history';
		//-------usersmeta table data--------------
		$feedata['mp_id']=$data['mp_id'];
		$feedata['amount']=$data['amount'];
		$feedata['payment_method']=$data['payment_method'];	
		if(isset($data['trasaction_id']))
		{
			$feedata['trasaction_id']=$data['trasaction_id'] ;
		}
		$feedata['paid_by_date']=date("Y-m-d");
		$feedata['created_by']=get_current_user_id();
		$paid_amount = $this->get_paid_amount_by_feepayid($feedata['mp_id']);
		
		$uddate_data['paid_amount'] = $paid_amount + $feedata['amount'];
		$uddate_data['mp_id'] = $data['mp_id'];
		$this->update_paid_fees_amount($uddate_data);
		$result=$wpdb->insert( $table_gmgt_membership_payment_history, $feedata );
		return $result;
		
	}
	public function get_paid_amount_by_feepayid($mp_id)
	{
		global $wpdb;
		$table_gmgt_membership_payment = $wpdb->prefix. 'gmgt_membership_payment';
		//echo "SELECT paid_amount FROM $table_gmgt_membership_payment where mp_id = $mp_id";
		$result = $wpdb->get_row("SELECT paid_amount FROM $table_gmgt_membership_payment where mp_id = $mp_id");
		return $result->paid_amount;
	}
	public function update_paid_fees_amount($data)
	{
		global $wpdb;
		$table_gmgt_membership_payment = $wpdb->prefix. 'gmgt_membership_payment';
		$feedata['paid_amount'] = $data['paid_amount'];
		$fees_id['mp_id']=$data['mp_id'];
			$result=$wpdb->update( $table_gmgt_membership_payment, $feedata ,$fees_id);
	}
	public function get_all_membership_payment()
	{
		global $wpdb;
		$table_gmgt_membership_payment = $wpdb->prefix. 'gmgt_membership_payment';
	
		$result = $wpdb->get_results("SELECT * FROM $table_gmgt_membership_payment");
		return $result;
	
	}
	public function get_single_membership_payment($mp_id)
	{
		global $wpdb;
		$table_gmgt_membership_payment = $wpdb->prefix. 'gmgt_membership_payment';
		$result = $wpdb->get_row("SELECT * FROM $table_gmgt_membership_payment where mp_id=".$mp_id);
		return $result;
	}
	
	public function delete_payment($id)
	{
		global $wpdb;
		$table_gmgt_membership_payment = $wpdb->prefix. 'gmgt_membership_payment';
		$result = $wpdb->query("DELETE FROM $table_gmgt_membership_payment where mp_id= ".$id);
		return $result;
	}
	
	
	
	
}
?>