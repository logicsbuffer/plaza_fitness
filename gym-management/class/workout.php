<?php 
//$user = new WP_User($user_id);
	  
class Gmgtworkout
{	
	public function gmgt_add_workout($data)
	{
		
		$obj_gym = new Gym_management(get_current_user_id());
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_daily_workouts';
		
		
		$workoutdata['record_date']=$data['record_date'];
		
		$workoutdata['note']=$data['note'];
		$workoutdata['created_date']=date("Y-m-d");
		$workoutdata['created_by']=get_current_user_id();
		
		if($obj_gym->role=='administrator' || $obj_gym->role=='staff_member')
		{
			$workoutdata['member_id']=$data['member_id'];
		}
		if($obj_gym->role=='member')
		{
			$workoutdata['member_id']=get_current_user_id();
		}
		if($data['action']=='edit')
		{
			$workoutid['id']=$data['daily_workout_id'];	
			$result=$wpdb->update( $table_workout, $workoutdata ,$workoutid);
			return $result;
		}
		else
		{
			$result=$wpdb->insert( $table_workout, $workoutdata );
			$wpdb->insert_id;
			$result=$this->add_user_workouts($wpdb->insert_id,$data);
			
			return $result;
		}
	
	}
	
	public function get_all_workout()
	{
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_daily_workouts';
	
		$result = $wpdb->get_results("SELECT * FROM $table_workout");
		return $result;
	
	}
	public function get_single_workout($id)
	{
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_daily_workouts';
		$result = $wpdb->get_row("SELECT * FROM $table_workout where id=".$id);
		return $result;
	}
	public function get_member_workout($role,$id)
	{
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_daily_workouts';
		if($role=='member')
			$result = $wpdb->get_results("SELECT * FROM $table_workout where member_id=".$id);
		elseif($role=='staff_member')
			$result = $wpdb->get_results("SELECT * FROM $table_workout where assigned_by=".$id);
		else
			$result = $wpdb->get_results("SELECT * FROM $table_workout");
		return $result;
	}
	public function delete_workout($id)
	{
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_daily_workouts';
		$result = $wpdb->query("DELETE FROM $table_workout where id= ".$id);
		return $result;
	}
	public function add_user_workouts($id,$data)
	{
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_user_workouts';
		foreach($data['workouts_array'] as $val){
			$user_workoutdata['user_workout_id']=$id;
			$user_workoutdata['workout_name']=$data['workout_name_'.$val];
			$user_workoutdata['sets']=$data['sets_'.$val];
			$user_workoutdata['reps']=$data['reps_'.$val];
			$user_workoutdata['kg']=$data['kg_'.$val];
			$user_workoutdata['rest_time']=$data['rest_'.$val];
			$result=$wpdb->insert( $table_workout, $user_workoutdata );
			
		}
		return $result;
		
	}
	public function gmgt_add_measurement($data)
	{
		global $wpdb;
		$table_gmgt_measurment = $wpdb->prefix. 'gmgt_measurment';
	
		$workoutdata['user_id']=$data['user_id'];
	
		$workoutdata['result_measurment']=$data['result_measurment'];
		$workoutdata['result']=$data['result'];
		$workoutdata['result_date']=$data['result_date'];
		$workoutdata['created_date']=date("Y-m-d");
		$workoutdata['created_by']=get_current_user_id();
		if($data['action']=='edit')
		{
			$workoutid['measurment_id']=$data['measurment_id'];
			$result=$wpdb->update( $table_gmgt_measurment, $workoutdata ,$workoutid);
			return $result;
		}
		else
		{
			$result=$wpdb->insert( $table_gmgt_measurment, $workoutdata );
			
			return $wpdb->insert_id;
				
		}
	}
	public function get_all_measurement()
	{
		global $wpdb;
		$table_gmgt_measurment = $wpdb->prefix. 'gmgt_measurment';
	
		$result = $wpdb->get_results("SELECT * FROM $table_gmgt_measurment");
		return $result;
	
	}
	public function get_all_measurement_by_userid($user_id)
	{
		global $wpdb;
		$table_gmgt_measurment = $wpdb->prefix. 'gmgt_measurment';
	
		$result = $wpdb->get_results("SELECT * FROM $table_gmgt_measurment where user_id = ".$user_id." ORDER BY  result_date DESC");
		return $result;
	
	}
	public function get_measurement_deleteby_id($measurement)
	{
		global $wpdb;
		$table_gmgt_measurment = $wpdb->prefix. 'gmgt_measurment';
		$result = $wpdb->query("DELETE FROM $table_gmgt_measurment where measurment_id= ".$measurement);
		return $result;
	}
	public function get_single_measurement($measurment_id)
	{
		global $wpdb;
		$table_gmgt_measurment = $wpdb->prefix. 'gmgt_measurment';
		
		$result = $wpdb->get_row("SELECT * FROM $table_gmgt_measurment where measurment_id = $measurment_id");
		return $result;
	}
	public function get_member_today_workouts($id,$date)
	{
		global $wpdb;
		$table_daily_workouts = $wpdb->prefix. 'gmgt_daily_workouts';
		$table_user_workouts = $wpdb->prefix. 'gmgt_user_workouts';
		
		$today_data = $wpdb->get_row("SELECT * FROM $table_daily_workouts where record_date = '$date' AND member_id=$id");
		if(!empty($today_data))
			$result = $wpdb->get_results("SELECT * FROM $table_user_workouts where user_workout_id=".$today_data->id);
		if(!empty($result))
			return $result;
	
	}
	
	
}
?>