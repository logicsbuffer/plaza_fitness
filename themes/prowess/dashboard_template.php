<?php
/*
Template Name: Dashboard Template
*/
?>
<?php
$qodef_sidebar_layout = prowess_select_sidebar_layout();

get_header();
prowess_select_get_title();
get_template_part( 'slider' );
do_action('prowess_select_before_main_content');
$user = wp_get_current_user ();
?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
 
<?php
/* 
<div class="container-fluid mainpage">
  <div class="navbar">
	
	<div class="col-md-8 col-sm-8 col-xs-6">
		<h3 class="logo-image"><img src="<?php echo get_option( 'gmgt_system_logo' ) ?>" class="img-circle head_logo" width="40" height="40" />
		<span><?php echo get_option( 'gmgt_system_name' );?> </span>
		</h3></div>
		
		<ul class="nav navbar-right col-md-4 col-sm-4 col-xs-6">
				
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<li class="dropdown"><a data-toggle="dropdown"
					class="dropdown-toggle" href="javascript:;">
						<?php
						$userimage = get_user_meta( $user->ID,'gmgt_user_avatar',true );
						if (empty ( $userimage )){
							echo '<img src='.get_option( 'gmgt_system_logo' ).' height="40px" width="40px" class="img-circle" />';
						}
						else	
							echo '<img src=' . $userimage . ' height="40px" width="40px" class="img-circle"/>';
						?>
							<span>	<?php echo $user->display_name;?> </span> <b class="caret"></b>
				</a>
					<ul class="dropdown-menu extended logout">
						<li><a href="?dashboard=user&page=account"><i class="fa fa-user"></i>
								<?php _e('My Profile','gym_mgt');?></a></li>
						<li><a href="<?php echo wp_logout_url(home_url()); ?>"><i
								class="fa fa-sign-out m-r-xs"></i><?php _e('Log Out','gym_mgt');?> </a></li>
					</ul></li>
				<!-- END USER LOGIN DROPDOWN -->
			</ul>
	
	</div>
</div> */

$menu = gmgt_menu();
?>
<div class="container-fluid">
	<div class="row container_row">
		<div class="col-sm-2 nopadding gym_left nav-side-menu">	<!--  Left Side -->
  <ul class="nav nav-pills nav-stacked collapse in" id="menu-content">
				<li><a href="<?php echo site_url();?>"><span class="icone"><img src="<?php echo plugins_url( 'gym-management/assets/images/icon/home.png' )?>"/></span><span class="title"><?php _e('Home','gym_mgt');?></span></a></li>
				<li><a href="?dashboard=user"><span class="icone"><img src="<?php echo plugins_url('gym-management/assets/images/icon/dashboard.png' )?>"/></span><span
						class="title"><?php _e('Dashboard','gym_mgt');?></span></a></li>
				<?php
								
								 //$role = $obj_gym->role;
								foreach ( $menu as $value ) {
									if($value){
										echo '<li><a href="?dashboard=user&page=' . $value ['page_link'] . '" class="left-tooltip" data-tooltip="'. $value ['menu_title'] . '" title="'. $value ['menu_title'] . '"><span class="icone"> <img src="' .$value ['menu_icone'].'" /></span><span class="title">'. $value ['menu_title'] . '</span></a></li>';
									}
									?>
									
        
        <?php
								}
								?>
	  </ul>
		 </div>
		<div class="col-sm-10 page-inner" style="min-height:1050px;">
			<div class="qodef-container qodef-default-page-template">
		<?php do_action( 'prowess_select_after_container_open' ); ?>
				
				<div class="qodef-container-inner clearfix">
					<?php do_action( 'prowess_select_after_container_inner_open' ); ?>
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<div class="qodef-grid-row">
							<div <?php echo prowess_select_get_content_sidebar_class(); ?>>
								<?php
									the_content();
									do_action( 'prowess_select_page_after_content' );
								?>
							</div>
							<?php if ( $qodef_sidebar_layout !== 'no-sidebar' ) { ?>
								<div <?php echo prowess_select_get_sidebar_holder_class(); ?>>
									<?php get_sidebar(); ?>
								</div>
							<?php } ?>
						</div>
					<?php endwhile; endif; ?>
					<?php do_action( 'prowess_select_before_container_inner_close' ); ?>
				</div>
		
			<?php do_action( 'prowess_select_before_container_close' ); ?>
			</div>
		</div>
	</div>
</div>




<?php get_footer(); ?>