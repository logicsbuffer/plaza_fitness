<?php

if ( ! function_exists( 'prowess_select_register_recent_posts_widget' ) ) {
	/**
	 * Function that register search opener widget
	 */
	function prowess_select_register_recent_posts_widget( $widgets ) {
		$widgets[] = 'ProwessRecentPosts';
		
		return $widgets;
	}
	
	add_filter( 'prowess_select_register_widgets', 'prowess_select_register_recent_posts_widget' );
}