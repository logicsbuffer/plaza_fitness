<?php

if ( prowess_select_contact_form_7_installed() ) {
	include_once QODE_FRAMEWORK_MODULES_ROOT_DIR . '/widgets/contact-form-7/contact-form-7.php';
	
	add_filter( 'prowess_select_register_widgets', 'prowess_select_register_cf7_widget' );
}

if ( ! function_exists( 'prowess_select_register_cf7_widget' ) ) {
	/**
	 * Function that register cf7 widget
	 */
	function prowess_select_register_cf7_widget( $widgets ) {
		$widgets[] = 'ProwessContactForm7Widget';
		
		return $widgets;
	}
}