<?php

if ( ! function_exists( 'prowess_select_register_author_info_widget' ) ) {
	/**
	 * Function that register author info widget
	 */
	function prowess_select_register_author_info_widget( $widgets ) {
		$widgets[] = 'ProwessAuthorInfoWidget';
		
		return $widgets;
	}
	
	add_filter( 'prowess_select_register_widgets', 'prowess_select_register_author_info_widget' );
}