<?php

if ( ! function_exists( 'prowess_select_register_blog_list_widget' ) ) {
	/**
	 * Function that register blog list widget
	 */
	function prowess_select_register_blog_list_widget( $widgets ) {
		$widgets[] = 'ProwessBlogListWidget';
		
		return $widgets;
	}
	
	add_filter( 'prowess_select_register_widgets', 'prowess_select_register_blog_list_widget' );
}