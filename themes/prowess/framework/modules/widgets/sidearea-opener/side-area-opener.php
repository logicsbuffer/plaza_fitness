<?php

class ProwessSideAreaOpener extends ProwessWidget {
	public function __construct() {
		parent::__construct(
			'qodef_side_area_opener',
			esc_html__( 'Select Side Area Opener', 'prowess' ),
			array( 'description' => esc_html__( 'Display a "hamburger" icon that opens the side area', 'prowess' ) )
		);
		
		$this->setParams();
	}
	
	protected function setParams() {
		$this->params = array(
			array(
				'type'        => 'colorpicker',
				'name'        => 'icon_color',
				'title'       => esc_html__( 'Side Area Opener Color', 'prowess' ),
				'description' => esc_html__( 'Define color for side area opener', 'prowess' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'icon_hover_color',
				'title'       => esc_html__( 'Side Area Opener Hover Color', 'prowess' ),
				'description' => esc_html__( 'Define hover color for side area opener', 'prowess' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'widget_margin',
				'title'       => esc_html__( 'Side Area Opener Margin', 'prowess' ),
				'description' => esc_html__( 'Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'prowess' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'widget_title',
				'title' => esc_html__( 'Side Area Opener Title', 'prowess' )
			)
		);
	}
	
	public function widget( $args, $instance ) {

		$side_area_icon_source 	 	= prowess_select_options()->getOptionValue( 'side_area_icon_source' );
		$side_area_icon_pack 		= prowess_select_options()->getOptionValue( 'side_area_icon_pack' );
		$side_area_icon_svg_path 	= prowess_select_options()->getOptionValue( 'side_area_icon_svg_path' );

		$side_area_icon_class_array = array(
			'qodef-side-menu-button-opener',
			'qodef-icon-has-hover'
		);
	
		$side_area_icon_class_array[]  = $side_area_icon_source == 'icon_pack' ? 'qodef-side-menu-button-opener-icon-pack' : 'qodef-side-menu-button-opener-svg-path';

		$holder_styles = array();
		
		if ( ! empty( $instance['icon_color'] ) ) {
			$holder_styles[] = 'color: ' . $instance['icon_color'] . ';';
		}
		if ( ! empty( $instance['widget_margin'] ) ) {
			$holder_styles[] = 'margin: ' . $instance['widget_margin'];
		}

		?>
		
		<a <?php prowess_select_class_attribute( $side_area_icon_class_array ); ?> <?php echo prowess_select_get_inline_attr( $instance['icon_hover_color'], 'data-hover-color' ); ?> href="javascript:void(0)" <?php prowess_select_inline_style( $holder_styles ); ?>>
			<?php if ( ! empty( $instance['widget_title'] ) ) { ?>
				<h5 class="qodef-side-menu-title"><?php echo esc_html( $instance['widget_title'] ); ?></h5>
			<?php } ?>
			<span class="qodef-side-menu-icon">
				<?php if ( ( $side_area_icon_source == 'icon_pack' ) && isset( $side_area_icon_pack ) ) {
	        		echo prowess_select_icon_collections()->getMenuIcon( $side_area_icon_pack ); 
	        	} else if ( isset( $side_area_icon_svg_path ) ) {
	            	print $side_area_icon_svg_path; 
	            }?>
            </span>
		</a>
	<?php }
}