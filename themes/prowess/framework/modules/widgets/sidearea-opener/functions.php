<?php

if ( ! function_exists( 'prowess_select_register_sidearea_opener_widget' ) ) {
	/**
	 * Function that register sidearea opener widget
	 */
	function prowess_select_register_sidearea_opener_widget( $widgets ) {
		$widgets[] = 'ProwessSideAreaOpener';
		
		return $widgets;
	}
	
	add_filter( 'prowess_select_register_widgets', 'prowess_select_register_sidearea_opener_widget' );
}