<?php

if ( ! function_exists( 'prowess_select_register_icon_widget' ) ) {
	/**
	 * Function that register icon widget
	 */
	function prowess_select_register_icon_widget( $widgets ) {
		$widgets[] = 'ProwessIconWidget';
		
		return $widgets;
	}
	
	add_filter( 'prowess_select_register_widgets', 'prowess_select_register_icon_widget' );
}