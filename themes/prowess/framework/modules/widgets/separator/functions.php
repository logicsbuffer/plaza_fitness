<?php

if ( ! function_exists( 'prowess_select_register_separator_widget' ) ) {
	/**
	 * Function that register separator widget
	 */
	function prowess_select_register_separator_widget( $widgets ) {
		$widgets[] = 'ProwessSeparatorWidget';
		
		return $widgets;
	}
	
	add_filter( 'prowess_select_register_widgets', 'prowess_select_register_separator_widget' );
}