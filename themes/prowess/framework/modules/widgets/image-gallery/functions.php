<?php

if ( ! function_exists( 'prowess_select_register_image_gallery_widget' ) ) {
	/**
	 * Function that register image gallery widget
	 */
	function prowess_select_register_image_gallery_widget( $widgets ) {
		$widgets[] = 'ProwessImageGalleryWidget';
		
		return $widgets;
	}
	
	add_filter( 'prowess_select_register_widgets', 'prowess_select_register_image_gallery_widget' );
}