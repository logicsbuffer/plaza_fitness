<?php

if ( ! function_exists( 'prowess_select_register_social_icons_widget' ) ) {
	/**
	 * Function that register social icon widget
	 */
	function prowess_select_register_social_icons_widget( $widgets ) {
		$widgets[] = 'ProwessClassIconsGroupWidget';
		
		return $widgets;
	}
	
	add_filter( 'prowess_select_register_widgets', 'prowess_select_register_social_icons_widget' );
}