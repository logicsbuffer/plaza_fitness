<?php

if(!function_exists('prowess_select_register_sticky_sidebar_widget')) {
	/**
	 * Function that register sticky sidebar widget
	 */
	function prowess_select_register_sticky_sidebar_widget($widgets) {
		$widgets[] = 'ProwessStickySidebar';
		
		return $widgets;
	}
	
	add_filter('prowess_select_register_widgets', 'prowess_select_register_sticky_sidebar_widget');
}