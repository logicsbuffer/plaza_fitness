<?php

if ( ! function_exists( 'prowess_select_register_custom_font_widget' ) ) {
	/**
	 * Function that register custom font widget
	 */
	function prowess_select_register_custom_font_widget( $widgets ) {
		$widgets[] = 'ProwessCustomFontWidget';
		
		return $widgets;
	}
	
	add_filter( 'prowess_select_register_widgets', 'prowess_select_register_custom_font_widget' );
}