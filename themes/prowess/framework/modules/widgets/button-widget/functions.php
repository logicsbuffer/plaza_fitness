<?php

if ( ! function_exists( 'prowess_select_register_button_widget' ) ) {
	/**
	 * Function that register button widget
	 */
	function prowess_select_register_button_widget( $widgets ) {
		$widgets[] = 'ProwessButtonWidget';
		
		return $widgets;
	}
	
	add_filter( 'prowess_select_register_widgets', 'prowess_select_register_button_widget' );
}