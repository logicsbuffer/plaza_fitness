<?php

class ProwessButtonWidget extends ProwessWidget {
	public function __construct() {
		parent::__construct(
			'qodef_button_widget',
			esc_html__( 'Select Button Widget', 'prowess' ),
			array( 'description' => esc_html__( 'Add button element to widget areas', 'prowess' ) )
		);
		
		$this->setParams();
	}
	
	protected function setParams() {
		$this->params = array(
			array(
				'type'    => 'dropdown',
				'name'    => 'type',
				'title'   => esc_html__( 'Type', 'prowess' ),
				'options' => array(
					'solid'   => esc_html__( 'Solid', 'prowess' ),
					'outline' => esc_html__( 'Outline', 'prowess' ),
					'simple'  => esc_html__( 'Simple', 'prowess' )
				)
			),
			array(
				'type'        => 'dropdown',
				'name'        => 'size',
				'title'       => esc_html__( 'Size', 'prowess' ),
				'options'     => array(
					'small'  => esc_html__( 'Small', 'prowess' ),
					'medium' => esc_html__( 'Medium', 'prowess' ),
					'large'  => esc_html__( 'Large', 'prowess' ),
					'huge'   => esc_html__( 'Huge', 'prowess' )
				),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'prowess' )
			),
			array(
				'type'    => 'textfield',
				'name'    => 'text',
				'title'   => esc_html__( 'Text', 'prowess' ),
				'default' => esc_html__( 'Button Text', 'prowess' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'link',
				'title' => esc_html__( 'Link', 'prowess' )
			),
			array(
				'type'    => 'dropdown',
				'name'    => 'target',
				'title'   => esc_html__( 'Link Target', 'prowess' ),
				'options' => prowess_select_get_link_target_array()
			),
			array(
				'type'  => 'colorpicker',
				'name'  => 'color',
				'title' => esc_html__( 'Color', 'prowess' )
			),
			array(
				'type'  => 'colorpicker',
				'name'  => 'hover_color',
				'title' => esc_html__( 'Hover Color', 'prowess' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'background_color',
				'title'       => esc_html__( 'Background Color', 'prowess' ),
				'description' => esc_html__( 'This option is only available for solid button type', 'prowess' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'hover_background_color',
				'title'       => esc_html__( 'Hover Background Color', 'prowess' ),
				'description' => esc_html__( 'This option is only available for solid button type', 'prowess' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'border_color',
				'title'       => esc_html__( 'Border Color', 'prowess' ),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'prowess' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'hover_border_color',
				'title'       => esc_html__( 'Hover Border Color', 'prowess' ),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'prowess' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'margin',
				'title'       => esc_html__( 'Margin', 'prowess' ),
				'description' => esc_html__( 'Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'prowess' )
			)
		);
	}
	
	public function widget( $args, $instance ) {
		$params = '';
		
		if ( ! is_array( $instance ) ) {
			$instance = array();
		}
		
		// Filter out all empty params
		$instance = array_filter( $instance, function ( $array_value ) {
			return trim( $array_value ) != '';
		} );
		
		// Default values
		if ( ! isset( $instance['text'] ) ) {
			$instance['text'] = 'Button Text';
		}
		
		// Generate shortcode params
		foreach ( $instance as $key => $value ) {
			$params .= " $key='$value' ";
		}
		
		echo '<div class="widget qodef-button-widget">';
			echo do_shortcode( "[qodef_button $params]" ); // XSS OK
		echo '</div>';
	}
}