<?php

if ( ! function_exists( 'prowess_select_register_widgets' ) ) {
	function prowess_select_register_widgets() {
		$widgets = apply_filters( 'prowess_select_register_widgets', $widgets = array() );
		
		foreach ( $widgets as $widget ) {
			register_widget( $widget );
		}
	}
	
	add_action( 'widgets_init', 'prowess_select_register_widgets' );
}