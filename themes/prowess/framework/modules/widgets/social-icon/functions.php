<?php

if ( ! function_exists( 'prowess_select_register_social_icon_widget' ) ) {
	/**
	 * Function that register social icon widget
	 */
	function prowess_select_register_social_icon_widget( $widgets ) {
		$widgets[] = 'ProwessSocialIconWidget';
		
		return $widgets;
	}
	
	add_filter( 'prowess_select_register_widgets', 'prowess_select_register_social_icon_widget' );
}