<?php

if ( ! function_exists( 'prowess_select_register_woocommerce_dropdown_cart_widget' ) ) {
	/**
	 * Function that register dropdown cart widget
	 */
	function prowess_select_register_woocommerce_dropdown_cart_widget( $widgets ) {
		$widgets[] = 'ProwessWoocommerceDropdownCart';
		
		return $widgets;
	}
	
	add_filter( 'prowess_select_register_widgets', 'prowess_select_register_woocommerce_dropdown_cart_widget' );
}

if ( ! function_exists( 'prowess_select_get_dropdown_cart_icon_class' ) ) {
	/**
	 * Returns dropdow cart icon class
	 */
	function prowess_select_get_dropdown_cart_icon_class() {
		$dropdown_cart_icon_source = prowess_select_options()->getOptionValue( 'dropdown_cart_icon_source' );
		
		$dropdown_cart_icon_class_array = array(
			'qodef-header-cart'
		);
		
		$dropdown_cart_icon_class_array[] = $dropdown_cart_icon_source == 'icon_pack' ? 'qodef-header-cart-icon-pack' : 'qodef-header-cart-svg-path';
		
		return $dropdown_cart_icon_class_array;
	}
}

if ( ! function_exists( 'prowess_select_get_dropdown_cart_icon_html' ) ) {
	/**
	 * Returns dropdown cart icon HTML
	 */
	function prowess_select_get_dropdown_cart_icon_html() {
		$dropdown_cart_icon_source   = prowess_select_options()->getOptionValue( 'dropdown_cart_icon_source' );
		$dropdown_cart_icon_pack     = prowess_select_options()->getOptionValue( 'dropdown_cart_icon_pack' );
		$dropdown_cart_icon_svg_path = prowess_select_options()->getOptionValue( 'dropdown_cart_icon_svg_path' );
		
		$dropdown_cart_icon_html = '';
		
		if ( ( $dropdown_cart_icon_source == 'icon_pack' ) && ( isset( $dropdown_cart_icon_pack ) ) ) {
			$dropdown_cart_icon_html .= prowess_select_icon_collections()->getDropdownCartIcon( $dropdown_cart_icon_pack );
		} else if ( isset( $dropdown_cart_icon_svg_path ) ) {
			$dropdown_cart_icon_html .= $dropdown_cart_icon_svg_path;
		}
		
		return $dropdown_cart_icon_html;
	}
}