<?php

if ( ! function_exists( 'prowess_select_dropdown_cart_icon_styles' ) ) {
	/**
	 * Generates styles for dropdown cart icon
	 */
	function prowess_select_dropdown_cart_icon_styles() {
		$icon_color       = prowess_select_options()->getOptionValue( 'dropdown_cart_icon_color' );
		$icon_hover_color = prowess_select_options()->getOptionValue( 'dropdown_cart_hover_color' );
		
		if ( ! empty( $icon_color ) ) {
			echo prowess_select_dynamic_css( '.qodef-shopping-cart-holder .qodef-header-cart a', array( 'color' => $icon_color ) );
		}
		
		if ( ! empty( $icon_hover_color ) ) {
			echo prowess_select_dynamic_css( '.qodef-shopping-cart-holder .qodef-header-cart a:hover', array( 'color' => $icon_hover_color ) );
		}
	}
	
	add_action( 'prowess_select_style_dynamic', 'prowess_select_dropdown_cart_icon_styles' );
}