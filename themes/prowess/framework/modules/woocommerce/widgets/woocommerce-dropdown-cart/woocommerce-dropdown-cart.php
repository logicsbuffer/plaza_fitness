<?php

class ProwessWoocommerceDropdownCart extends ProwessWidget {
    public function __construct() {
        parent::__construct(
            'qodef_woocommerce_dropdown_cart',
            esc_html__('Woocommerce Dropdown Cart', 'prowess'),
            array('description' => esc_html__('Display a shop cart icon with a dropdown that shows products that are in the cart', 'prowess'),)
        );

        $this->setParams();
    }

    protected function setParams() {
        $this->params = array(
            array(
                'type'        => 'textfield',
                'name'        => 'woocommerce_dropdown_cart_margin',
                'title'       => esc_html__('Icon Margin', 'prowess'),
                'description' => esc_html__('Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'prowess')
            )
        );
    }

    public function widget($args, $instance) {
        extract($args);

        global $woocommerce;

        $icon_styles = array();

        if ($instance['woocommerce_dropdown_cart_margin'] !== '') {
            $icon_styles[] = 'padding: ' . $instance['woocommerce_dropdown_cart_margin'];
        }

        $cart_is_empty = sizeof($woocommerce->cart->get_cart()) <= 0;

        $dropdown_cart_icon_class = prowess_select_get_dropdown_cart_icon_class();
        ?>
        <div class="qodef-shopping-cart-holder" <?php prowess_select_inline_style($icon_styles) ?>>
            <div class="qodef-shopping-cart-inner">
                <a itemprop="url" <?php prowess_select_class_attribute( $dropdown_cart_icon_class ); ?> href="<?php echo wc_get_cart_url(); ?>">
                    <span class="qodef-cart-icon"><?php echo prowess_select_get_dropdown_cart_icon_html(); ?></span>
	                <span class="qodef-cart-number"><?php echo sprintf(_n('%d', '%d', WC()->cart->cart_contents_count, 'prowess'), WC()->cart->cart_contents_count); ?></span>
                </a>
                <div class="qodef-shopping-cart-dropdown">
                    <ul>
                        <?php if (!$cart_is_empty) : ?>
                            <?php foreach ($woocommerce->cart->get_cart() as $cart_item_key => $cart_item) :
                                $_product = $cart_item['data'];
                                // Only display if allowed
                                if (!$_product->exists() || $cart_item['quantity'] == 0) {
                                    continue;
                                }
                                // Get price
                                $product_price = get_option('woocommerce_tax_display_cart') == 'excl' ? wc_get_price_excluding_tax($_product) : wc_get_price_including_tax($_product);
                                ?>
                                <li>
                                    <div class="qodef-item-image-holder">
                                        <a itemprop="url" href="<?php echo esc_url(get_permalink($cart_item['product_id'])); ?>">
                                            <?php echo wp_kses($_product->get_image(), array(
                                                'img' => array(
                                                    'src'    => true,
                                                    'width'  => true,
                                                    'height' => true,
                                                    'class'  => true,
                                                    'alt'    => true,
                                                    'title'  => true,
                                                    'id'     => true
                                                )
                                            )); ?>
                                        </a>
                                    </div>
                                    <div class="qodef-item-info-holder">
                                        <h5 itemprop="name" class="qodef-product-title">
	                                        <a itemprop="url" href="<?php echo esc_url(get_permalink($cart_item['product_id'])); ?>"><?php echo apply_filters('prowess_select_woo_widget_cart_product_title', $_product->get_name(), $_product); ?></a>
                                        </h5>
                                        <?php if(apply_filters('prowess_select_woo_cart_enable_quantity', true)) { ?>
                                            <span class="qodef-quantity"><?php echo esc_html($cart_item['quantity']); esc_html_e(' x', 'prowess'); ?></span>
                                        <?php } ?>
                                        <?php echo apply_filters('prowess_select_woo_cart_item_price_html', wc_price($product_price), $cart_item, $cart_item_key); ?>
                                        <?php echo apply_filters('prowess_select_woo_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s"><span class="icon-arrows-remove"></span></a>', esc_url( wc_get_cart_remove_url( $cart_item_key ) ), esc_html__('Remove this item', 'prowess')), $cart_item_key); ?>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                            <li class="qodef-cart-bottom">
                                <div class="qodef-subtotal-holder clearfix">
                                    <span class="qodef-total"><?php esc_html_e('TOTAL', 'prowess'); ?></span>
                                    <span class="qodef-total-amount">
										<?php echo wp_kses($woocommerce->cart->get_cart_subtotal(), array(
                                            'span' => array(
                                                'class' => true,
                                                'id'    => true
                                            )
                                        )); ?>
									</span>
                                </div>
                                <div class="qodef-btn-holder clearfix">
                                    <a itemprop="url" href="<?php echo wc_get_cart_url(); ?>"
                                       class="qodef-view-cart"
                                       data-title="<?php esc_attr_e('VIEW CART', 'prowess'); ?>"><span><?php esc_html_e('VIEW CART', 'prowess'); ?></span>
                                    </a>
                                    <a itemprop="url" href="<?php echo wc_get_checkout_url(); ?>"
                                       class="qodef-view-checkout"
                                       data-title="<?php esc_attr_e('CHECKOUT', 'prowess'); ?>"><span><?php esc_html_e('CHECKOUT', 'prowess'); ?></span>
                                    </a>
                                </div>
                            </li>
                        <?php else : ?>
                            <li class="qodef-empty-cart"><?php esc_html_e('No products in the cart.', 'prowess'); ?></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php
    }
}

add_filter('woocommerce_add_to_cart_fragments', 'prowess_select_woocommerce_header_add_to_cart_fragment');

function prowess_select_woocommerce_header_add_to_cart_fragment($fragments) {
    global $woocommerce;

    ob_start();

    $cart_is_empty = sizeof($woocommerce->cart->get_cart()) <= 0;
    
    $dropdown_cart_icon_class = prowess_select_get_dropdown_cart_icon_class();

    ?>
    <div class="qodef-shopping-cart-inner">
        <a itemprop="url" <?php prowess_select_class_attribute( $dropdown_cart_icon_class ); ?> href="<?php echo wc_get_cart_url(); ?>">
            <span class="qodef-cart-icon"><?php echo prowess_select_get_dropdown_cart_icon_html(); ?></span>
	        <span class="qodef-cart-number"><?php echo sprintf(_n('%d', '%d', WC()->cart->cart_contents_count, 'prowess'), WC()->cart->cart_contents_count); ?></span>
        </a>
        <div class="qodef-shopping-cart-dropdown">
            <ul>
                <?php if (!$cart_is_empty) : ?>
                    <?php foreach ($woocommerce->cart->get_cart() as $cart_item_key => $cart_item) :
                        $_product = $cart_item['data'];
                        // Only display if allowed
                        if (!$_product->exists() || $cart_item['quantity'] == 0) {
                            continue;
                        }
                        // Get price
                        $product_price = get_option('woocommerce_tax_display_cart') == 'excl' ? wc_get_price_excluding_tax($_product) : wc_get_price_including_tax($_product);
                        ?>
                        <li>
                            <div class="qodef-item-image-holder">
                                <a itemprop="url" href="<?php echo esc_url(get_permalink($cart_item['product_id'])); ?>">
                                    <?php echo wp_kses($_product->get_image(), array(
                                        'img' => array(
                                            'src'    => true,
                                            'width'  => true,
                                            'height' => true,
                                            'class'  => true,
                                            'alt'    => true,
                                            'title'  => true,
                                            'id'     => true
                                        )
                                    )); ?>
                                </a>
                            </div>
                            <div class="qodef-item-info-holder">
                                <h5 itemprop="name" class="qodef-product-title">
	                                <a itemprop="url" href="<?php echo esc_url(get_permalink($cart_item['product_id'])); ?>"><?php echo apply_filters('prowess_select_woo_widget_cart_product_title', $_product->get_name(), $_product); ?></a>
                                </h5>
		                        <?php if(apply_filters('prowess_select_woo_cart_enable_quantity', true)) { ?>
                                    <span class="qodef-quantity"><?php echo esc_html($cart_item['quantity']); esc_html_e(' x', 'prowess'); ?></span>
                                <?php } ?>
                                <?php echo apply_filters('prowess_select_woo_cart_item_price_html', wc_price($product_price), $cart_item, $cart_item_key); ?>
                                <?php echo apply_filters('prowess_select_woo_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s"><span class="icon-arrows-remove"></span></a>', esc_url( wc_get_cart_remove_url( $cart_item_key ) ), esc_html__('Remove this item', 'prowess')), $cart_item_key); ?>
                            </div>
                        </li>
                    <?php endforeach; ?>
                    <li class="qodef-cart-bottom">
                        <div class="qodef-subtotal-holder clearfix">
                            <span class="qodef-total"><?php esc_html_e('TOTAL', 'prowess'); ?></span>
                            <span class="qodef-total-amount">
								<?php echo wp_kses($woocommerce->cart->get_cart_subtotal(), array(
                                    'span' => array(
                                        'class' => true,
                                        'id'    => true
                                    )
                                )); ?>
							</span>
                        </div>
                        <div class="qodef-btn-holder clearfix">
                            <a itemprop="url" href="<?php echo wc_get_cart_url(); ?>"
                               class="qodef-view-cart"
                               data-title="<?php esc_attr_e('VIEW CART', 'prowess'); ?>"><span><?php esc_html_e('VIEW CART', 'prowess'); ?></span>
                            </a>
                            <a itemprop="url" href="<?php echo wc_get_checkout_url(); ?>"
                               class="qodef-view-checkout"
                               data-title="<?php esc_attr_e('CHECKOUT', 'prowess'); ?>"><span><?php esc_html_e('CHECKOUT', 'prowess'); ?></span>
                            </a>
                        </div>
                    </li>
                <?php else : ?>
                    <li class="qodef-empty-cart"><?php esc_html_e('No products in the cart.', 'prowess'); ?></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>

    <?php
    $fragments['div.qodef-shopping-cart-inner'] = ob_get_clean();

    return $fragments;
}

?>