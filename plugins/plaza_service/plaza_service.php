<?php

/*

Plugin Name: Plaza Service

Description: Plaza Service

Version: 1.0.0

Plugin URI: https://www.fiverr.com/logics_buffer

Author: LogicsBuffer

Author URI: http://logicsbuffer.com/

*/

add_filter( 'woocommerce_checkout_fields' , 'default_values_checkout_fields' );
function default_values_checkout_fields( $fields ) {
// You can use this for postcode, address, company, first name, last name and such. 
	global $woocommerce;
    $items = $woocommerce->cart->get_cart();
	foreach($items as $item => $values) { 
		$user_email = get_post_meta($values['product_id'] , 'user_email', true);
		$user_phone = get_post_meta($values['product_id'] , 'user_phone', true);
	} 
	$fields['billing']['billing_email']['default'] = $user_email;
	$fields['billing']['billing_phone']['default'] = $user_phone;
	return $fields;
}
function add_cart_item_custom_data($cart_item_meta, $new_post_id) {
	global $woocommerce;
	$user_email = get_post_meta($new_post_id, 'user_email' );
	$user_phone = get_post_meta($new_post_id, 'user_phone' );
	$cart_item_meta['user_email'] = $user_email;
	$cart_item_meta['user_phone'] = $user_phone;
	return $cart_item_meta;
}


add_action('wp_enqueue_scripts', 'immortal_script_front_css');



add_action('init', 'wp_immortal');

function wp_immortal() {

   add_shortcode('plazafitness_service', 'wp_plazafitness_service_form');

}

function wp_plazafitness_service_form($atts) {

	if(isset($_POST['submit_prod'])){
			global $woocommerce;
			$place_to_start= $_POST['place_to_start'];
			$user_email= $_POST['user_email'];
			$user_phone= $_POST['user_phone'];
			$trainer_product_id = $_POST['trainer'];			
			$skuu = "trainer_service";
			$product = wc_get_product( $trainer_product_id );
			$product_price = $product->get_price();
			update_post_meta($trainer_product_id, '_sku', $skuu );
			update_post_meta( $trainer_product_id, '_price', $product_price );
			update_post_meta( $trainer_product_id,'_regular_price', $product_price );							
			update_post_meta( $trainer_product_id,'user_phone', $user_phone );							
			update_post_meta( $trainer_product_id,'user_email', $user_email);							
			$woocommerce->cart->add_to_cart($trainer_product_id);
			$cart_url = site_url().'/checkout/';
			wp_redirect( $cart_url );
			exit;
	}
	if(isset($_POST['submit_prod_personal'])){
		global $woocommerce;
		$user_phone_personal= $_POST['user_phone_personal'];
		$user_email_personal= $_POST['user_email_personal'];
		$new_post_id = 22112;
		$product = wc_get_product($new_post_id);
		$product_price = $product->get_price();
		update_post_meta( $new_post_id, '_price', $product_price );
		update_post_meta( $new_post_id,'_regular_price', $product_price );							
		update_post_meta( $new_post_id,'user_phone', $user_phone_personal );							
		update_post_meta( $new_post_id,'user_email', $user_email_personal );							
		$woocommerce->cart->add_to_cart($new_post_id);
		$cart_url = site_url().'/checkout/';
		wp_redirect( $cart_url );
		exit;
	}

$posts_array = get_posts(
    array(
        'posts_per_page' => -1,
        'post_type' => 'product',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => array('service')
            )
        )
    )
);
ob_start();

?>

<div class='form_parent'>
	<div id="back_btn" style="display: none;"><button onclick="myFunction()" type='button'>Back</button></div>

	<div class="heading_experience"><p>Experience Authentic</p></div>

	<div class="heading_training"><p>PERSONAL TRAINING</p></div>

	<div class='form_experience' id='step_1'>


	<div class='radio_buttons' id='radio_buttons1'>

			

			<h2>Select The Best Place To Start</h2>



			<label><div class="form-control"><input onclick="show1();" type='radio' name='place_to_start' value='male'>Online - Guided by a Certified Coach</div></label>



			<label><div class="form-control"><input onclick='show2();' type='radio' name='place_to_start' value='female'>Online - Self Guided Personal Training</div></label>



			<label> <div class="form-control"><input onclick='show3();' type='radio' name='place_to_start' value='other'>Albany, New York - Stuyvesant Plaza</div> </label>

	</div>

	</div>



	<div class="radio_buttons" class="form-group" class='form_experience' id='step_2' style="display: none;">
		<form role="form" action="" method="post" id="add_cart" enctype="multipart/form-data" method="post">						
			<div class="form-group" class='radio_buttons' id='radio_buttons2' >
				<div class="form-group">
					<label>Email:</label> 

				<input type='text' name="user_email" placeholder='Your Email'>
				</div>

				<div class="form-group">
					<label>Phone: </label> 

				<input type='text' name="user_phone" placeholder='Your Phone'>
				</div>


				<div class="form-group">

					<label>Select Your Trainer:</label>
							
					<select name="trainer" class='select_dropdown'>
						<option value=''>Select trainer</option>
						<?php
							foreach($posts_array as $poste){
								$id = $poste->ID;
								$post_title = $poste->post_title;
									echo '<option value="'.$id.'">'.$post_title.'</option>';
									
							}
						?>
					</select>
				</div>
			   <div class="ready_btn"><input class="submit_prod" name="submit_prod" type="submit" value="I am Ready To Start"></div>
			</div>
		</form>
	</div>

                           

	<div class="form-group" class='form_experience' id='step_3' style="display:none">
		<form role="form" action="" method="post" id="add_cart_personal" method="post">
			<div class='radio_buttons input_ep'>

				<div class="form-group"> <label>Email:</label> <input type='email'  name="user_email_personal" placeholder='Your Email' required></div>

				<div class="form-group"> <label>Phone:</label> <input type='text'  name="user_phone_personal" placeholder='Your Phone' required></div>

			   <div class="ready_btn"><input class="submit_prod" name="submit_prod_personal" type="submit" value="I am Ready To Start"></div>
			</div>
		</form>
	</div>
</div>

<script>
function myFunction() {
	  document.getElementById('step_1').style.display ='block';

	  document.getElementById('step_2').style.display ='none';

	  document.getElementById('step_3').style.display ='none';
	  document.getElementById('back_btn').style.display ='none';

}

function show1(){

	  document.getElementById('step_1').style.display ='none';

	  document.getElementById('step_2').style.display ='block';
	  document.getElementById('back_btn').style.display ='block';

	}

	function show2(){

	  document.getElementById('step_1').style.display ='none';

	  document.getElementById('step_2').style.display ='none';

	  document.getElementById('step_3').style.display ='block';
	  document.getElementById('back_btn').style.display ='block';

	}

	function show3(){

	 window.location.replace("https://www.plazafitness.org/schedule-a-free-strategy-session");

	}



</script>

<style type="text/css">
.submit_prod{
    background-color: #d41100;
    border: none;
    font-size: 19px;
    border-radius: 4px;
    color: white;
    padding: 7px 14px;
}
.ready_btn {
    margin-top: 10px;
}
.form_parent {
    width: 74%;
    margin: 0 auto;
}
.form_parent .form-control:hover {
    color: #FFF;
    background-color: #3d3f47;
    border-radius: 4px;
    transform: scale(1.03);
    box-shadow: 0 2px 6px rgba(0,0,0,0.14),inset 0 0 0 2px rgba(255,255,255,0.16) !important;
}
.radio_buttons {
    padding: 39px 71px 50px 50px;
    background-color: rgba(255,255,255,0.61);
    border-width: 3px;
    border-style: solid;
    border-top-color: rgba(0,0,0,0.7);
    border-bottom-color: rgba(0,0,0,0.7);
    border-left: none;
    border-radius: 25px;
    border-right: none;
    box-shadow: 0 2px 5px 2px rgba(0,0,0,0.4);
    width: 65%;
    margin: 0 auto;
}	
.form_parent .heading_experience {
    color: #d41100;
    font-size: 33px;
    font-weight: 700;
    line-height: 56px;
}
.form_parent .heading_training {
    font-size: 35px;
    color: white;
    font-weight: 500;
}
div#radio_buttons1 h2 {
    font-size: 30px;
    color: #333333;
}
.form_parent .form-control {
    background: none;
    border: none;
    box-shadow: none;
    padding: 25px 15px;
    color: rgb(47, 47, 47);
    font-size: 19px;
    display: flex;
    height: auto;
    font-weight: 500;
}
.form_parent {
    width: 74%;
    margin: 0 auto;
    text-align: center;
    padding-top: 30px;
    padding-bottom: 30px;
    margin-top: 100px;
    margin-bottom: 100px;
    border-color: #d41100;
    background-color: #000;
    border-width: 10px;
    box-shadow: 0 2px 5px 2px rgba(0,0,0,0.4);
    border-style: solid;
    border-left: none;
    border-right: none;
    border-radius: 25px;
}
div#radio_buttons2 label {
    color: black;
    float: left;
}
.select_dropdown {
    display: block;
    width: 100%;
    padding: 12px;
}
div#radio_buttons2 {
    width: 50%;
    margin: 0 auto;
}
div#radio_buttons2 input {
    padding: 8px;
}
.form_parent .ready_btn button {
    background-color: #D82717;
    border: none;
    font-size: 17px;
    color: white;
    padding: 11px;
    margin-top: 18px;
}
.form_parent .radio_buttons input {
    padding: 8px;
}
.radio_buttons.input_ep .form-group {
    width: 50%;
    margin: auto;
}
.form_parent .input_ep label {
    color: black;
    float: left;
}
.form_parent #back_btn button {
    background-color: #d41100;
    border: none;
    font-size: 19px;
    border-radius: 4px;
    color: white;
    padding: 4px 20px 4px 20px;
}
.form_parent div#back_btn button {
    display: block;
    margin-left: 10px;
}	
</style>
<?php

return ob_get_clean();

}





function immortal_script_front_css() {

		/* CSS */

        wp_register_style('immortal_style', plugins_url('css/coins_calculator.css',__FILE__));
		//wp_enqueue_style( 'immortal_style', plugins_url( 'css/coins_calculator.css', _FILE_ ), '', time() );

}



